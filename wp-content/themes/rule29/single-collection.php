<?php get_header(); ?>
<?php rule29_slider( get_the_ID() ); ?>
<section class="section-gray section-content">
	<div class="wrap">
		<?php rule29_collection_project_posts(); ?>
		<?php rule29_collection_news_posts(); ?>
		<div class="border-top">
			<h2><em>Around the Web</em></h2>
		</div>
		<div class="grid cf">
			<div class="grid-cell grid-cell-1" style="position: absolute; left: 480px; top: 0px;"><div class="grid-inner "><div class="feed-cache" data-callback="render_instagram" data-source="instagram" data-user="rule29" data-index="0"></div></div></div>
			<div class="grid-cell grid-cell-1">
				<div class="grid-inner grid-open">
					<div class="feed-cache" data-callback="render_twitter" data-source="twitter" data-user="rule29"></div>
				</div>
			</div>
			<div class="grid-cell grid-cell-1" style="position: absolute; left: 480px; top: 0px;"><div class="grid-inner "><div class="feed-cache" data-callback="render_instagram" data-source="instagram" data-user="rule29" data-index="1"></div></div></div>
			<div class="grid-cell grid-cell-1">
				<div class="grid-inner grid-open">
					<div class="feed-cache" data-callback="render_twitter" data-source="twitter" data-user="designsobriety"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>