<?php /* Template Name: Creative Matter */ ?>
<?php get_header(); ?>
<?php rule29_slider( get_option( 'rule29_creative_matter_page' ), 'full', true, array(), true ); ?>
<?php get_template_part( 'project', 'listing' ); ?>
<?php get_footer(); ?>
