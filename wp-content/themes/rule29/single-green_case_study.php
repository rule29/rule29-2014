<?php get_header(); ?>
<?php
if (rule29_slider( get_the_ID(), 'full', false)){
	rule29_slider( get_the_ID(), 'full', false);
}else{
	rule29_featured_image();
}

?>
<?php if ( have_posts() ) { ?>
<?php while ( have_posts() ) { ?>
<?php the_post(); ?>
<section class="section-gray section-content">
	<div class="wrap cf">
		<div class="col-1-6 col-tb-1-4">
			<nav class="nav-sub">
				<?php wp_nav_menu(array(
					'theme_location' => 'case-study',
					'menu_class' => 'menu cf',
					'container'       => false,
					'depth'           => 3,
				));?>
			</nav>
		</div>
		<div class="col-2-3 col-tb-3-4">
			<div class="col-inner">
				<article class="main-content">
					<?php the_content(); ?>
				</article>
			</div>
		</div>
		<div class="col-1-6 col-tb-1">
			<?php if( have_rows('case_study_related_links') ):?>
				<nav class="nav-social nav-related">
					<ul class="menu menu-social">
						<?php while( have_rows('case_study_related_links') ): the_row();?>
							<li class="menu-item">
								<a href="<?php the_sub_field('url');?>" target="_blank"><?php the_sub_field('link_text');?></a>
							</li>
						<?php endwhile;?>
					</ul>
				</nav>
			<?php endif;?>
		</div>
	</div>
</section>
<?php } // endwhile posts ?>
<?php } // endif posts ?>
<?php get_footer(); ?>
