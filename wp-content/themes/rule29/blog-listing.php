<?php get_header(); ?>
<?php rule29_blog_query(); ?>
<?php rule29_blog_slider(); ?>
<section class="section-filters">
	<div class="wrap">
		<div class="trigger-filters">
			<div class="trigger-filters-text">Filter Posts</div>
		</div>
	</div>
	<div class="filters">
		<div class="wrap cf">
			<div class="col-1-3 col-tb-1">
				<h4 class="filter-type">Authors</h4>
				<?php rule29_author_filters(); ?>
			</div>
			<div class="col-1-3 col-tb-1">
				<h4 class="filter-type">Categories</h4>
				<?php rule29_category_filters(); ?>
			</div>
			<div class="col-1-3 col-tb-1">
				<h4 class="filter-type">Date</h4>
				<div class="selector selector-secondary">
					<div class="selector-trigger">Select a Date</div>
					<div class="selector-dropdown">
						<ul class="selector-options">
							<?php rule29_date_filters(); ?>
						</ul>
					</div>
				</div>
				<h4 class="filter-type">Search</h4>
				<form class="form-search" action="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">
					<input type="text" name="search" class="input-search" placeholder="Enter Keywords">
					<button class="button-submit">Submit</button>
				</form>
			</div>
		</div>
	</div>
</section>
<section class="section-gray section-content">
	<div class="wrap cf">
<?php if ( have_posts() ) { ?>
		<ul class="list-posts">
			<?php while ( have_posts() ) { ?>
			<?php the_post(); ?>
			<li class="col-1-2">
				<div class="post-item">
					<a href="<?php the_permalink(); ?>" class="post-header">
						<?php $class = '';
						if ( ! has_post_thumbnail() ) {
							$class = ' no-thumb';
						} // endif has thumbnail ?>
						<div class="post-header-text<?php echo $class; ?>">
							<div class="post-header-column">
								<div class="post-header-row">
									<h1 class="post-title"><?php the_title(); ?></h1>
									<time class="post-date"><?php //echo get_the_date('F j, Y'); ?></time>
								</div>
							</div>
						</div>
						<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'blog-listing', array( 'class' => 'post-featured-image' ) );
						} // endif has thumbnail ?>
					</a>
					<div class="post-excerpt">
						<?php $excerpt = get_the_excerpt(); ?>
						<p><?php echo '<span class="first-letter">' . substr( $excerpt, 0, 1 ) . '</span>' . substr( $excerpt, 1 ); ?></p>
					</div>
					<div class="read-more">
						<a href="<?php the_permalink(); ?>" class="button button-primary">Read More</a>
					</div>
				</div>
			</li>
			<?php } // endwhile posts ?>
		</ul>
		<?php
       special_nav();
       /*
		<div class="col-1">
			<div class="navigation-post cf">
				<?php $previous_posts_link = get_previous_posts_link( 'Previous' );
				if ( ! empty( $previous_posts_link ) ) { ?>
				<div class="prev-posts pull-left">
					<?php echo $previous_posts_link; ?>
				</div>
				<?php }
				$next_posts_link = get_next_posts_link( 'Next' );
				if ( ! empty( $next_posts_link ) ) { ?>
				<div class="next-posts pull-right">
					<?php echo $next_posts_link; ?>
				</div>
				<?php } ?>
			</div>
		</div> */
		?>
<?php } else { ?>
		<h2 class="archive-title"><em>No posts found.</em></h2>
<?php } // endif posts ?>
	</div>
</section>
<?php get_footer(); ?>