<?php

class fld_custom_taxonomy {
	public $fields;
	public $taxonomy_name;

	function __construct( $fields, $taxonomy_name ) {
		$this->fields = $fields;
		$this->taxonomy_name = $taxonomy_name;

		add_action( 'init', array($this, 'init') );
	}

	public function init() {
		add_action( $this->taxonomy_name . '_edit_form_fields', array($this, 'display'), 10, 2);
		add_action( 'edited_' . $this->taxonomy_name, array($this, 'save'), 10, 2);

		global $wpdb;
		$wpdb->termmeta = $wpdb->prefix."termmeta";
	}

	public function display($tag, $taxonomy) {
		foreach($this->fields as $fld) {
			echo <<<EOS
<tr class="form-field">
	<th scope="row" valign="top"><label for="{$fld['name']}">{$fld['label']}</label></th>
	<td>
EOS;

			$value = get_metadata('term', $tag->term_id, $fld['name'], true);
			fld_create_field($fld, $value);

			echo <<<EOS
	</td>
</tr>
EOS;
		}
	}

	public function save($term_id, $tt_id) {
		if (!$term_id) return;

        foreach($this->fields as $field) {
            $value = fld_save_field( $field, $fld );

			update_metadata('term', $term_id, $field['name'], $value);
        }
	}
}
