<?php

add_action( 'admin_enqueue_scripts', 'fld_enqueue_media_browser' );
add_action( 'wp_ajax_fld_media_preview', 'custom_fields_media_preview' );

function custom_fields_media_preview() {
    $image_ids = $_POST['image_ids'];

    $image_urls = array();

    foreach($image_ids as $image_id) {
        $image_id = intval($image_id);

        $image_url = wp_get_attachment_image_src($image_id, 'thumbnail');
        if (!empty($image_url))
            $image_urls[] = $image_url[0];
    }

    $response = json_encode( array( 'success' => true, 'urls' => $image_urls ) );

    header( "Content-Type: application/json" );
    echo $response;

    exit;
}

function fld_create_field( $args, $value = '' ) {
    $className = 'fld_option_' . $args['type'];
    if (class_exists($className)) {
        $fld = new $className($args, $value);
        $fld->meta_content();
    }
}

function fld_save_field( $args ) {
    $className = 'fld_option_' . $args['type'];
    if (class_exists($className)) {
        $fld = new $className($args);
        return $fld->get_post_value();
    }
}

function fld_enqueue_media_browser() {
    wp_enqueue_media();
    wp_enqueue_script('fld-fields', get_template_directory_uri() . '/core/fields.js' );
    wp_enqueue_style('fld-fields', get_template_directory_uri() . '/core/fields.css' );
}

abstract class fld_option_input {
    var $name;
    var $args;

    function __construct( $args, $value = '' ) {
        $this->args = $args;
        $this->name = $args['name'];
        $this->value = $value;

        // wp_enqueue_script('fld-fields', get_template_directory_uri() . '/core/fields.js' );
    }

    public function meta_content() {
        $this->output();
    }

    public function get_post_value() {
        return $_POST[$this->name];
    }

    public function output() {
        $value = htmlspecialchars($this->value);
        echo <<<EOS
<input type="hidden" name="{$this->name}" value="$value" />
EOS;
    }
}

class fld_option_text extends fld_option_input {

    public function output() {
        $value = htmlspecialchars($this->value);
        echo <<<EOS
    <input type="text" id="{$this->name}" name="{$this->name}" value="{$value}" size="60" />
EOS;
    }
}

class fld_option_textarea extends fld_option_input {

    public function output() {
        $value = htmlspecialchars($this->value);
        echo <<<EOS
    <textarea id="$this->name" name="{$this->name}" style="width: 450px; height: 100px;resize: none;">$value</textarea>
EOS;
    }
}

class fld_option_html extends fld_option_input {

    public function output() {
        wp_editor($this->value, $this->name);
    }
}

class fld_option_checkbox extends fld_option_input {

    public function output() {
        $checked = checked( 1, $this->value, false );

        echo <<<EOS
    <input type="checkbox" name="$this->name" id="$this->name" value="1" {$checked} />
EOS;
    }
}

class fld_option_dropdown extends fld_option_input {

    public function output() {
        $args = $this->args;

        $options_html = '';
        $multiple = false;
        if (isset($args['multiple']) && $args['multiple']) {
            $multiple = true;
        } else {
            if (isset($args['include_empty']) && $args['include_empty']) {
                $options_html .= '<option value=""></option>';
            }
        }

        if (!empty($args['options'])) {
            foreach($args['options'] as $value => $label) {
                $selected = '';
                if ($value == $this->value || (is_array($this->value) && in_array($value, $this->value))) {
                    $selected = 'selected="selected" ';
                }
                $options_html .= '<option ' . $selected . 'value="' . htmlspecialchars($value) . '">' . htmlspecialchars($label) . '</option>';
            }
        }

        $select_multiple = "";
        $name = $this->name;
        if ($multiple) {
            $select_multiple = 'multiple="multiple"';
            $name .= '[]';
        }

        echo <<<EOS
    <select name="{$name}" id="{$this->name}" {$select_multiple}>
        {$options_html}
    </select>
EOS;
    }
}

// should probably be depricated/avoided?
class fld_option_dropdown_sql extends fld_option_dropdown {
    public function output() {
        global $wpdb;

        $rows = $wpdb->get_results($this->args['query']);

        $options = array();
        foreach($rows as $row) {
            $options[$row->value] = $row->label;
        }
        $this->args['options'] = $options;


        parent::output();
    }
}

class fld_option_dropdown_posts extends fld_option_dropdown {
    public function output() {
        $args = $this->args;

        $query_args = (!empty($args['query'])) ? $args['query'] : array( 'post_type' => 'page', 'posts_per_page' => -1 );

        $wp_query = new WP_Query( $query_args );

        $posts = array();

        if ($wp_query->have_posts()) {
            while ($wp_query->have_posts()) {
                $wp_query->the_post();

                $posts[get_the_ID()] = get_the_title();
            }
        }

        wp_reset_postdata();

        $this->args['options'] = $posts;

        parent::output();
    }
}

class fld_option_list extends fld_option_input {

    public function output() {
        $args = $this->args;

        $multiple = (isset($args['multiple']) && !$args['multiple']) ? false : true;

        $box_index = 0;

        if (!empty($args['options'])) {
            foreach($args['options'] as $value => $label) {
                $checked = ($value == $this->value || (is_array($this->value) && in_array($value, $this->value))) ? ' checked="checked"' : '';
                $type = ($multiple) ? 'checkbox' : 'radio';
                $name = ($multiple) ? "{$this->name}[{$value}]" : $this->name;

                echo <<<EOS
<input type="$type" id="{$this->name}-$box_index" name="$name" $checked value="$value">
<label for="{$this->name}-$box_index">$label</label>
<br />
EOS;

                $box_index++;
            }
        }
    }

}

class fld_option_list_posts extends fld_option_list {
    public function output() {
        $args = $this->args;

        $query_args = (!empty($args['query'])) ? $args['query'] : array( 'post_type' => 'page', 'posts_per_page' => -1 );

        $wp_query = new WP_Query( $query_args );

        $posts = array();

        if ($wp_query->have_posts()) {
            while ($wp_query->have_posts()) {
                $wp_query->the_post();

                $posts[get_the_ID()] = get_the_title();
            }
        }

        wp_reset_postdata();

        $this->args['options'] = $posts;

        parent::output();
    }
}

class fld_option_list_taxonomies extends fld_option_input {

}

class fld_option_dropdown_taxonomies extends fld_option_dropdown {
    public function output() {
        $args = $this->args;

        $taxonomies = ( ! empty( $args['taxonomies'] ) )
            ? $args['taxonomies'] : array( 'category' );
        $query_args = (!empty($args['query'])) ? $args['query'] : array( );

        $terms = get_terms( $taxonomies, $query_args );

        $term_options = array();

        if ( ! empty( $terms ) )
        foreach( $terms as $term ) {
            $term_options[ $term->term_id ] = $term->name;
        }

        $this->args['options'] = $term_options;

        parent::output();
    }
}

class fld_option_media extends fld_option_input {
    public function output() {
        $images = '';
        $image_ids = explode(',', $this->value);

        $change_text = 'Change';
        $display_none = '';

        if (!empty($image_ids) && !empty($image_ids[0])) {
            foreach($image_ids as $image_id) {
                $image_id = intval($image_id);

                $image_url = wp_get_attachment_image_src($image_id, 'thumbnail');
                if (!empty($image_url)) {
                    $images .= '<li><img src="' . $image_url[0] . '" /></li>';
                }
            }
        } else {
            $change_text = 'Add Images';
            $display_none = ' style="display: none;"';
        }

        echo <<<EOS
<ul class="fld-media-list-thumbs">{$images}</ul>
<button class="button fld-media-change">{$change_text}</button>
<button class="button fld-media-clear" {$display_none}>Clear</button>
<input class="fld-media-input" type="hidden" name="{$this->name}" value="{$this->value}" />
EOS;
    }
}

class fld_option_date extends fld_option_input {
    public function output() {
        $value = htmlspecialchars($this->value);
        echo <<<EOS
    <input type="date" id="{$this->name}" name="{$this->name}" value="{$value}" size="60" />
EOS;
    }
}

