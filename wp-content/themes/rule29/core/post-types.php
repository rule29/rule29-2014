<?php

// Create Green Custom Post Type
// Register Custom Post Type
function green_case_study() {

	$labels = array(
		'name'                  => _x( 'Green Case Studies', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Green Case Study', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Green Case Studies', 'text_domain' ),
		'name_admin_bar'        => __( 'Green Case Studies', 'text_domain' ),
		'archives'              => __( 'Green Case Study Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Green Case Study:', 'text_domain' ),
		'all_items'             => __( 'All Green Case Studies', 'text_domain' ),
		'add_new_item'          => __( 'Add New Green Case Study', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Green Case Study', 'text_domain' ),
		'edit_item'             => __( 'Edit Green Case Study', 'text_domain' ),
		'update_item'           => __( 'Update Green Case Study', 'text_domain' ),
		'view_item'             => __( 'View Green Case Study', 'text_domain' ),
		'search_items'          => __( 'Search Green Case Study', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Green Case Study', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Green Case Study', 'text_domain' ),
		'items_list'            => __( 'Green Case Studies list', 'text_domain' ),
		'items_list_navigation' => __( 'Green Case Studies list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter Green Case Study list', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                  => 'green-case-studies',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Green Case Study', 'text_domain' ),
		'description'           => __( 'Going Green Case Studies', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'green-case-studies',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'green_case_study', $args );

}
add_action( 'init', 'green_case_study', 0 );
// end green case studies

class custom_post_type {
    public $args;
    public $fields;
    public $names;

    public function __construct($args) {
        $this->args = $args;
        $this->fields = (!empty($args['fields'])) ? $args['fields'] : array();

        $this->set_names();

        add_action( 'init', array($this, 'create_post_type') );
        add_action( 'admin_init', array($this, 'admin_init') );
        add_filter('manage_' . $this->names['post_type'] . '_posts_columns', array($this, 'add_table_columns'));
        add_action('manage_' . $this->names['post_type'] . '_posts_custom_column', array($this, 'columns_content'), 10, 2);
    }

    public function set_names() {
        $names = $this->names = $this->args['names'];

        if (empty($this->names['post_type'])) {
            die('Missing post type name');
            return false;
        }

        $this->names['post_type'] = str_replace(' ', '_', strtolower($this->names['post_type']));
        $this->names['single'] = (!empty($names['single'])) ? $names['single'] : ucfirst(strtolower($names['post_type']));
        $this->names['plural'] = (!empty($names['plural'])) ? $names['plural'] : $this->names['single'] . 's';
        $this->names['slug'] = (!empty($names['slug'])) ? $names['slug'] : $this->names['post_type'];
        $this->names['meta'] = (!empty($names['meta'])) ? $names['meta'] : 'Information';
    }

    public function create_post_type() {
        $names = $this->names;

        $arg_params = array(
            'public'                => true,
            'supports'              => array('title', 'editor', 'thumbnail', 'page-attributes'),
            'has_archive'           => true,
            'hierarchical'          => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'description'           => '',
            'show_in_nav_menus'     => true,
            'rewrite'               => true,
            // implement rewrite
        );

        $args = array(
            'labels' => array(
                    'name' => $names['plural'],
                    'singular_name' => $names['single'],
                    'add_new' => 'Add New ' . $names['single'],
                    'add_new_item' => 'Add New ' . $names['single'],
                    'edit_item' => 'Edit ' . $names['single'],
                    'new_item' => 'New ' . $names['single'],
                    'view_item' => 'View ' . $names['single'],
                    'menu_name' => $names['plural']
                  )
        );

        foreach($arg_params as $param_key => $default) {
            $args[$param_key] = (isset($this->args[$param_key])) ? $this->args[$param_key] : $default;
        }

        register_post_type( $names['post_type'], $args);

        $this->add_table_columns();
    }

    public function admin_init() {
        new fld_custom_post_meta($this->fields, array($this->names['post_type']));
    }

    public function add_table_columns($defaults = array()) {
        if (!empty($this->args['table_columns'])) {
            if (in_array('thumbnail', $this->args['table_columns'])) {
                $defaults['thumbnail'] = 'Thumbnail';
            }
        }
        return $defaults;
    }

    function columns_content($column_name, $post_id) {
        if ($column_name === 'thumbnail') {
            echo get_the_post_thumbnail($post_id, 'thumbnail');
        }
    }
}
