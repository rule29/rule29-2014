FldMedia = function( jq_input ) {
    'use strict';
    var that = this;
    
    that.triggerOpen = function(e) {
        that.open();
        e.preventDefault();
    };
    that.jq_input = jq_input;
    that.jq_change = jq_input.siblings('.fld-media-change').click(that.triggerOpen);
    that.jq_list = jq_input.siblings('.fld-media-list-thumbs').on('click', 'li', that.triggerOpen);
    that.jq_clear = jq_input.siblings('.fld-media-clear').click(function(e) {
        that.jq_list.empty();
        jq_input.val('');
        that.update();
        e.preventDefault();
    });
    
    return this;
};

FldMedia.prototype.update = function() {
    'use strict';
    var that = this;
    if (that.jq_input.val() === '') {
        that.jq_change.text('Add Images');
        that.jq_clear.fadeOut(250);
    } else {
        that.jq_change.text('Change');
        that.jq_clear.fadeIn(250);
    }
};

FldMedia.prototype.open = function() {
    'use strict';
    var that = this;
    
    that.frame().open();
};

FldMedia.prototype.frame = function() {
    'use strict';
    var that = this,
    wp = window.wp,
    image_ids = that.jq_input.val(),
    selection = that.select(),
    opts = {
        title:      'Choose Images',
        multiple:   true,
        frame:     'post',
        state:     'gallery-edit',
        editing:   true
    },
    frame;
    
    if (image_ids !== '') {
        opts.selection = selection;
    }
        
    this.media_frame = frame = wp.media(opts);
    
    this.media_frame.on( 'update', function(e) {
        that.jq_input.val('');
        frame.state().attributes.library.forEach(function(image) {
            that.addImageID(image.id);
            that.update();
        });
        that.updateImageList(that.jq_input.val().split(','));
    });
    
    return this.media_frame;
};

FldMedia.prototype.addImageID = function( id ) {
    'use strict';
    var that = this,
    id_str = that.jq_input.val(),
    ids = (id_str !== '') ? id_str.split(',') : [];
    ids.push(id);
    that.jq_input.val(ids.join(','));
};

FldMedia.prototype.select = function() {
    'use strict';
        
    var that = this,
        wp = window.wp,
        shortcode = wp.shortcode.next( 'gallery', '[gallery ids="' + that.jq_input.val() + '"]' ),
        defaultPostId = wp.media.gallery.defaults.id,
        attachments, 
        selection;
 
        // Bail if we didn't match the shortcode or all of the content.
    if ( ! shortcode ) {
        return;
    }
        
    // Ignore the rest of the match object.
    shortcode = shortcode.shortcode;
 
    if ( _.isUndefined( shortcode.get('id') ) && ! _.isUndefined( defaultPostId ) ) {
        shortcode.set( 'id', defaultPostId );
    }
 
    attachments = wp.media.gallery.attachments( shortcode );
    
    selection = new wp.media.model.Selection( attachments.models, {
        props:    attachments.props.toJSON(),
        multiple: true
    });
     
    selection.gallery = attachments.gallery;
 
    // Fetch the query's attachments, and then break ties from the
    // query to allow for sorting.
    selection.more().done( function() {
        // Break ties with the query.
        selection.props.set({ query: false });
        selection.unmirror();
        selection.props.unset('orderby');
    });
 
    return selection;
};

FldMedia.prototype.updateImageList = function( ids ) {
    'use strict';
    var that = this,
    jQuery = window.jQuery,
    ajaxurl = window.ajaxurl;
    
    jQuery.post(
        ajaxurl,
        {
            action : 'fld_media_preview',
            image_ids: ids
        },
        function( response ) {
            that.addImagesToList(response.urls);
        }
    );
};

FldMedia.prototype.addImagesToList = function( urls ) {
    'use strict';
    var that = this,
    jQuery = window.jQuery,
    url_index = 0,
    num_urls = urls.length;
    
    that.jq_list.empty();
    for (url_index; url_index < num_urls; url_index += 1) {
        that.jq_list.append(jQuery('<li />').append(jQuery('<img />').attr('src', urls[url_index])));
    }
};

jQuery(function() {
    'use strict';
    
    jQuery('.fld-media-input').each(function() {
        jQuery(this).data('fld-media', new FldMedia( jQuery(this) ) );
    });
});
