<?php

$template_dir = get_template_directory();

require_once($template_dir . '/core/fields.php');

require_once($template_dir . '/core/options.php');

require_once($template_dir . '/core/post-types.php');

require_once($template_dir . '/core/meta.php');

require_once($template_dir . '/core/attachments.php');

require_once($template_dir . '/core/taxonomy.php');

require_once($template_dir . '/core/user.php');
