<?php

class fld_site_option_section {

    public static $section_count = 1;

    var $args = array(
        'name'          => 'section_name',
        'label'         => 'Label that appears as title of section',
        'description'   => 'Description of section',
        'page'          => 'wp_admin_page_to_appear_on'
    );

    function __construct( $args = array() ) {
        if (empty($args['name'])) {
            return;
        }

        $args['label']          = (!empty($args['label'])) ? $args['label'] : 'Options Section ' . self::$section_count;
        $args['description']    = (!empty($args['description'])) ? $args['description'] : '';
        $args['page']           = (!empty($args['page'])) ? $args['page'] : 'general';

        $this->args = $args;

        add_action('admin_init', array($this, 'settings_api_init'));

        self::$section_count += 1;
    }

    function settings_api_init() {
        $args = $this->args;

        add_settings_section(
            $args['name'],
            $args['label'],
            array($this, 'section_description'),
            $args['page']
        );
    }

    function section_description() {
        echo $this->args['description'];
    }
}

class fld_site_option {

    public static $field_count = 1;

    var $args = array(
        'name'      => 'field_name',
        'label'     => 'Label for field',
        'type'      => 'type_of_field',
        'page'      => 'wp_page',
        'section'   => 'section_name'
    );

    function __construct( $args = array() ) {
        if (empty($args['section'])) {
            return;
        }

        $args['name'] = (!empty($args['name'])) ? $args['name'] : 'fld_option_' . self::$field_count;
        $args['type'] = (!empty($args['type'])) ? $args['type'] : 'text';
        $args['label'] = (!empty($args['label'])) ? $args['label'] : $args['type'] . ' ' . self::$field_count;
        $args['page'] = (!empty($args['page'])) ? $args['page'] : 'general';

        $this->args = $args;

        add_action('admin_init', array($this, 'settings_api_init'));

        self::$field_count += 1;
    }

    function settings_api_init() {
        $args = $this->args;

        add_settings_field(
            $args['name'],
            $args['label'],
            array($this, 'create_input'),
            $args['page'],
            $args['section']
        );

        register_setting(
            $args['page'],
            $args['name']
        );
    }

    function create_input() {
        fld_create_field($this->args, get_option($this->args['name']));
    }
}

class fld_site_page {
    var $args = array(
        'title'         => 'Title for Page',
        'menu_title'    => 'Title for Menu',
        'slug'          => 'menu_slug',
    );

    function __construct( $args = array() ) {
        add_action( 'admin_menu', array($this, 'register_menu_page') );

        if ( empty( $args['slug'] ) ) {
            return false;
        }

        $args['title'] = ( ! empty( $args['title'] ) )
            ? $args['title'] : ucfirst($args['slug']);
        $args['menu_title'] = ( ! empty( $args['menu_title'] ) )
            ? $args['menu_title'] : $args['title'];

        $this->args = $args;
    }

    function register_menu_page() {
        $args = $this->args;

        add_menu_page(
            $args['title'],
            $args['menu_title'],
            'manage_options',
            $args['slug'],
            array($this, 'output')
        );

    }

    function output() {
        $args = $this->args;
        ?>
        <form action="options.php" method="post">
        <?php settings_fields( $args['slug'] ); ?>
        <?php do_settings_sections( $args['slug'] ); ?>
        <?php submit_button(); ?>
        </form>
        <?php
    }
}
