<?php

class attachment_meta {
	var $name;
	var $label;

	function __construct( $args ) {
		if ( ! empty( $args['name'] ) ) {
			$this->name = $args['name'];
			$this->label = ( ! empty( $args['label'] ) ) 
				? $args['label'] : ucfirst( $args['name'] );

			add_filter( 'attachment_fields_to_edit', array( $this, 'add_attachment_field' ), 10, 2 );
			add_action( 'edit_attachment', array( $this, 'save_attachment_field' ) );
		}
	}

	function add_attachment_field( $form_fields, $post ) {
		$field_value = get_post_meta( $post->ID, $this->name, true );
	    $form_fields[ $this->name ] = array(
	        'value' => $field_value ? $field_value : '',
	        'label' => $this->label,
	    );
	    return $form_fields;
	}

	function save_attachment_field( $attachment_id ) {
		if ( isset( $_REQUEST['attachments'][$attachment_id][ $this->name ] ) ) {
	        $location = $_REQUEST['attachments'][$attachment_id][ $this->name ];
	        update_post_meta( $attachment_id, $this->name, $location );
	    }
	}
}