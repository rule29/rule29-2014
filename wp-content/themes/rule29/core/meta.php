<?php

class fld_custom_post_meta {
    var $fields;
    var $post_types;
    var $label;
    var $this_index;

    public static $index = 0;

    function __construct( $fields, $post_types = array( 'page' ), $label = '' ) {
        $label = ( ! empty( $label ) )
            ? $label : 'Information ' . self::$index;
        $this->fields       = $fields;
        $this->post_types   = $post_types;
        $this->label        = $label;
        $this->this_index   = self::$index;

        add_action( 'add_meta_boxes', array($this, 'add_meta'));
        add_action( 'save_post', array($this, 'save_meta') );
        self::$index++;
    }

    function add_meta() {
        if (!empty($this->fields)) {
            foreach($this->post_types as $post_type) {
                add_meta_box(
                    'fld-page-meta-content-' . $this->this_index,
                    $this->label,
                    array($this, 'meta_content'),
                    $post_type,
                    'normal',
                    'high'
                );
            }
        }
    }

    function meta_content( $post ) {
        wp_nonce_field( 'fld_custom_meta', 'page_nonce' );

        echo '<table class="form-table">';

        foreach($this->fields as $field) {
            if ( empty( $field['templates'] ) || $this->is_template( $field['templates'], $post->ID ) ) {
                echo '<tr>';

                echo '<th>';
                echo '<label for="' . $field['name'] . '">' . $field['label'] . '</label>';
                echo '</th>';

                echo '<td>';
                fld_create_field(
                    $field, // field args
                    get_post_meta( $post->ID, $field['name'], true) // value for field
                );

                echo '</td>';
                echo '</tr>';
            }
        }

        echo '</table>';
    }

    /**
     * returns whether fields active for this template
     * @param  array  $templates  array of template names to activate
     * @return boolean            [description]
     */
    function is_template( $templates, $post_id ) {
        $valid = false;

        foreach ( $templates as $template ) {
            switch( $template ) {
                case 'front-page' :
                    if ( intval( $post_id ) === intval( get_option( 'page_on_front' ) ) ) {
                        $valid = true;
                    }
                    break;
            }
            if ( get_post_meta( $post_id, '_wp_page_template', true ) === $template ) {
                $valid = true;
            }
        }

        return $valid;
    }

    function save_meta( $post_id ) {
        if ( !isset($_POST['page_nonce']) || !wp_verify_nonce( $_POST['page_nonce'], 'fld_custom_meta' )) {
            return $post_id;
        }

        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
            return $post_id;

        foreach($this->fields as $field) {
            $value = fld_save_field( $field );
            update_post_meta( $post_id, $field['name'], $value );
        }

        return $post_id;
    }
}
