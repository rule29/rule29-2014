<?php

class fld_custom_user_field {
	public $fields;
	public $taxonomy_name;

	function __construct( $fields ) {
		$this->fields = $fields;

		add_action( 'init', array($this, 'init') );

		add_action( 'show_user_profile', array( &$this, 'fields' ) );
		add_action( 'edit_user_profile', array( &$this, 'fields' ) );

		add_action( 'personal_options_update', array( &$this, 'save' ) );
		add_action( 'edit_user_profile_update', array( &$this, 'save' ) );
	}

	public function init() {
		add_action( $this->taxonomy_name . '_edit_form_fields', array($this, 'display'), 10, 2);
		add_action( 'edited_' . $this->taxonomy_name, array($this, 'save'), 10, 2);

		global $wpdb;
		$wpdb->termmeta = $wpdb->prefix."termmeta";
	}

	public function fields( $user ) {
		echo '<table class="form-table"><tbody>';
		foreach($this->fields as $fld) {
			echo <<<EOS
<tr class="form-field">
	<th scope="row" valign="top"><label for="{$fld['name']}">{$fld['label']}</label></th>
	<td>
EOS;

			$value = get_user_meta( $user->ID, $fld['name'], true );
			fld_create_field($fld, $value);

			echo <<<EOS
	</td>
</tr>
EOS;
		}
		echo '</tbody></table>';
	}

	public function save( $user_id ) {
		if ( ! $user_id )
			return;

        foreach($this->fields as $field) {
            $value = fld_save_field( $field, $fld );

        	update_user_meta( $user_id, $field['name'], $value );
        }
	}
}
