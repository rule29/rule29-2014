<?php get_header(); ?>
<?php rule29_featured_image(); ?>
<?php if ( have_posts() ) { ?>
<?php while ( have_posts() ) { ?>
<?php the_post(); ?>
<section class="section-gray section-content">
	<div class="wrap cf">
		<div class="col-1-6 col-tb-1-4">
			<?php rule29_author_image(); ?>
			<?php rule29_list_categories(); ?>
			<?php rule29_social_buttons(); ?>
		</div>
		<div class="col-2-3 col-tb-3-4">
			<div class="col-inner">
				<article class="main-content">
					<?php the_content(); ?>
				</article>
				<?php rule29_list_tags(); ?>
				<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="620"></div>
			</div>
		</div>
	</div>
</section>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=300619216746699";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php } // endwhile posts ?>
<?php } // endif posts ?>
<?php get_footer(); ?>
