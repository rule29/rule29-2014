<?php /* Template Name: Contact Page */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) { ?>
<?php the_post(); ?>
<?php } ?>
<?php rule29_featured_image( get_the_ID(), 'image-featured', array( 'text' => get_the_content() ) ); ?>
<section class="section-gray section-content">
	<div class="wrap cf">
		<div class="col-1-6">
		</div>
		<div class="col-2-3 contact-content">
			<div class="col-1">
				<?php if ( ! empty( $_GET['success'] ) ) { ?>
					<h2><em>Thank you for contacting us. We will be in touch shortly.</em></h2>
				<?php } ?>
				<div class="selector selector-form">
					<div class="selector-trigger">Select</div>
					<div class="selector-dropdown">
						<ul class="selector-options">
							<li class="selector-option"><a href="#/client">I'm a Client</a></li>
							<li class="selector-option"><a href="#/job">I'm a Job Hunter</a></li>
							<li class="selector-option"><a href="#/friend">I'm an Old Friend</a></li>
							<li class="selector-option"><a href="#/general">I'm a Person Who Can't Be Defined by Categories</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="forms-contact">
				<form action="<?php fld_form_submit_uri(); ?>" method="post" data-form="client" class="default-form form-contact form-ajax">
					<?php fld_form_hidden_fields( 1 ); 
					//get_flood_captcha();
					?>
					<div class="col-1">
						<h2><em>We have a feeling that this could be the beginning of a beautiful relationship.</em></h2>
						<p>We’d love to hear all about what you do and where you want to go.</p>
						<p><em>(All fields marked with an asterik are required)</em></p>
					</div>
					<div class="col-1">
						<input type="text" name="name" required="required" class="input-text" placeholder="Name*">
					</div>
					<div class="col-1">
						<input type="text" name="title" class="input-text" placeholder="Title">
					</div>
					<div class="col-1">
						<input type="email" name="email" required="required" class="input-text" placeholder="Email*">
					</div>
					<div class="cf">
						<div class="col-1-2">
							<input type="text" name="phone" required="required" placeholder="Phone*" class="input-text">
						</div>
						<div class="col-1-2">
							<input type="text" name="company" required="required" placeholder="Company*" class="input-text">
						</div>
					</div>
					<div class="col-1">
						<h5>What type of project are you interested in talking about? (Select one, select them all)</h5>
					</div>
					<ul class="list-interests cf">
						<?php rule29_capabilities_checkboxes(); ?>
					</ul>
					<div class="col-1">
						<textarea name="message" cols="30" rows="10" placeholder="Message*" required="required" class="input-textarea"></textarea>
					</div>
					<div class="col-1">
						<h5>Time to prove that you're not a robot. Enter the characters you see in the image below</h5>
					</div>
					<div class="col-1 captcha">
						
					</div>
					<button class="button button-primary pull-right">Submit</button>
				</form>
				<form action="<?php fld_form_submit_uri(); ?>" method="post" data-form="job" class="form-contact" enctype="multipart/form-data">
					<?php fld_form_hidden_fields( 2 ); //get_flood_captcha(); ?>
					<div class="col-1">
						<h2><em>Do you want in on making some Creative Matter&reg;?</em></h2>
						<p>Thanks for checking to see if you can help us make Creative Matter. We’re not always hiring, but we are definitely always interested in meeting new people.</p>
						<p><em>(All fields marked with an asterik are required)</em></p>
					</div>
					<div class="col-1">
						<input type="text" name="name" required="required" class="input-text" placeholder="Name*">
					</div>
					<div class="col-1">
						<input type="email" name="email" required="required" class="input-text" placeholder="Email*">
					</div>
					<div class="cf">
						<div class="col-1-2">
							<input type="text" name="phone" required="required" placeholder="Phone*" class="input-text">
						</div>
						<div class="col-1-2">
							<input type="text" name="website" required="required" placeholder="Website*" class="input-text">
						</div>
					</div>
					<div class="col-1">
						<h5>Attach a PDF of your Work*</h5>
						<input type="file" name="work" required="required" class="input-file">
					</div>
					<div class="col-1">
						<textarea name="message" cols="30" rows="10" placeholder="Message*" required="required" class="input-textarea"></textarea>
					</div>
					<div class="col-1">
						<h5>Time to prove that you're not a robot. Enter the characters you see in the image below</h5>
					</div>
					<div class="col-1 captcha"></div>
					<button class="button button-primary pull-right">Submit</button>
				</form>
				<form action="<?php fld_form_submit_uri(); ?>" method="post" data-form="friend" class="form-contact form-ajax">
					<?php fld_form_hidden_fields( 3 );// get_flood_captcha(); ?>
					<div class="col-1">
						<h2><em>Hi! Long time, no see. You look great.</em></h2>
						<p>Where have you been? What’s going on? Don’t you owe us lunch? …Or are you just trying to find out if we’re making cookies today?</p>
						<p><em>(All fields marked with an asterik are required)</em></p>
					</div>
					<div class="col-1">
						<input type="text" name="name" required="required" class="input-text" placeholder="Name*">
					</div>
					<div class="col-1">
						<input type="email" name="email" required="required" class="input-text" placeholder="Email*">
					</div>
					<div class="col-1">
						<textarea name="message" cols="30" rows="10" placeholder="Message*" required="required" class="input-textarea"></textarea>
					</div>
					<div class="col-1">
						<h5>Time to prove that you're not a robot. Enter the characters you see in the image below</h5>
					</div>
					<div class="col-1 captcha"></div>
					<button class="button button-primary pull-right">Submit</button>
				</form>
				<form action="<?php fld_form_submit_uri(); ?>" method="post" data-form="general" class="form-contact form-ajax">
					<?php fld_form_hidden_fields( 4 ); //get_flood_captcha(); ?>
					<div class="col-1">
						<h2><em>Well hello there!</em></h2>
						<p>We don't want to leave anyone out. If none of the categories above are a good fit, don't fret, this is just the place for you!</p>
						<p><em>(All fields marked with an asterik are required)</em></p>
					</div>
					<div class="col-1">
						<input type="text" name="name" required="required" class="input-text" placeholder="Name*">
					</div>
					<div class="col-1">
						<input type="email" name="email" required="required" class="input-text" placeholder="Email*">
					</div>
					<div class="col-1">
						<textarea name="message" cols="30" rows="10" placeholder="Message*" required="required" class="input-textarea"></textarea>
					</div>
					<div class="col-1">
						<h5>Time to prove that you're not a robot. Enter the characters you see in the image below</h5>
					</div>
					<div class="col-1 captcha"></div>
					<button class="button button-primary pull-right">Submit</button>
				</form>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
<?php rule29_captcha_front(); ?>