</div>
<section class="section-newsletter">
	<div class="wrap">
		<div class="cf">
			<div class="col-1">
				<label class="trigger-form-newsletter" for="input-newsletter-first-name">Want to keep up to date on all things Rule29? We know you do, signup and we’ll keep you updated</label>
			</div>
		</div>
		<form action="javascript:newsletter.submit()" class="form-newsletter" method="post">
			<div class="form-row cf">
				<div class="col-1-4">
					<input type="text" name="first-name" placeholder="First Name" required="required" class="input-newsletter" id="input-newsletter-first-name">
				</div>
				<div class="col-1-4">
					<input type="text" name="last-name" placeholder="Last Name" required="required" class="input-newsletter"  id="input-newsletter-last-name">
				</div>
				<div class="col-1-2 form-column">
					<input type="email" name="email" placeholder="Email" required="required" class="input-newsletter"  id="input-newsletter-email">
					<input type="submit" class="submit-newsletter">
				</div>
			</div>
			<div class="form-row cf">
				<div class="col-1-6">
					<input type="checkbox" name="list-newsletters" value="1" class="check-newsletter" id="list-newsletters" checked="checked">
					<label for="list-newsletters" class="checkbox-newsletter"></label>
					<label for="list-newsletters" class="label-newsletter">Newsletters</label>
				</div>
				<div class="col-1-6">
					<input type="checkbox" name="list-creative-matters" value="1" class="check-newsletter" id="list-creative-matters" checked="checked">
					<label for="list-creative-matters" class="checkbox-newsletter"></label>
					<label for="list-creative-matters" class="label-newsletter">Creative Matters</label>
				</div>
				<div class="col-1-6">
					<input type="checkbox" name="list-blog-updates" value="1" class="check-newsletter" id="list-blog-updates" checked="checked">
					<label for="list-blog-updates" class="checkbox-newsletter"></label>
					<label for="list-blog-updates" class="label-newsletter">Blog Updates</label>
				</div>
			</div>
		</form>
	</div>
</section>
<footer class="footer-primary">
	<div class="wrap cf">
		<?php
			if (VERSION < VERSION_THREE) {
?>
		<div class="col-1-6 phone-stack">
			<h1 class="logo">Rule29</h1>
			<address>
				<p><strong>501 Hamilton St<br>Geneva, IL 60134</strong></p>
				<p><span class="white">T</span> 630 262 1009<br><span class="white">F</span> 630 447 0043</p>
			</address>
		</div>
		<div class="col-1-2 phone-stack copyright">
			<p>&copy;2000-<?php echo date('Y'); ?> Rule29 Creative, Inc. All rights reserved.</p>
			<p>&ldquo;Rule29 Creative&rdquo;, &ldquo;Rule29&rdquo;, &ldquo;R29&rdquo;, and the R29 mark are service marks of Rule29 Creative.</p>
			<p>Creative Matters&reg; &amp; Making Creative Matter&reg; are registered trademarks of Rule29.</p>
			<p>All Rights Reserved.</p>
		</div>
<?php
			} else {
		?>
		<div class="phone-stack col-custom-footer" style="float: left; width: 230px; margin-right: 85px">
			<h1 class="logo">Rule29</h1>
			<address style="float: left">
				<p><strong>501 Hamilton St<br>Geneva, IL 60134</strong></p>
				<p><span class="white">T</span> 630 262 1009<br><span class="white">F</span> 630 447 0043</p>
			</address>
			<address style="float: left; margin-left: 28px;">
				<p><strong>366 N Second Ave<br>Phoenix, AZ 85003</strong></p>
				<p><span class="white">T</span> 602 820 7023
			</address>
			<div style="clear:both"></div>
		</div>
		<div class="phone-stack copyright col-custom-footer" style="float: left; width: 314px; min-height: 135px;">
			<p>&copy;2000-<?php echo date('Y'); ?> Rule29 Creative, Inc. All rights reserved.</p>
			<p>&ldquo;Rule29 Creative&rdquo;, &ldquo;Rule29&rdquo;, &ldquo;R29&rdquo;, and the R29 mark are service marks of Rule29 Creative.</p>
			<p>Creative Matters&reg; &amp; Making Creative Matter&reg; are registered trademarks of Rule29.</p>
			<p>All Rights Reserved.</p>
		</div>
		<?php
			}
		?>
		<?php wp_nav_menu( array(
			'theme_location'  => 'social-footer',
			'container'       => 'div',
			'container_class' => 'col-custom-footer-right phone-stack',
			'menu_class'      => 'menu menu-social cf',
			'depth'           => 1, ) ); ?>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>