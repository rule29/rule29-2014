<?php get_header(); ?>
<?php rule29_slider( get_the_ID(), 'full', false, array(), true, true ); ?>
<section class="section-white home-content">
	<div class="wrap">
		<div class="cf">
			<div class="col-1-6"></div>
			<div class="col-2-3">
				<?php if ( have_posts() ) { ?>
				<?php while ( have_posts() ) { ?>
				<?php the_post(); ?>
				<?php the_content(); ?>
				<?php } ?>
				<?php } // endif have posts ?>
			</div>
		</div>
	</div>
</section>
<section class="section-gray">
	<div class="wrap">
		<div class="grid grid-home cf">
			<?php rule29_front_grid(); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>