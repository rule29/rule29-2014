<?php get_header(); ?>
<?php 
if (rule29_slider( get_the_ID(), 'full', false)){
	rule29_slider( get_the_ID(), 'full', false);
}else{
	rule29_featured_image();	
} 

?>
<?php if ( have_posts() ) { ?>
<?php while ( have_posts() ) { ?>
<?php the_post(); ?>
<section class="section-gray section-content">
	<div class="wrap cf">
		<div class="col-1-6 col-tb-1-4">
			<?php rule29_subnav(); ?>
		</div>
		<div class="col-2-3 col-tb-3-4">
			<div class="col-inner">
				<article class="main-content">
					<?php the_content(); ?>
				</article>
			</div>
		</div>
		<div class="col-1-6 col-tb-1">
			<?php rule29_social_nav(); ?>
			<?php rule29_related_nav(); ?>
		</div>
	</div>
</section>
<?php } // endwhile posts ?>
<?php } // endif posts ?>
<?php get_footer(); ?>
