<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <script type="text/javascript" src="//use.typekit.net/ntd6ejn.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <html xmlns:fb="http://www.facebook.com/2008/fbml">
    <title><?php rule29_title(); ?><?php echo get_bloginfo( 'name' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>

    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-354326-1']);
      _gaq.push(['_trackPageview']);
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>

    <!-- HitTail Code -->
    <script type="text/javascript">
    (function(){ var ht = document.createElement('script');ht.async = true;
      ht.type='text/javascript';ht.src = '//103355.hittail.com/mlt.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ht, s);})();
    </script>

	<!-- Facebook Remarketing Code -->
	<script>(function(){
	  window._fbds = window._fbds || {};
	  _fbds.pixelId = 446593295473248;
	  var fbds = document.createElement('script');
	  fbds.async = true;
	  fbds.src = '//connect.facebook.net/en_US/fbds.js';
	  var s = document.getElementsByTagName('script')[0];
	  s.parentNode.insertBefore(fbds, s);
	})();
	window._fbq = window._fbq || [];
	window._fbq.push(["track", "PixelInitialized", {}]);
	</script>
	<noscript><img height="1" width="1" border="0" alt="" style="display:none" src="https://www.facebook.com/tr?id=446593295473248&amp;ev=NoScript" /></noscript>

</head>

<body <?php body_class( 'allow-phone allow-tablet' ); ?>>
<?php if (VERSION == VERSION_ONE) { ?> 
<header class="header-primary">
    <div class="wrap">
        <div class="trigger-menu phone">Menu</div>
    </div>
    <nav class="nav-primary">
        <div class="wrap">
            <?php wp_nav_menu( array(
                'theme_location'  => 'nav-primary',
                'container'       => false,
                'menu_class'      => 'menu cf',
                'depth'           => 2,
                'walker'          => new Rule29_Walker_Nav_Menu() ) ); ?>
        </div>
    </nav>
</header>
<?php } elseif (VERSION > VERSION_ONE) { ?>
	<header class="header-primary">
	    <div class="trigger-menu phone"><img src="<?php echo get_template_directory_uri(); ?>/assets/hamburger-icon.png" alt="Menu"/></div>
	    <nav class="nav-primary">
	    	<div class="wrap">
		    	<dir class="phone-logo">
		    		<img src="<?php echo get_template_directory_uri(); ?>/assets/phone-logo.png" alt="Rule 29"/>
		    	</dir>
	            <?php wp_nav_menu( array(
	                'theme_location'  => 'nav-primary',
	                'container'       => false,
	                'menu_class'      => 'menu cf',
	                'depth'           => 2,
	                'walker'          => new Rule29_Walker_Nav_Menu() ) ); ?>
	            <div class="nav-phone-info">
	            	<p>501 Hamilton St<br />
	            	Geneva, IL 60134</p>
	            	<p><span>T</span> 630 262 1009</p>
	            	<ul>
	            		<li id="fb"><a href="#"></a></li>
	            		<li id="tw"><a href="#"></a></li>
	            		<li id="inst"><a href="#"></a></li>
	            		<li id="link"><a href="#"></a></li>
	            	</ul>
	            	<div class="phone-copyright-info">
	            		<p>&copy;2000-<?php echo date(Y); ?> Rule29 Creative, Inc. All rights reserved.</p>
	            		<p>"Rule29 Creative", "Rule29", "R29", and the R29 mark are service marks of Rule29 Creative.</p>
	            		<p>Creative Matters&reg; &amp; Making Creative Matter&reg; are registered trademarks of Rule29.</p>
	            		<p>All Rights Reserved.</p>
	            	</div>
	            </div>
            </div>
	    </nav>
	</header>	
<?php } ?>
<div class="sections">
