<?php get_header(); ?>
<?php $image = wp_get_attachment_image_src( get_option( 'rule29_404_image' ), 'image-page' );

	if ( ! empty( $image ) ) {
		$ratio = $image[2] / $image[1];

		$text = 'Oh no! It looks like you’ve come across a dead-end. We’re terribly sorry about that.'
			. '<br> Try finding your destination through the menu or <a href="'
			. get_permalink( get_option( 'rule29_contact_page' ) )
			. '">send us a message</a> to let us know<br> what’s missing.';

		$slide = array(
			'title' 	=> 'Hang in There!',
			'image' 	=> $image[0],
			'text' 		=> $text,
			'show_text' => true,
		);

		$slides = array( $slide );
		rule29_slider_output( $slides, array(), $ratio );
	}
?>
<?php get_footer(); ?>
