<?php
/*
 * Template Name: Downloads Archive
 */
 	
 	$dir = get_template_directory();
    wp_enqueue_style('downloads-style', '/temp-ui/downloads.css', array('rule29'));
 	
    get_header();
    
    //rule29_slider( get_option( 'rule29_creative_matter_page' ), 'full', true, array(), true );
    rule29_featured_image();
 ?>
<section class="section-gray section-content">
    <div class="wrap cf">
        <div class="col-1-6 col-tb-1-4">
            <?php rule29_subnav(); ?>
        </div>
        <div class="col-5-6 col-tb-3-4">
            <div class="col-inner">
                <article class="main-content">
                    <?php
                        $posts = get_posts(array(
                            'post_type' => 'downloads',
                            'orderby' => 'post_date',
                            'order' => 'DESC',
                            'posts_per_page' => -1
                        ));
                        
                
                        foreach($posts as $p) {
                            global $post;
                            $post = $p;                            
                            
                            $url = get_post_meta($p->ID, 'download_link', true);

                            if (!empty($url))
                                echo '<a href="' . $url . '">';
                    ?>                    
                    <div class="box">
                    	<div class="over">
                    		<p><?php echo $p->post_title; ?></p>
                    	</div>
                    	<?php
                    	   the_post_thumbnail('download-thumbnail');
                    	?>
                    </div>
                    <?php
                            if (!empty($url))
                                echo '</a>';
                        }
                    ?>
                </article>
            </div>
        </div>
    </div>
</section>
<?php
    get_footer();