var RecaptchaOptions = {
    theme : 'clean'
};

function render_twitter_quote(jq, result) {
    var quote = jQuery('<blockquote/>');
    quote.append(jQuery('<p/>').html(result.text));
    quote.append(
        jQuery('<p class="author-tweet"/>').append(
            jQuery('<a target="_blank"/>')
                .attr('href','https://www.twitter.com/' + result.screen_name)
                .html('&ndash;@' + result.screen_name)
        )
    );
    jq.after(quote);
    jq.remove();
}

function render_twitter(jq, result) {
    jq.append(jQuery('<p class="twitter-text"/>').html(result.text));
    jq.append(jQuery('<p class="twitter-user"/>').html('&ndash;@<a href="http://www.twitter.com/' + result.screen_name + '" target="_blank">' + result.screen_name + '</a>'));
    jq.append(jQuery('<div class="grid-icon time-twitter"/>').text('Posted ' + result.tweet_age));
    jQuery('.grid').masonry();
}

function render_lastfm(jq, result) {
    var songs = result.songs,
        song_index = 0,
        songs_len = songs.length,
        output_songs = [],
        this_song;
    for( song_index; song_index < songs_len; song_index += 1 ) {
        this_song = songs[song_index];
        output_songs.push('<li class="lastfm-item"><p class="lastfm-title">' + this_song.song + '</p><p class="lastfm-artist">' + this_song.artist + '</p></li>');
    }
    jq.html(output_songs.join(''));
    jQuery('.grid').masonry();
}

function render_instagram(jq, result) {
    var link,
        img,
        date,
        index = jq.data('index'),
        this_image_data;

    if (jQuery.isArray(result.media) && result.media[index]) {
        this_image_data = result.media[index];
        img = jQuery('<img class="instagram-image" />').attr('src', this_image_data.images.standard_resolution.url).load(function () {
            jQuery('.grid').masonry();
        });
        date = jQuery('<div class="grid-icon time-instagram"/>').text('Posted ' + this_image_data.human_time + ' ago');
        link = jQuery('<a target="_blank" />').attr('href', this_image_data.link).append(img).append(date);
        jq.append(link);
    }
}

;(function ( $, window, undefined ) {
    'use strict';

    var pluginName = "slider",
        defaults = {
            images: ".images",
            image:  ".image",
            texts:  ".texts",
            text:   ".text",
            indicators: ".indicators",
            ratio:  0.46666666,
            minWidth: 320,
            duration: 500,
            indicatorActive: "indicator-active"
        };

    function Plugin(element, options) {
        this.element = element;

        this.options = $.extend({}, defaults, options);

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype.init = function () {
        var that        = this,
            options     = that.options,
            container   = $(that.element),
            images      = container.find(options.images),
            image       = container.find(options.image),
            texts       = container.find(options.texts),
            text        = container.find(options.text),
            indicators  = container.find(options.indicators),
            prev        = $('<div class="prev"/>'),
            next        = $('<div class="next"/>');

        that.container  = container;
        that.images     = images;
        that.image      = image;
        that.texts      = texts;
        that.text       = text;
        that.indicators = indicators;
        that.pos        = 0;

        image.hide().first().show();
        text.hide().first().show();

    	var do_auto_move = container.data('auto-movement') == 1;
    	var auto_move_timer = 0;
    	function auto_move() {
    		if (do_auto_move) {
        		clearTimeout(auto_move_timer);
	    		auto_move_timer = setTimeout(function() {
    				that.goTo(that.nextIndex());
    				auto_move();
   				}, 6000);
   			} 
    	}
    	

        if (image.length > 1) {
            image.each(function () {
                indicators.append($('<li class="indicator"></li>'));
            });
            next.click(function () {
                that.goTo(that.nextIndex());
                auto_move();
            });
            prev.click(function () {
                that.goTo(that.nextIndex(true));
                auto_move();
            });
            container.prepend(prev).append(next);
            
            auto_move();
        }

        that.setIndicator(0);

        indicators.children().click(function () {
            that.goTo($(this).index());
             auto_move();
        });


        that.resize();
        $(window).resize(function () {
            that.resize();
        });
    };

    Plugin.prototype.goTo = function (index) {
        var that = this,
            image = that.image,
            text = that.text,
            options = that.options,
            duration = options.duration;

        if (index === that.pos) {
            return false;
        }

        that.setIndicator(index);

        text.add(image).fadeOut(duration);
        text.eq(index).add(image.eq(index)).stop(true).fadeIn(duration, function () {
            that.pos = index;
        });
    };

    Plugin.prototype.nextIndex = function (backwards) {
        var that = this,
            current_index = that.pos,
            next_index = current_index + 1,
            last_index = that.image.length - 1;

        if (backwards) {
            next_index = current_index - 1;
        }

        if (next_index > last_index) {
            next_index = 0;
        } else if (next_index < 0) {
            next_index = last_index;
        }
        return next_index;
    };

    Plugin.prototype.setIndicator = function (index) {
        var that = this,
            indicators = that.indicators,
            active = that.options.indicatorActive;

        indicators.children().removeClass(active).eq(index).addClass(active);
    };

    Plugin.prototype.resize = function () {
        var that = this,
            minWidth = that.options.minWidth,
            winWidth = $(window).width(),
            sliderWidth = minWidth;

        if (winWidth > minWidth) {
            sliderWidth = winWidth;
        }
        $(that.element).height(sliderWidth * that.options.ratio);
    };

    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if ( !$.data(this, pluginName )) {
                $.data( this, pluginName, new Plugin( this, options ) );
            }
        });
    };
} ( jQuery, window ) );

;(function ( $, window, undefined ) {
    'use strict';

    var pluginName = "projectListing",
        defaults = {
        };

    function Plugin(element, options) {
        this.element = element;

        this.options = $.extend({}, defaults, options);

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype.init = function () {
        var that        = this;

        that.container = $(that.element);

        that.capability = 'all';
        that.client = 'all';

        $('[data-client]').click(function (e) {
            that.setClient($(this).data('client'));
            that.setHash();
            e.preventDefault();
        });

        $('[data-capability]').click(function (e) {
            that.setCapability($(this).data('capability'));
            that.setHash();
            e.preventDefault();
        });

        that.hashChange();
        $(window).on('hashchange', function () {
            that.hashChange();
        });
    };

    Plugin.prototype.setClient = function (client) {
        if (!client || client === '') {
            client = 'all';
        }
        this.client = client;
    };

    Plugin.prototype.setCapability = function (capability) {
        if (!capability || capability === '') {
            capability = 'all';
        }
        this.capability = capability;
    };

    Plugin.prototype.setHash = function () {
        var that = this;

        window.location.hash = '#/' + that.client + '/' + that.capability;
    };

    Plugin.prototype.hashChange = function () {
        var that = this,
            hash_parts = window.location.hash.split('/');
        if (!hash_parts[1] || !hash_parts[2]) {
            return;
        }
        that.client = hash_parts[1];
        that.capability = hash_parts[2];
        that.loadPosts();
    };

    Plugin.prototype.loadPosts = function () {
        var that = this;
        $.post(window.ajaxurl, {
            action: 'creativematter',
            client: that.client,
            capability: that.capability
        }, function (response) {
            if ( response ) {
                that.container.html( response );
            }
        });
    };

    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if ( !$.data(this, pluginName )) {
                $.data( this, pluginName, new Plugin( this, options ) );
            }
        });
    };
} ( jQuery, window ) );

;(function ( $, window, undefined ) {
    'use strict';

    var pluginName = "contactForms",
        defaults = {
        };

    function Plugin(element, options) {
        this.element = element;

        this.options = $.extend({}, defaults, options);

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype.init = function () {
        var that        = this;

        that.container = $(that.element);
        that.forms = that.container.find('.form-contact');

        that.hashChange();
        $(window).on('hashchange', function () {
            that.hashChange();
        });
    };

    Plugin.prototype.hashChange = function () {
        var that = this,
            hash_parts = window.location.hash.split('/');
        if (!hash_parts[1]) {
            return;
        }
        that.switchForm(hash_parts[1]);
    };

    Plugin.prototype.switchForm = function (slug) {
        var that = this,
            form = $('[data-form=' + slug + ']');

        if ( form.length === 0 ) {
            form = $('[data-form=general]');
        }

        form.find('.captcha').append(that.container.find('#recaptcha_widget_div'));
        that.forms.not(':eq(' + form.index() + ')').fadeOut(500, function() {
            form.fadeIn(500);
        });
    };

    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if ( !$.data(this, pluginName )) {
                $.data( this, pluginName, new Plugin( this, options ) );
            }
        });
    };
} ( jQuery, window ) );

jQuery( function () {
    jQuery('.slider').each(function() {
        jQuery(this).slider({
            ratio: jQuery(this).data('ratio')
        });
    });

    jQuery('.list-projects').projectListing();

    //jQuery('.trigger-menu').click(function () {
    //    jQuery('.nav-primary').toggleClass('nav-primary-active');
    //});
    jQuery('.nav-primary a').click(function () {
        jQuery('.nav-primary').removeClass('nav-primary-active');
    });

    jQuery('.selector-trigger').click(function () {
        jQuery(this).closest('.selector').toggleClass('selector-active');
    });
    jQuery('.selector-option').click(function () {
        jQuery(this).closest('.selector').removeClass('selector-active');
        jQuery(this).closest('.selector').find('.selector-trigger').text(jQuery(this).text());
    });

    jQuery('.trigger-filters').click(function () {
        jQuery('.filters').slideToggle(250);
    });

    jQuery('.grid').masonry({
        columnWidth: '.grid-cell-1',
        gutter: 0
    }).imagesLoaded( function () {
        jQuery('.grid').masonry();
    });

    jQuery('.list-posts').masonry().imagesLoaded(function () {
        jQuery('.list-posts').masonry();
    });

    jQuery('.list-news').masonry().imagesLoaded(function () {
        jQuery('.list-news').masonry();
    });

    jQuery('.forms-contact').contactForms();

    jQuery('form').each(function () {
        jQuery(this).validate({
            errorPlacement: function() {}
        });
    });

    // moves captcha into from from outside of it due to weird effects from iframe
    jQuery('#recaptcha_widget_div').appendTo(jQuery('.captcha').first());

    jQuery('.trigger-form-newsletter').click(function () {
        jQuery('.form-newsletter').slideDown(250);
    });

    jQuery('.form-ajax').submit(function ( e ) {
        if ( jQuery(this).valid() ) {
            fld_form_logger_ajax.submit(this, function (result) {
                console.log(result);
                if ( result.success ) {
                    jQuery('.contact-content').fadeOut(250, function () {
                        jQuery(this).after(jQuery('<h2><em>Thank you for contacting us. We will be in touch shortly.</em></h2>').hide().fadeIn(250));
                    });
                }
            });
        }
        e.preventDefault();
    })
});

var scale_image = {
	init: function() {
		jQuery('.scale-image').each(function() {
			scale_image.fix(jQuery(this));
		});	
	},
	
	fix: function(jq) {
		jq.removeAttr('width').removeAttr('height').css({
			width: '100%'
		});	
	}
};
jQuery(function() {
	scale_image.init();
});

var newsletter = {
	submit: function() {
		var lists = [];
		if (jQuery('#list-newsletters').is(':checked')) {
			lists.push(1);	
		}
		if (jQuery('#list-creative-matters').is(':checked')) {
			lists.push(2);	
		}
		if (jQuery('#list-blog-updates').is(':checked')) {
			lists.push(3);	
		}
		
		if (lists.length == 0) {
			lists = [1];
		}		
		
		var data = {
			first_name: jQuery('#input-newsletter-first-name').val(),
			last_name: jQuery('#input-newsletter-last-name').val(),
			email: jQuery('#input-newsletter-email').val(),
			lists: lists	
		}	
		data.action = 'fld_emma_submit';
		
		jQuery.post(ajaxurl, data, function(response) {
			jQuery('.section-newsletter').html('<div class="wrap"><div class="cf"><div class="col-1"><span style="font-size: 1.25em; line-height: 1.45em; color: #002E5D; padding: 0.5em 0; display: block; position: relative;">Thanks for signing up!</span></div></div></div>');
		});
	}
};

var mobile_menu = {
	nav: {},
	
	init: function() {
		this.nav = jQuery('.nav-primary');
		jQuery('.trigger-menu').click(function () {
			mobile_menu.toggle_menu();
		});	
	},
	
	open: function() {
		jQuery('body,html').animate({scrollTop: 0}, 250);
		
		jQuery('.header-primary').css({position: 'absolute'});
		
		//open menu here
		this.nav.show();
		
		//first slide over the icon
		jQuery('.trigger-menu').css({marginLeft: 0}).animate({marginLeft: 260,}, 500);
				
		//next slide over the menu
		this.nav.css({left: -260}).animate({left: 0, opacity: 1}, 500);
		
		//then slide over .sections, .section-newsletter and .footer-primary
		jQuery('.sections, .section-newsletter, .foot-primary').css({marginLeft: 0}).animate({marginLeft: 264}, 500);
	},
	
	close: function() {
		

		//Slide Icon back
		jQuery('.trigger-menu').css({marginLeft: 260}).animate({marginLeft: 0,}, 500);
		
		//Slide Menu back
		this.nav.css({left: 0, overflow: "hidden"}).animate({left: -260}, 500, function(){
			mobile_menu.nav.removeClass('nav-primary-active').attr("style", "");
			jQuery('.header-primary').css({position: 'fixed'});
		});
		
		//Slide Content back
		jQuery('.sections, .section-newsletter, .foot-primary').css({marginLeft: 264}).animate({marginLeft: 0}, 500);
	},
	
	toggle_menu: function() {
		if (this.nav.hasClass('nav-primary-active')) {
			mobile_menu.close();
		} else {
			this.nav.addClass('nav-primary-active');
			mobile_menu.open();
		}
	}
};
jQuery(function() {
	mobile_menu.init();
});