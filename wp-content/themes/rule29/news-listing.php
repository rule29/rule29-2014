<?php get_header(); ?>
<?php rule29_news_query(); ?>
<?php if ( have_posts() ) { ?>
<section class="section-gray section-content">
	<div class="wrap cf">
		<div class="cf">
			<div class="col-1" style="padding: 0 0 0 1%; width: 100%">
				<div class="selector selector-primary selector-news">
					<div class="selector-trigger"><?php rule29_current_news_category(); ?></div>
					<div class="selector-dropdown">
						<ul class="selector-options">
							<li class="selector-option">
				    			<a href="<?php echo get_post_type_archive_link( 'news' ); ?>" class="selector-link">View All</a>
    						</li>
							<?php rule29_news_categories_options(); ?>
						</ul>
					</div>
				</div>
				<div class="selector selector-primary selector-news">
					<div class="selector-trigger"><?php rule29_current_news_month(); ?></div>
					<div class="selector-dropdown">
						<ul class="selector-options">
							<li class="selector-option">
				    			<a href="<?php echo get_post_type_archive_link( 'news' ); ?>" class="selector-link">View All</a>
    						</li>
							<?php rule29_news_date_options(); ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<ul class="list-news cf">
			<?php while ( have_posts() ) {
				the_post();
				rule29_news_listing_post();
			} // endwhile posts ?>
		</ul>
	   <?php
	   special_nav();
       ?>
	</div>
</section>
<?php } // endif posts ?>
<?php get_footer(); ?>