<?php get_header(); ?>
<?php rule29_slider( get_the_ID(), 'full', false, rule29_get_capabilities() ); ?>
<?php if ( have_posts() ) { ?>
<?php while ( have_posts() ) { ?>
<?php the_post(); ?>
<section class="section-gray section-content">
	<div class="wrap">
		<div class="cf">
			<div class="col-1-6 col-tb-1 social-icons">
				<?php rule29_client_images(); ?>
				<?php rule29_social_buttons(); ?>
			</div>
			<div class="col-2-3 col-tb-1 description">
				<article class="main-content">
					<?php the_content(); ?>
					<?php rule29_project_link(); ?>
				</article>
			</div>
		</div>
		<div class="project-images">
			<?php rule29_project_images(); ?>
		</div>
		<?php rule29_related_projects( 'client' ); ?>
		<?php rule29_related_projects( 'capability' ); ?>
		<div class="cf project-meta">
			<div class="col-1-4">
				<a href="<?php echo get_post_type_archive_link( 'creative-matter' ); ?>" class="back-to">Back to Creative Matter</a>
			</div>
		</div>
	</div>
</section>
<?php } // endwhile posts ?>
<?php } // endif posts ?>
<?php get_footer(); ?>
