<?php

require 'core/index.php';

add_theme_support( 'post-thumbnails' );
add_image_size( 'project-grid', 272, 272, true );
add_image_size( 'project-grid-large', 460, 460, true );
add_image_size( 'project-detail', 9999, 1000, true );
add_image_size( 'blog-listing', 461, 221, true );
add_image_size( 'image-featured', 1200, 500, true );
add_image_size( 'image-blog', 1200, 575, true );
add_image_size( 'image-page', 1200, 740, true );
add_image_size( 'author-image', 85, 85, true );
add_image_size( 'client-image', 138, 138, true );
add_image_size( 'capability-large', 60, 60, true );
add_image_size( 'capability-small', 30, 30, true );
add_image_size( 'capability-tiny', 24, 24, true );
add_image_size( 'news-listing', 461, 9999 );
add_image_size( 'green-case-study', 195, 195, array( 'center', 'center' ) );

add_image_size('download-thumbnail', 140, 140, true);

add_action( 'wp_enqueue_scripts', 'rule29_enqueue' );

add_action('wp_ajax_creativematter', 'rule29_get_creative_matter');
add_action('wp_ajax_nopriv_creativematter', 'rule29_get_creative_matter');

// creative matter columns
add_filter( 'manage_creative-matter_posts_columns', 'rule29_creative_matter_add_table_columns' );
add_action( 'manage_creative-matter_posts_custom_column', 'rule29_creative_matter_columns_content', 10, 2 );

add_filter('excerpt_more', 'rule29_excerpt_more');

add_action( 'init', 'rule29_admin_init' );

register_nav_menus(
	array(
		'nav-primary' => 'Primary Nav',
		'social-footer' => 'Footer Social',
		'case-study' => 'Case Study Single',
	)
);

add_filter( 'show_admin_bar', '__return_false' );

rule29_meta();

rule29_tax_meta();

rule29_user_meta();

rule29_options();

/**
 * adds meta fields to user
 */
function rule29_user_meta() {
	new fld_custom_user_field(
		array(
			array(
				'type' => 'media',
				'name' => 'rule29_author_image',
				'label' => 'Image',
			),
		)
	);
}

/**
 * adds fields to taxonomies
 */
function rule29_tax_meta() {
	new fld_custom_taxonomy(
		array(
			array(
				'type' => 'media',
				'name' => 'rule29_client_image',
				'label' => 'Image',
			),
		),
		'client'
	);

	new fld_custom_taxonomy(
		array(
			array(
				'type' => 'media',
				'name' => 'rule29_capability_image',
				'label' => 'Image',
			),
		),
		'capability'
	);

	new fld_custom_taxonomy(
		array(
			array(
				'type' => 'media',
				'name' => 'rule29_category_image',
				'label' => 'Image',
			),
		),
		'news-category'
	);
}

/**
 * enqueues scripts and styles
 */
function rule29_enqueue() {
	wp_enqueue_style(
		'normalize',
		get_template_directory_uri() . '/css/normalize.css',
		array(),
		'1.1.1'
	);

	wp_enqueue_style(
		'rule29-fonts',
		get_template_directory_uri() . '/fonts.css',
		array(),
		'1.0'
	);


	wp_enqueue_style(
		'rule29-styles',
		get_template_directory_uri() . '/style.css',
		array(),
		''
	);

	wp_enqueue_script(
		'masonry-new',
		get_template_directory_uri() . '/js/jquery.masonry.js',
		array( 'jquery' ),
		'3.1.2'
	);

	wp_enqueue_script(
		'imagesloaded',
		get_template_directory_uri() . '/js/imagesloaded.js',
		array( 'jquery' ),
		'3.0.4'
	);

	wp_enqueue_script(
		'modernizr',
		get_template_directory_uri() . '/js/modernizr.js',
		array(),
		'2.6.2'
	);

	wp_enqueue_script(
		'jquery-validate',
		get_template_directory_uri() . '/js/validate.js',
		array( 'jquery' ),
		'1.11.1'
	);

	if (VERSION > VERSION_ONE) {
		wp_enqueue_script(
			'rule29',
			get_template_directory_uri() . '/scripts_v2.js',
			array( 'jquery', 'modernizr', 'masonry-new', 'jquery-validate', 'imagesloaded' ),
			filemtime(dirname(__FILE__) . '/scripts_v2.js')
		);
	} else {
		wp_enqueue_script(
			'rule29',
			get_template_directory_uri() . '/scripts.js',
			array( 'jquery', 'modernizr', 'masonry-new', 'jquery-validate', 'imagesloaded' ),
			'1.0'
		);

	}

	wp_localize_script(
		'rule29',
		'ajaxurl',
		admin_url( 'admin-ajax.php' )
	);
}

/**
 * adds pages, sections, and options
 */
function rule29_options() {
	new fld_site_page(
		array(
			'title'      => 'Site Pages',
			'menu_title' => 'Site Pages',
			'slug' 		 => 'site_pages',
		)
	);

	new fld_site_option_section(
		array(
			'name' 		  => 'rule29_pages',
			'label' 	  => 'Pages',
			'description' => 'Settings for important pages',
			'page' 		  => 'site_pages',
		)
	);

	new fld_site_option(
		array(
			'name'      => 'rule29_creative_matter_page',
	        'label'     => 'Creative Matter',
	        'type'      => 'dropdown_posts',
	        'page'      => 'site_pages',
	        'section'   => 'rule29_pages',
		)
	);

	new fld_site_option(
		array(
			'name'      => 'rule29_news_page',
	        'label'     => 'News',
	        'type'      => 'dropdown_posts',
	        'page'      => 'site_pages',
	        'section'   => 'rule29_pages',
		)
	);

	new fld_site_option(
		array(
			'name'      => 'rule29_contact_page',
	        'label'     => 'Contact',
	        'type'      => 'dropdown_posts',
	        'page'      => 'site_pages',
	        'section'   => 'rule29_pages',
		)
	);

	new fld_site_option(
		array(
			'name'      => 'rule29_blog_slider_bg',
	        'label'     => 'Blog Archive Image',
	        'type'      => 'media',
	        'page'      => 'site_pages',
	        'section'   => 'rule29_pages',
		)
	);

	new fld_site_option_section(
		array(
			'name' 		  => 'rule29_404',
			'label' 	  => '404',
			'description' => 'Settings for 404 page',
			'page' 		  => 'site_pages',
		)
	);

	new fld_site_option(
		array(
			'name'      => 'rule29_404_image',
	        'label'     => 'Image',
	        'type'      => 'media',
	        'page'      => 'site_pages',
	        'section'   => 'rule29_404',
		)
	);

	new fld_site_option_section(
		array(
			'name' 		  => 'rule29_grid',
			'label' 	  => 'Grid',
			'description' => 'Settings for Home Page Grid',
			'page' 		  => 'site_pages',
		)
	);

	new fld_site_option(
		array(
			'name'      => 'rule29_grid_news_category',
	        'label'     => 'News Category',
	        'type'      => 'dropdown_taxonomies',
	        'page'      => 'site_pages',
	        'section'   => 'rule29_grid',
	        'taxonomies' => array( 'news-category' ),
		)
	);
}

/**
 * adds meta to post types
 */
function rule29_meta() {
	new attachment_meta(
		array(
			'name' => 'rule29_link',
			'label' => 'URL',
			'type' => 'text',
		)
	);

    new fld_custom_post_meta(
        array(
            array(
                'type' => 'text',
                'name' => 'download_link',
                'label' => 'URL'
            ),
        ),
        array('downloads'),
        'Download Link'
    );


	new fld_custom_post_meta(
		array(
			array(
				'type' => 'media',
				'name' => 'rule29_slider',
				'label' => 'Slider'
			),
			array(
				'type' => 'text',
				'name' => 'rule29_subtitle',
				'label' => 'Subtitle'
			),
			array(
				'type' => 'textarea',
				'name' => 'rule29_text',
				'label' => 'Text'
			),
			array(
				'type' => 'dropdown',
				'name' => 'rule29_no_text',
				'label' => 'Hide Text Overlay',
				'options' => array(
					'No',
					'Yes',
				),
			),
		),
		array( 'page', 'creative-matter', 'collection' ),
		'Slider'
	);

	new fld_custom_post_meta(
		array(
			array(
				'type' => 'dropdown',
				'name' => 'rule29_no_text',
				'label' => 'Hide Text Overlay',
				'options' => array(
					'No',
					'Yes',
				),
			),
		),
		array( 'post' ),
		'Featured Image'
	);

	new fld_custom_post_meta(
		array(
			array(
				'name'  => 'rule29_project_images',
				'label' => 'Images',
				'type'  => 'media',
			),
			array(
				'name'  => 'rule29_project_url',
				'label' => 'Project URL',
				'type'  => 'text',
			),
		),
		array( 'creative-matter' ),
		'Project Details'
	);

	new fld_custom_post_meta(
		array(
			array(
				'name'  => 'rule29_author',
				'label' => 'Author',
				'type'  => 'text',
			),
			array(
				'name' => 'rule29_show_more',
				'label' => 'Show Read More',
				'type' => 'dropdown',
				'options' => array(
					'No',
					'Yes',
				),
			),
		),
		array( 'news' ),
		'News Info'
	);

	new fld_custom_post_meta(
		array(
			array(
				'name'  => 'rule29_social_facebook',
				'label' => 'Facebook',
				'type'  => 'text',
			),
			array(
				'name'  => 'rule29_social_twitter',
				'label' => 'Twitter',
				'type'  => 'text',
			),
			array(
				'name'  => 'rule29_social_instagram',
				'label' => 'Instagram',
				'type'  => 'text',
			),
			array(
				'name'  => 'rule29_social_linkedin',
				'label' => 'LinkedIn',
				'type'  => 'text',
			),
			array(
				'name'  => 'rule29_social_flickr',
				'label' => 'Flickr',
				'type'  => 'text',
			),
			array(
				'name'  => 'rule29_social_vimeo',
				'label' => 'Vimeo',
				'type'  => 'text',
			),
			array(
				'name'  => 'rule29_social_dribbble',
				'label' => 'Dribbble',
				'type'  => 'text',
			),
		),
		array( 'page' ),
		'Social'
	);

	new fld_custom_post_meta(
		array(
			array(
				'name'  => 'rule29_featured_project_1',
				'label' => 'Featured Project',
				'type'  => 'dropdown_posts',
				'include_empty' => true,
				'query' => array(
					'post_type' => 'creative-matter',
					'posts_per_page' => -1,
				),
			),
			array(
				'name'  => 'rule29_featured_project_2',
				'label' => 'Featured Project',
				'type'  => 'dropdown_posts',
				'include_empty' => true,
				'query' => array(
					'post_type' => 'creative-matter',
					'posts_per_page' => -1,
				),
			),
			array(
				'name'  => 'rule29_projects',
				'label' => 'Projects',
				'type'  => 'list_posts',
				'include_empty' => true,
				'query' => array(
					'post_type' => 'creative-matter',
					'posts_per_page' => -1,
				),
			),
			array(
				'name'  => 'rule29_news_updates',
				'label' => 'News &amp; Updates',
				'type'  => 'list_posts',
				'include_empty' => true,
				'query' => array(
					'post_type' => array( 'news', 'post' ),
					'posts_per_page' => -1,
				),
			),
		),
		array( 'collection' ),
		'Collection'
	);
}

/**
 * registers post types and taxonomies
 */
function rule29_admin_init() {
    register_post_type(
        'creative-matter',
        array(
            'label'       => 'Creative Matter',
            'public'      => true,
            'has_archive' => true,
            'supports' => array( 'title', 'editor', 'thumbnail' )
        )
    );

    register_post_type(
        'downloads',
        array(
            'label'       => 'Downloads',
            'public'      => false,
            'show_ui' => true,
            'has_archive' => true,
            'supports' => array( 'title', 'thumbnail' )
        )
    );

	register_taxonomy(
		'client',
		'creative-matter',
		array(
			'label' => 'Client',
		)
	);

	register_taxonomy(
		'capability',
		'creative-matter',
		array(
			'label' => 'Capability',
		)
	);

	register_post_type(
		'news',
		array(
			'label'       => 'News',
			'public'      => true,
			'has_archive' => true,
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' )
		)
	);

	register_taxonomy(
		'news-category',
		'news',
		array(
			'label' => 'Category',
		)
	);

	register_post_type(
		'collection',
		array(
			'label'       => 'Collection',
			'public'      => true,
			'supports' => array( 'title' )
		)
	);
}

/**
 * ajax function to get creative matter by client type and capability
 */
function rule29_get_creative_matter() {
	$client = ( ! empty( $_POST['client'] ) && $_POST['client'] !== 'all' )
		? $_POST['client']
		: false;
	$capability = ( ! empty( $_POST['capability'] ) && $_POST['capability'] !== 'all' )
		? $_POST['capability']
		: false;

	$project_query_args = array(
		'post_type' => 'creative-matter',
		'posts_per_page' => -1,
		'tax_query' => array(),
		'post_status' => 'publish'
	);

	if ( ! empty( $client ) ) {
		$project_query_args['tax_query'][] = array(
			'taxonomy' => 'client',
			'field'    => 'slug',
			'terms'    => $client,
		);
	}
	if ( ! empty( $capability ) ) {
		$project_query_args['tax_query'][] = array(
			'taxonomy' => 'capability',
			'field'    => 'slug',
			'terms'    => $capability,
		);
	}

	$project_query = new WP_Query( $project_query_args );

	rule29_project_list( $project_query );

	exit;
}

/**
 * change excerpt readmore
 * @return string ellipsis
 */
function rule29_excerpt_more() {
	return '&hellip;';
}

function rule29_news_categories_options() {
	$categories = get_terms( 'news-category' );
	if ( ! empty( $categories ) ) {
		foreach ( $categories as $category ) {
			$url = get_term_link( $category, 'news-category' );
			?>
			<li class="selector-option">
    			<a href="<?php echo $url; ?>" class="selector-link"><?php echo $category->name; ?></a>
    		</li>
			<?php
		}
	}
}

function rule29_news_date_options() {
	global $wpdb;

	$query = "
SELECT
    YEAR(`post_date`) as `year`,
    MONTH(`post_date`) as `month`,
    `post_date`
FROM
    {$wpdb->posts}
WHERE
    `post_type` = 'news' AND `post_status` = 'publish'
GROUP BY
    YEAR(`post_date`), MONTH(`post_date`)
ORDER BY
    `post_date` DESC";

    $dates = $wpdb->get_results( $query );

    if ( ! empty( $dates ) ) {
    	foreach( $dates as $date ) {
    		$url = get_permalink(get_option('rule29_news_page')) . '?y=' . $date->year . '&mon=' . $date->month;
    		$label = date('F Y', strtotime( $date->post_date ) ); ?>
    		<li class="selector-option">
    			<a href="<?php echo $url; ?>" class="selector-link"><?php echo $label; ?></a>
    		</li>
    	<?php }
    } // endif dates
}

/**
 * output links for authors
 */
function rule29_author_filters() {
	$users = get_users(
		array(
			'exclude' => array( 1 ),
			'orderby' => 'registered',
		)
	);
	if ( ! empty( $users ) ) { ?>
	<ul class="list-authors list-filters">
		<?php foreach( $users as $user ) {
		$url = get_author_posts_url( $user->ID );
		$nicename = get_the_author_meta( 'display_name', $user->ID ); ?>
		<li class="author filter">
			<a href="<?php echo $url; ?>" class="button button-secondary"><?php echo $nicename; ?></a>
		</li>
		<?php
		} // endforeach users ?>
	</ul>
	<?php } // endif users
}

/**
 * output links for categories
 */
function rule29_category_filters() {
	$categories = get_categories();
	if ( ! empty( $categories ) ) { ?>
	<ul class="list-categories list-filters">
		<?php foreach ( $categories as $category ) { ?>
		<li class="category filter">
			<a href="<?php echo get_term_link( $category ); ?>" class="button button-secondary"><?php echo $category->name; ?></a>
		</li>
		<?php } // endforeach category ?>
	</ul>
	<?php } // endif categories
}

/**
 * output dropdown options for date ranges
 */
function rule29_date_filters() {
	global $wpdb;

	$query = "
SELECT
    YEAR(`post_date`) as `year`,
    MONTH(`post_date`) as `month`,
    `post_date`
FROM
    {$wpdb->posts}
WHERE
    `post_type` = 'post' AND `post_status` = 'publish'
GROUP BY
    YEAR(`post_date`), MONTH(`post_date`)
ORDER BY
    `post_date` DESC";

    $dates = $wpdb->get_results( $query );

    if ( ! empty( $dates ) ) {
    	foreach( $dates as $date ) {
    		$url = get_site_url( null, '/' . $date->year . '/' . $date->month );
    		$label = date('F Y', strtotime( $date->post_date ) ); ?>
    		<li class="selector-option">
    			<a href="<?php echo $url; ?>" class="selector-link"><?php echo $label; ?></a>
    		</li>
    	<?php }
    } // endif dates
}

/**
 * output author image
 */
function rule29_author_image() {
	$author_image = get_the_author_meta( 'rule29_author_image', get_the_author_meta('ID') );
	$image_ids = explode( ',', $author_image );
	?>
	<a href="<?php echo get_author_posts_url( get_the_author_meta('ID'), get_the_author_meta( 'user_nicename' ) ); ?>" class="author-link">
	<?php
		if ( ! empty( $image_ids[0] ) ) {
			echo wp_get_attachment_image( $image_ids[0], 'author-image', false, array( 'class' => 'author-image' ) );
		}
	?>
	<span class="crafted-by">Crafted by</span>
	<span class="author-name"><?php the_author(); ?></span>
	</a>
	<?php
}

/**
 * output links in list of all categories
 */
function rule29_list_categories() {
	$categories = get_the_category();
	if ( ! empty( $categories ) ) { ?>
	<ul class="list-categories menu">
		<?php foreach ( $categories as $category ) { ?>
		<li class="category">
			<a href="<?php echo get_term_link( $category ); ?>" class="button button-primary button-category"><?php echo $category->name; ?></a>
		</li>
		<?php } // endforeach category ?>
	</ul>
	<?php } // endif categories
}

/**
 * output links in list of all tags
 */
function rule29_list_tags() {
	$tags = get_the_tags();
	if ( ! empty( $tags ) ) { ?>
		<ul class="list-tags menu cf">
		<?php foreach ( $tags as $tag ) { ?>
		<li class="tag-item">
			<a href="<?php echo get_term_link( $tag ); ?>" class="button button-tag">
				<span class="tag-inner"><?php echo $tag->name; ?></span>
			</a>
		</li>
		<?php } // endif ?>
		</ul>
	<?php } // endif tags
}

/**
 * output links to clients
 */
function rule29_client_selector() {
	$clients = get_terms( array( 'client' ) );
	if ( ! empty( $clients ) ) {
		$client_options = array(
			array(
				'url' => '',
				'label' => 'All',
				'data' => array(
					'client' => 'all',
				)
			)
		);
		foreach( $clients as $client ) {
			$client_options[] = array(
				'url' => '#', /*get_term_link( $client, 'client' ),*/
				'label' => $client->name,
				'data' => array(
					'client' => $client->slug
				),
			);
		}
		rule29_selector( $client_options, 'Select a Client' );
	}
}

/**
 * output links to capabilities
 */
function rule29_capability_selector() {
	$capabilities = get_terms( array( 'capability' ) );
	if ( ! empty( $capabilities ) ) {
		$capability_options = array(
			array(
				'url' => '',
				'label' => 'All',
				'data' => array(
					'capability' => 'all',
				)
			)
		);
		foreach( $capabilities as $capability ) {
			$capability_options[] = array(
				'url' => get_term_link( $capability, 'capability' ),
				'label' => $capability->name,
				'data' => array(
					'capability' => $capability->slug
				),
			);
		}
		rule29_selector( $capability_options, 'Select a Capability' );
	}
}

/**
 * output project list with thumbnails
 * @param  WP_Query $projects wordpress query of projects
 */
function rule29_project_list( $projects ) {
	if ( $projects->have_posts() ) { ?>
		<?php while ( $projects->have_posts() ) {
			$projects->the_post();
			if ( has_post_thumbnail() ) { ?>
			<li class="project-item">
				<div class="project-link">
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail( 'project-grid', array('class' => 'project-image' ) ); ?>
					</a>
					<span class="project-overlay">
						<table class="project-overlay-table">
							<tbody>
								<tr>
									<td class="project-overlay-cell">
										<a href="<?php the_permalink(); ?>">
											<?php
											echo '<span class="project-client">'
												. rule29_get_client_list()
												. '</span>'
												. '<span class="project-title">';
											the_title();
											echo '</span>'
												. '<span class="icons">';
											rule29_project_list_icons();
											echo '</span>';
											?>
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</span>
				</div>
			</li>
			<?php } // endif thumbnail ?>
		<?php } // endwhile posts ?>
	<?php } // endif posts
	wp_reset_postdata();
}

/**
 * output capability icons for listings
 * @param  boolean $echo whether to echo or return
 * @param  string  $size size of image
 * @return string        images as HTML
 */
function rule29_project_list_icons( $echo = true, $size = 'capability-small' ) {
	$capabilities = wp_get_post_terms(
		get_the_ID(),
		'capability',
		array(
			'orderby' => 'term_order',
		)
	);
	$icons = '';
	if ( ! empty( $capabilities ) ) {
		foreach ( $capabilities as $index => $capability ) {
			if ( $index < 3 ) {
				$images = get_metadata( 'term', $capability->term_id, 'rule29_capability_image', true );
				$image_ids = explode( ',', $images );
				if ( ! empty( $image_ids[0] ) ) {
					$icons .= wp_get_attachment_image( $image_ids[0], $size, false, array( 'class' => 'icon' ) );
				}
			}
		}
	}
	if ( $echo ) {
		echo $icons;
	}
	return $icons;
}


/**
 * output client images
 */
function rule29_client_images() {
	$clients = wp_get_post_terms( get_the_ID(), array( 'client' ), array() );
	if ( ! empty( $clients ) ) {
		foreach( $clients as $client ) {
			$image_data = get_metadata( 'term', $client->term_id, 'rule29_client_image', true );
			$image_ids = explode( ',', $image_data );
			if ( ! empty( $image_ids[0] ) ) {
				echo '<a href="' . str_replace('client', 'creative-matter/#', get_term_link( $client, 'client')) . 'all">';
				echo wp_get_attachment_image( $image_ids[0], 'client-image', false, array( 'class' => 'client-image' ) );
				echo '</a>';
			}
		}
	}
}

/**
 * get simple text list of clients names associated with post
 * @return string list of names
 */
function rule29_get_client_list() {
	$client_names = array();
	$clients = wp_get_post_terms( get_the_ID(), 'client' );
	if ( ! empty( $clients ) ) {
		foreach( $clients as $client ) {
			$client_names[] = $client->name;
		}
	} // endif clients
	return implode( ', ', $client_names );
}

/**
 * output selector
 * @param  options to output $options   options to output in selector
 * @param  string $select_text 			default text to display in trigger
 * @param  string $class       class to apply to selector
 */
function rule29_selector( $options, $select_text = 'Select an Option', $class = 'selector-primary' ) {
	?>
<div class="selector <?php echo $class; ?>">
	<div class="selector-trigger"><?php echo $select_text; ?></div>
	<div class="selector-dropdown">
		<ul class="selector-options">
		<?php foreach( $options as $option ) { ?>
		<li class="selector-option">
			<a href="<?php echo $option['url']; ?>" class="selector-link" <?php rule29_echo_data_attributes( $option['data'] ); ?>><?php echo $option['label']; ?></a>
		</li>
		<?php } ?>
		</ul>
	</div>
</div>
	<?php
}

/**
 * front page grid output
 */
function rule29_front_grid() {
	$projects = new WP_Query(
		array(
			'post_type'      => 'creative-matter',
			'posts_per_page' => 4,
		)
	);

	$news_query_args = array(
		'post_type'      => array( 'news' ),
		'posts_per_page' => 3,
	);

	$grid_term_id = get_option( 'rule29_grid_news_category' );

	if ( ! empty( $grid_term_id ) ) {
		$news_query_args['tax_query'] = array(
			array(
				'taxonomy' => 'news-category',
				'field' => 'id',
				'terms' => $grid_term_id,
			)
		);
	}

	$news_query = new WP_Query( $news_query_args );

	$news_cells = array();
	if ( $news_query->have_posts() ) {
		while ( $news_query->have_posts() ) {
			$news_query->the_post();
			$news_cells[] = rule29_news_cell();
		}
	}

	$cells = array();

	if ( $projects->have_posts() ) {
		$projects->the_post();
		$cells[] = rule29_project_cell( 2 );
	}

	$cells[] = array(
		'size' 	  => 1,
		'content' => '<div class="feed-cache" data-callback="render_instagram" data-source="instagram" data-user="rule29" data-index="0"></div>',
	);

	if ( $projects->have_posts() ) {
		$projects->the_post();
		$cells[] = rule29_project_cell( 1 );
	}

	if ( ! empty( $news_cells[0]) ) {
		$cells[] = $news_cells[0];
	}

	$cells[] = array(
		'size' 	  => 1,
		'content' => '<div class="feed-cache cell-twitter" data-callback="render_twitter" data-source="twitter" data-user="rule29"></div>',
		'type' => 'grid-open',
	);

	if ( ! empty( $news_cells[1]) ) {
		$cells[] = $news_cells[1];
	}

	if ( $projects->have_posts() ) {
		$projects->the_post();
		$cells[] = rule29_project_cell( 1 );
	}

	$cells[] = array(
		'size' 	  => 1,
		'content' => '<div class="feed-cache" data-callback="render_instagram" data-source="instagram" data-user="rule29" data-index="1"></div>',
	);

	if ( $projects->have_posts() ) {
		$projects->the_post();
		$cells[] = rule29_project_cell( 1 );
	}

	 $cells[] = array(
		 'size' 	  => 1,
		 'content' => '<h5 class="lastfm-heading">What We&rsquo;re Listening To:</h5><ul class="feed-cache list-lastfm" data-callback="render_lastfm" data-source="lastfm" data-user="rule29"></ul><a href="http://www.last.fm/user/rule29" target="_blank" class="grid-icon time-lastfm">Rule29 on Last.fm</a>',
		 'type' => 'grid-open',
	 );

	$cells[] = array(
		'size' 	  => 1,
		'content' => '<div class="feed-cache cell-twitter" data-callback="render_twitter" data-source="twitter" data-user="designsobriety"></div>',
		'type' => 'grid-open',
	);

	if ( ! empty( $news_cells[2]) ) {
		$cells[] = $news_cells[2];
	}

	$cells[] = array(
		'size' 	  => 1,
		'content' => '<div class="feed-cache" data-callback="render_instagram" data-source="instagram" data-user="rule29" data-index="2"></div>',
	);

	foreach( $cells as $cell ) {
		rule29_grid_cell( $cell );
	}
}

function rule29_grid_cell( $cell ) {
	echo '<div class="grid-cell grid-cell-' . $cell['size'] . '">';
	echo '<div class="grid-inner ';
	if ( ! empty( $cell['type'] ) ) {
		echo $cell['type'];
	}
	echo '">';
	echo $cell['content'];
	echo '</div>';
	echo '</div>';
}

/**
 * outputs cell relating to a blog post
 * @return text HTML of cell
 */
function rule29_post_cell() {
	$content = '<h2><em><a href="' . get_permalink() . '">' . get_the_title() . '</a></em></h2>';
	$content .= apply_filters( 'the_content', get_the_excerpt() );
	$content .= '<div class="cf">';
	$content .= '<span class="time-post pull-left grid-icon">Posted on ' . get_the_date('F j, Y') . '</span>';
	$content .= '<a href="' . get_permalink() . '" class="button button-primary pull-right">Read More</a>';
	$content .= '</div>';
	$cell = array(
		'type' => 'grid-text',
		'size' => 2,
		'content' => $content,
	);
	return $cell;
}

function rule29_news_cell() {
	$cell = array(
		'type' => 'no-padding cell-news',
		'size' => 2,
		'content' => rule29_news_item( false ),
	);
	return $cell;
}

/**
 * outputs cell relating to project
 * @param  integer $size grid cell width
 * @return string        HTML of cell
 */
function rule29_project_cell( $size = 1 ) {
	//open creative matter collection projects in a new window, but not the home page projects
	if (!is_front_page()) {$newwindow="target=\"_blank\"";}

	$content = '<a href="' . get_permalink() . '"'.$newwindow.'>';
	if ( has_post_thumbnail() ) {
		$content .= get_the_post_thumbnail( get_the_ID(), 'project-grid-large', array( 'class' => 'project-image' ) );
	}
	$content .= '</a>';
	$content .= '<div class="grid-overlay"><table class="grid-overlay-table"><tbody><tr><td>';
	$content .= '<a href="' . get_permalink() . '"'.$newwindow.'>';
	$clients = get_the_terms( get_the_ID(), 'client' );
	if ( ! empty( $clients ) ) {
		$client_names = array();
		foreach ( $clients as $client ) {
			$client_names[] = $client->name;
		}
		$content .= '<span class="project-client">' . implode( ', ', $client_names ) . '</span>';
	}
	$content .= '<span class="project-title">' . get_the_title() . '</span>';
	$capability_size = 'capability-small';
	if ( $size > 1 ) {
		$capability_size = 'capability-large';
	}
	$content .= '<span class="icons">' . rule29_project_list_icons( false, $capability_size ) . '</span>';
	$content .= '</a>';
	$content .= '</td></tr></tbody></table>';

	$content .= '<a href="' . get_permalink() . '" class="overlay-link" '.$newwindow.'></a>';
	$content .= '</div>'; // end overlay
	$cell = array(
		'size' => $size,
		'content' => $content,
	);
	return $cell;
}

/**
 * echoes array as data attributes
 * @param  array $data array of key value paris
 */
function rule29_echo_data_attributes( $data ) {
	if ( ! empty( $data ) ) {
		foreach( $data as $key => $value ) {
			echo ' data-' . $key . '="' . $value . '"';
		}
	}
}

/**
 * outputs link to project if external link exists
 */
function rule29_project_link() {
	$url = get_post_meta( get_the_ID(), 'rule29_project_url', true );
	if ( ! empty( $url ) ) { ?>
		<p>
			<a href="<?php echo $url; ?>" target="_blank" class="button button-primary">Visit the site</a>
		</p>
	<?php }
}

/**
 * output images for project
 */
function rule29_project_images() {
	$project_images = explode( ',', get_post_meta( get_the_ID(), 'rule29_project_images', true ) );
	if ( ! empty( $project_images[0] ) ) {
		foreach( $project_images as $project_image ) { ?>
<div class="cf project-image-row">
	<div class="col-1-6 col-tb-1">
		<?php $image = get_post( $project_image ); ?>
		<?php if ( ! empty( $image->post_content ) ) { ?>
		<div class="border-top">
		<?php echo apply_filters( 'the_content', $image->post_content ); ?>
		</div>
		<?php } // endif image description ?>
	</div>
	<div class="col-5-6 col-tb-1">
		<?php echo wp_get_attachment_image( $project_image, 'project-detail', false, array( 'class' => 'project-image' ) ); ?>
	</div>
</div>
		<?php } // endforeach images
	} // endif images
}

/**
 * output subnav based on primary menu
 */
function rule29_subnav() {
	?>
<nav class="nav-sub">
	<?php wp_nav_menu( array(
        'theme_location'  => 'nav-primary',
        'container'       => false,
        'menu_class'      => 'menu cf',
        'depth'           => 3,
        'walker'          => new Rule29_Walker_Nav_Menu() ) ); ?>
</nav>
	<?php
}

/**
 * output social links from page
 */
function rule29_social_nav() {
	$social_meta = array(
		'rule29_social_facebook' => 'Facebook',
		'rule29_social_twitter' => 'Twitter',
		'rule29_social_instagram' => 'Instagram',
		'rule29_social_linkedin' => 'LinkedIn',
		'rule29_social_flickr' => 'Flickr',
		'rule29_social_vimeo' => 'Vimeo',
		'rule29_social_dribbble' => 'Dribbble',
	);
	$social_links = array();
	foreach( $social_meta as $social_meta_key => $social_label ) {
		$social_url = get_post_meta( get_the_ID(), $social_meta_key, true );
		if ( ! empty( $social_url ) ) {
			$class = str_replace('rule29_social_', '', $social_meta_key);
			$social_links[] = '<li class="menu-item ' . $class . '"><a href="' . $social_url . '" target="_blank">' . $social_label . '</a></li>';
		} // endif social url
	}
	if ( ! empty( $social_links ) ) { ?>
<nav class="nav-social nav-related">
	<ul class="menu menu-social">
		<?php echo implode('', $social_links); ?>
	</ul>
</nav>
	<?php } // endif social links
}

/**
 * output related links based on page
 */
function rule29_related_nav() {
	$related_links = get_post_meta( get_the_ID(), 'rule29_related_links', true );
	if ( ! empty( $related_links ) ) { ?>
<nav class="nav-social nav-related">
	<ul class="menu menu-social">
		<?php foreach ( $related_links as $related_link ) { ?>
			<li class="menu-item">
				<a href="<?php echo $related_link['url']; ?>" target="_blank"><?php echo $related_link['label']; ?></a>
			</li>
		<?php } // endforeach related links ?>
	</ul>
</nav>
	<?php } // endif related links
}

/**
 * output related projects based on a taxonomy
 * @param  string $taxonomy taxonomy slug to output related projects from
 */
function rule29_related_projects( $taxonomy = 'client' ) {
	$terms = wp_get_post_terms( get_the_ID(), $taxonomy, array() );
	if ( ! empty( $terms ) ) { ?>
		<?php foreach( $terms as $term ) { ?>
		<?php $projects = new WP_Query( array(
			'post_type' => 'creative-matter',
			'posts_per_page' => 10,
			'post__not_in' => array( get_the_ID() ),
			'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field' => 'id',
					'terms' => $term->term_id
				)
			),
		) ); ?>
		<?php if ( $projects->have_posts() ) { ?>
<div class="cf">
	<div class="col-1-6 col-tb-1">
		<?php
			// this is to get the link to the listing for the term
			$client = 'all';
			$capability = 'all';
			if ( 'client' === $taxonomy ) {
				$client = $term->slug;
			} else {
				$capability = $term->slug;
			}
		?>
		<div class="border-top">
			<a class="more-similar" href="<?php echo get_post_type_archive_link( 'creative-matter' ) . '#/' . $client . '/' . $capability; ?>">More for <?php echo $term->name; ?></a>
		</div>
	</div>
	<div class="col-5-6 col-tb-1">
		<ul class="list-projects cf">
		<?php rule29_project_list( $projects ); ?>
		</ul>
	</div>
</div>
		<?php } // endif projects ?>
		<?php } // endforeach clients ?>
	<?php } // endif clients
}

function rule29_news_item( $thumb = false ) {
	$output = '';
	$show_more = get_post_meta( get_the_ID(), 'rule29_show_more', true );
	if ( has_post_thumbnail() && $thumb ) {
		if ( ! empty( $show_more ) ) {
			$output .= '<a href="' . get_permalink() . '">';
		}
		$output .= get_the_post_thumbnail( get_the_ID(), 'news-listing', array( 'class' => 'post-featured-image' ) );
		if ( ! empty( $show_more ) ) {
			$output .= '</a>';
		}
	} // endif has thumbnail
	$output .= '<div class="news-inner">'
		. '<h2 class="news-title"><em>';
	if ( ! empty( $show_more ) ) {
		$output .= '<a href="' . get_permalink() . '">';
	}
	$output .= get_the_title();
	if ( ! empty( $show_more ) ) {
		$output .= '</a>';
	}
	$output .= '</em></h2>';
	$author = get_post_meta( get_the_ID(), 'rule29_author', true );
	if ( ! empty( $author ) ) {
		$output .= '<p class="news-author">'
		. $author
		. '</p>';
	}


	global $post;
	$post_type = $post->post_type;


	if ( $show_more || $post_type == 'post' ) {
		$show_more = true; // for post_type == post
		$excerpt_words = explode(' ', get_the_excerpt() );
		$excerpt_slice = array_slice( $excerpt_words, 0, 22 );
		$excerpt = implode( ' ', $excerpt_slice ) . '&hellip;';
	} else {
		$excerpt = get_the_content();
	}

	$output .= '<div class="news-excerpt">'
		. apply_filters( 'the_content', $excerpt )
		. '</div>'
		. '<div class="cf">'
		. '<div class="pull-left cf">'
		. rule29_news_icons( 'capability-small', false )
		. '</div>'
		. '<div class="date-news pull-left">Posted '
		. get_the_date( 'F j, Y' )
		. '</div>';
	if ( ! empty( $show_more ) ) {
		$output .= '<div class="read-more pull-right">'
			. '<a href="'
			. get_permalink()
			. '" class="button button-primary">Read More</a>'
			. '</div>';
	}

	$output .= '</div>'
		. '</div>';
	return $output;
}

function rule29_news_listing_post() {
	$output = '<li class="col-1-2"><div class="news-item">'
		. rule29_news_item( true )
		. '</li>';
	echo $output;
}

/**
 * get array of icons
 * @param  string $size size of icon
 * @return array        icons
 */
function rule29_get_capabilities( $size = 'capability-small' ) {
	$icons = array();
	$capabilities = wp_get_post_terms( get_the_ID(), 'capability' );
	if ( ! empty( $capabilities ) ) {
		foreach ( $capabilities as $capability ) {
			$images = get_metadata( 'term', $capability->term_id, 'rule29_capability_image', true );
			$image_ids = explode( ',', $images );
			if ( ! empty( $image_ids[0] ) ) {
				$icons[] = '<a href="'
					. get_term_link( $capability, 'capability' )
					. '">'
					. wp_get_attachment_image( $image_ids[0], $size )
					. '</a>';
			}
		}
	}
	return $icons;
}

function rule29_news_icons( $size = 'capability-small', $echo = true ) {
	$icons = '';
	$categories = wp_get_post_terms( get_the_ID(), 'news-category' );
	if ( ! empty( $categories ) ) {
		foreach ( $categories as $category ) {
			$images = get_metadata( 'term', $category->term_id, 'rule29_category_image', true );
			$image_ids = explode( ',', $images );
			if ( ! empty( $image_ids[0] ) ) {
				$icons = '<a class="pull-left" href="'
					. get_term_link( $category, 'news-category' )
					. '">'
					. wp_get_attachment_image( $image_ids[0], $size )
					. '</a>';
			}
		}
	}
	if ( $echo ) {
		echo $icons;
	}
	return $icons;
}

/**
 * echo list of icons for slider
 * @param  array  $capabilities array of icons
 */
function rule29_capabilities_icons( $capabilities = array() ) {
	if ( ! empty( $capabilities ) ) {
		echo '<div class="capabilities"><ul class="list-capabilities cf">';
		foreach ( $capabilities as $capability ) {
			echo '<li class="capability">' . $capability . '</li>';
		}
		echo '</ul></div>';
	}
}

/**
 * output standalone image in slider
 */
function rule29_standalone_image() {
	$post_id = get_the_ID();
	if ( has_post_thumbnail( $post_id ) ) {
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'image-page' );

		$ratio = $image[2] / $image[1];

		$post = get_post( $post_id );

		$slide = array(
			'image' 	=> $image[0],
			'show_text' => true,
		);
		$hide_text = get_post_meta( get_the_ID(), 'rule29_no_text', true );
		if ( empty( $hide_text ) ) {
			$slide['title'] = get_the_title( $post_id );
			$slide['text']  = $post->post_content;
		}

		$slides = array( $slide );
		rule29_slider_output( $slides, array(), $ratio );
	}
}

/**
 * output  featured image as slider
 * @param  integer $post_id    post id
 * @param  string  $image_size image size
 * @param  array   $slide      slide overrides
 */
function rule29_featured_image( $post_id = 0, $image_size = 'full', $slide = array() ) {
	if ( empty( $post_id ) ) {
		$post_id = get_the_ID();
	}
	if ( has_post_thumbnail( $post_id ) ) {
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $image_size );

		$ratio = $image[2] / $image[1];

		$hide_text = get_post_meta( get_the_ID(), 'rule29_no_text', true );
		if ( empty( $hide_text ) ) {
			$slide['title'] = get_the_title( $post_id );
			$slide['subtitle'] = get_post_meta( $post_id, 'rule29_subtitle', true );
			$slide['text'] = get_post_meta( $post_id, 'rule29_text', true );
			if ( is_single() ) {
				$slide['subtitle'] = ''; //get_the_date( 'F j, Y' );
			}
		}
		$slide['image'] = $image[0];

		$slides = array( $slide );
		rule29_slider_output( $slides, array(), $ratio );
	}
}

/**
 * create generic set of slides
 * @param  integer  $post_id         post id
 * @param  string  $image_size      image size
 * @param  boolean $subtitle_before whether to place subtitle before or after title
 * @param  array   $capabilities    array of capabilities icons
 */
function rule29_slider( $post_id, $image_size = 'full', $subtitle_before = false, $capabilities = array(), $link = false, $auto = false ) {
	$link = true; //why would this ever be false? if they type in a link, link it!
	$slides = array();

	$slide_ids = get_post_meta( $post_id, 'rule29_slider', true );
	$slide_ids = explode( ',' , $slide_ids );

	$ratio = 999;

	if ( ! empty( $slide_ids[0] ) ) {
		foreach ( $slide_ids as $key => $slide_id ) {
			$image = get_post( $slide_id );
			$image_src = wp_get_attachment_image_src( $slide_id, $image_size );

			$this_ratio = $image_src[2] / $image_src[1];

			if ( $this_ratio < $ratio ) {
				$ratio = $this_ratio;
			}

			$slide = array(
				'image' 	=> $image_src[0],
			);

			$hide_text = get_post_meta( get_the_ID(), 'rule29_no_text', true );
			if ( empty( $hide_text ) ) {
				$slide['title'] 	= $image->post_title;
				$slide['text'] 		= $image->post_content;

				if ( $subtitle_before ) {
					$slide['before-subtitle'] = $image->post_excerpt;
				} else {
					$slide['subtitle'] = $image->post_excerpt;
				}
			}

			if ( $link ) {
				$url = get_post_meta( $slide_id, 'rule29_link', true );
				if ( ! empty( $url ) ) {
					$slide['link'] = $url;
				}
			}

			$slides[] = $slide;
		}
	}
	rule29_slider_output( $slides, $capabilities, $ratio, $auto );
}

/**
 * creates a blog slider
 */
function rule29_blog_slider() {
	if ( is_home() && empty( $_GET['search'] ) ) {
		rule29_blog_home_slider();
	} else {
		rule29_blog_archive_slider();
	}
}

function rule29_blog_archive_slider() {
	$image = wp_get_attachment_image_src( get_option( 'rule29_blog_slider_bg' ), 'full' );
	$slides = array(
		array(
			'title' => rule29_blog_label( false ),
			'image' => $image[0],
		)
	);
	$ratio = $image[2] / $image[1];
	rule29_slider_output( $slides, array(), $ratio );
}

function rule29_blog_home_slider() {
	global $wp_query;
	$slider_query_args = wp_parse_args(
		array(
			'posts_per_page' => 10,
			'post__in' => get_option( 'sticky_posts' ),
		),
		$wp_query->query_vars
	);

	$slides = array();

	$ratio = 999;

	$slider_query = new WP_Query( $slider_query_args );
	if ( $slider_query->have_posts() ) {
		while ( $slider_query->have_posts() ) {
			$slider_query->the_post();
			if ( has_post_thumbnail() ) {
				$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'image-blog' );
				$this_ratio = $image[2] / $image[1];
				if ( $this_ratio < $ratio ) {
					$ratio = $this_ratio;
				}
				$slide = array(
					'image' => $image[0],
					'link' => get_permalink(),
				);
				$hide_text = get_post_meta( get_the_ID(), 'rule29_no_text', true );
				if ( empty( $hide_text ) ) {
					$slide['title'] = get_the_title();
					$slide['subtitle'] = ''; //get_the_date('F j, Y');
				}

				$slides[] = $slide;
			}
		}
	}
	wp_reset_postdata();

	if ( ! empty( $slides ) ) {
		rule29_slider_output( $slides, array(), $ratio );
	}
}

/**
 * outputs slider
 * @param  array  $slides       slides to output
 * @param  array  $capabilities icons to output
 * @param  float  $ratio        ratio of slider
 */
function rule29_slider_output( $slides, $capabilities = array(), $ratio = 0.46666666, $auto = false ) {
	if ( ! empty( $slides ) ) { ?>
<section class="section-black slider" data-ratio="<?php echo $ratio; ?>" data-auto-movement="<?php echo $auto ? 1 : 0; ?>">
	<div class="wrap">
		<a href="<?php echo get_site_url(); ?>" class="logo">Rule29</a>
	</div>
	<div class="images slides">
		<?php
			foreach( $slides as $slide ) {
			if ( ! empty( $slide['link'] ) ) {
				echo '<a href="' . $slide['link'] . '">';
			}
		?>
		<img class="image slide" src="<?php echo $slide['image']; ?>">
		<?php
			if ( ! empty( $slide['link'] ) ) {
				echo '</a>';
			}
		?>
		<?php } // endforeach slide images ?>
	</div>
	<ul class="texts slides">
		<?php foreach( $slides as $slide ) { ?>
		<?php echo '<li class="text slide';
		if ( empty( $slide['show_text'] ) ) {
			echo ' hide-text';
		}
		echo '">'; ?>
			<div class="wrap">
				<?php
					if ( ! empty( $slide['link'] ) ) {
						echo '<a href="' . $slide['link'] . '">';
					}
				?>
				<?php if ( ! empty( $slide['before-subtitle'] ) ) { ?>
				<h3 class="slider-subtitle"><?php echo $slide['before-subtitle']; ?></h3>
				<?php } // endif before subtitle ?>
				<?php if ( ! empty( $slide['title'] ) ) { ?>
				<h1 class="slider-title"><?php echo $slide['title']; ?></h1>
				<?php } ?>
				<?php if ( ! empty( $slide['subtitle'] ) ) { ?>
				<h3 class="slider-subtitle"><?php echo $slide['subtitle']; ?></h3>
				<?php
					} // endif subtitle
					if ( ! empty( $slide['text'] ) ) {
						echo apply_filters( 'the_content', $slide['text'] );
					} // endif text
					if ( ! empty( $slide['link'] ) ) {
						echo '</a>';
					}
				?>
			</div>
		</li>
		<?php } // endforeach slide text ?>
	</ul>
	<?php rule29_capabilities_icons( $capabilities ); ?>
	<ul class="indicators"></ul>
</section>
	<?php } // endif slides
}

/**
 * set up query for blog pages
 */
function rule29_blog_query() {
	global $wp_query;

	$additional_query_vars = array();
	if ( is_home() ) {
		$additional_query_vars['post__not_in'] = get_option( 'sticky_posts' );
	}
	if ( ! empty( $_GET['search'] ) ) {
		$additional_query_vars['s'] = $_GET['search'];
	}
	query_posts(
		array_merge(
			$wp_query->query_vars,
			$additional_query_vars
		)
	);
}

/**
 * set up query for news pages
 */
function rule29_news_query() {
	global $wp_query;

	$additional_query_vars = array();
	if ( ! empty( $_GET['y'] ) ) {
		$additional_query_vars['year'] = $_GET['y'];
	}
	if ( ! empty( $_GET['mon'] ) ) {
		$additional_query_vars['monthnum'] = $_GET['mon'];
	}
	query_posts(
		array_merge(
			$wp_query->query_vars,
			$additional_query_vars
		)
	);
}

/**
 * walker to add classes to menu items
 */
class Rule29_Walker_Nav_Menu extends Walker_Nav_Menu {
	/**
	 * @see Walker_Nav_Menu::start_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of page. Used for padding.
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		//echo "This is a test of getting to custom walker level";
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"sub-menu sub-menu-" . ( $depth + 1 ) . "\">\n";
	}

	/**
	 * @see Walker_Nav_Menu::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param int $current_page Menu item ID.
	 * @param object $args
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		//echo "This is a test of getting to custom walker EL";
		$item->classes[] = 'depth-' . $depth;
		parent::start_el( $output, $item, $depth, $args, $id );
	}
}

/**
 * output captcha for forms
 */
function rule29_captcha_front() {
	require_once('recaptchalib.php');
    //$publickey = "6Ldcq-cSAAAAAPzGG25iB9rhZed-v8g9LHc0R5G9";
    $publickey = "6LemreoSAAAAAD54WPZD3cJ3VNoDoyRsE47wFtZ8";
    echo recaptcha_get_html( $publickey, null, true );
}

/**
 * output social buttons
 */
function rule29_social_buttons() {
	?>
<div class="addthis_toolbox addthis_default_style ">
	<a class="addthis_button_facebook_like addthis-button" fb:like:layout="button_count"></a>
	<a class="addthis_button_tweet addthis-button"></a>
	<a class="addthis_button_google_plusone addthis-button" g:plusone:size="medium"></a>
	<a class="addthis_button_pinterest_pinit addthis-button"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-52338be91643695c"></script>
	<?php
}

/**
 * output capabilities checkboxes for contact form
 */
function rule29_capabilities_checkboxes() {
	$capabilities = array(
		'branding' 			=> 'Branding',
		'print' 			=> 'Print',
		'books' 			=> 'Books',
		'social-media' 		=> 'Social Media',
		'logo-identity' 	=> 'Logo &amp; Identity',
		'advertising' 		=> 'Advertising',
		'premiums' 			=> 'Premiums',
		'environmental' 	=> 'Environmental',
		'packaging' 		=> 'Packaging',
		'annual-reports' 	=> 'Annual Reports',
		'web-development' 	=> 'Web Development',
		'video-motion' 		=> 'Video/Motion',
		'graphics' 			=> 'Graphics',
		'strategy'          => 'Strategy'
	);

	foreach( $capabilities as $slug => $capability ) {
	?>
	<li class="col-1-4">
		<label class="label-check"><input type="checkbox" name="<?php echo $slug; ?>" id="<?php echo $slug; ?>" class="input-check" value="1"><?php echo $capability; ?></label>
	</li>
	<?php
	} // endforeach capabilities
}

function rule29_current_news_category() {
	$label = 'Select a Category';
	if ( is_tax( 'news-category' ) ) {
		$queried_object = get_queried_object();
		$label = $queried_object->name;
	}
	echo $label;
}

function rule29_current_news_month() {
	$label = 'Select a Month';
	if ( ! empty( $_GET['y'] ) && ! empty( $_GET['mon'] ) ) {
		$label =  date('F Y', strtotime( '1-' . $_GET['mon'] . '-' . $_GET['y'] ) );
	}
	echo $label;
}

function rule29_collection_project_posts() {
	$project_ids = get_post_meta( get_the_ID(), 'rule29_projects', true );
	$rule29_featured_project_1 = get_post_meta( get_the_ID(), 'rule29_featured_project_1', true );
	$rule29_featured_project_2 = get_post_meta( get_the_ID(), 'rule29_featured_project_2', true );

	if ( ! empty( $rule29_featured_project_2 ) ) {
		array_unshift( $project_ids, $rule29_featured_project_2 );
	}
	if ( ! empty( $rule29_featured_project_1 ) ) {
		array_unshift( $project_ids, $rule29_featured_project_1 );
	}
	if ( empty( $project_ids ) ) {
		return false;
	}

	$project_query_args = array(
		'post__in' => $project_ids,
		'post_type' => 'creative-matter',
		'posts_per_page' => -1,
		'orderby' => 'post__in',
	);
	$project_query = new WP_Query( $project_query_args );
	if ( $project_query->have_posts() ) { ?>
	<div class="border-top">
		<h2><em>Our Latest Work</em></h2>
		<div class="grid cf">
			<div class="grid-cell grid-cell-1"></div>
		<?php while ( $project_query->have_posts() ) {
			$project_query->the_post();
			$size = ( get_the_ID() == $rule29_featured_project_2 || get_the_ID() == $rule29_featured_project_1 ) ? 2: 1;
			echo rule29_grid_cell( rule29_project_cell( $size ) );
		} ?>
		</div>
	</div>
	<?php }
	wp_reset_postdata();
}

function rule29_collection_news_posts() {
	$news_ids = get_post_meta( get_the_ID(), 'rule29_news_updates', true );
	if ( empty( $news_ids ) ) {
		return false;
	}
	$news_query_args = array(
		'post__in' => $news_ids,
		'post_type' => array( 'news', 'post' ),
		'posts_per_page' => -1,
		'ignore_sticky_posts' => true
	);
	$news_query = new WP_Query( $news_query_args );

	if ( $news_query->have_posts() ) {
	?>
<div class="border-top">
	<h2><em>News &amp; Updates</em></h2>
	<ul class="list-news cf">
	<?php
		while ( $news_query->have_posts() ) {
			$news_query->the_post();
			rule29_news_listing_post();
		}
		?>
	</ul>
</div>
		<?php
	}
	wp_reset_postdata();
}

function rule29_blog_label( $echo = true ) {
	ob_start();
	the_post();
	if ( is_author() ) {
		echo 'Blog Posts by ' . get_the_author();
	} elseif ( is_date() ) {
		echo 'Blog posts from ' . get_the_date('F Y');
	} elseif ( is_category() ) {
		echo 'Blog posts tagged ';
		single_cat_title();
	} elseif ( is_tag() ) {
		echo 'Blog posts tagged ';
		single_tag_title();
	} elseif ( ! empty( $_GET['search'] ) ) {
		echo 'Blog Posts containing &ldquo;' . $_GET['search'] . '&rdquo;';
	}
	$label = ob_get_clean();
	rewind_posts();
	if ( $echo ) {
		echo $label;
	}
	return $label;
}

function rule29_list_client_names( $post_id, $echo = true ) {
	$client_name_string = '';
	$clients = wp_get_post_terms( $post_id, 'client' );
	if ( ! empty( $clients ) ) {
		$client_names = array();
		foreach ( $clients as $client ) {
			$client_names[] = $client->name;
		}
		$client_name_string = implode( ', ', $client_names );
	}
	if ( $echo ) {
		echo $client_name_string;
	}
	return $client_name_string;
}

function rule29_title() {
	if ( is_singular( 'creative-matter' ) ) {
		$client_names = rule29_list_client_names( get_the_ID(), false );
		if ( ! empty( $client_names ) ) {
			echo $client_names . ' - ';
		}
	}
	wp_title( '|', true, 'right' );
}

function rule29_creative_matter_add_table_columns( $defaults = array() ) {
    $defaults['client'] = 'Client';
    return $defaults;
}

function rule29_creative_matter_columns_content( $column_name, $post_id ) {
	$clients = wp_get_post_terms( $post_id, 'client' );

	rule29_list_client_names( $post_id );
}


function special_nav() {
    global $wp_query;
    $max_page = $wp_query->max_num_pages;

    $previous_link = get_previous_posts_page_link();
    $next_link =  get_next_posts_page_link($max_page);

    $str = "";
    if ($wp_query->query_vars['paged'] > 0 && $previous_link != "") {
        $str .= '<a href="' . $previous_link . '" class="previous">Previous</a>';
    }
    if ($next_link != "") {
        $str .= '<a href="' . $next_link . '" class = "next">Next</a>';
    }
?>
    <div class="navigation-post col-1">
        <?php
            echo $str;
        ?>
    </div>
<?php
}
