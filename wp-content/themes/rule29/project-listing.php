<section class="section-gray section-content">
	<div class="wrap cf">
		<div class="col-1 cf">
			<?php rule29_client_selector(); ?>
			<?php rule29_capability_selector(); ?>
		</div>
		<div class="col-1 cf">
			<?php $query_args = array(
				'post_type' => 'creative-matter',
				'posts_per_page' => -1
			); ?>
			<ul class="list-projects cf">
				<?php rule29_project_list( new WP_Query( $query_args ) ); ?>
			</ul>
		</div>
	</div>
</section>
