<?php
/* Template Name: Going Green */
get_header();

if (rule29_slider( get_the_ID(), 'full', false)){
	rule29_slider( get_the_ID(), 'full', false);
}else{
	rule29_featured_image();
}
?>

<?php if ( have_posts() ): ?>
	<?php while ( have_posts() ) : ?>
		<?php the_post(); ?>
		<section class="section-gray section-content">
			<div class="wrap cf">
				<div class="col-1-6 col-tb-1-4">
					<nav class="nav-sub">
						<?php wp_nav_menu(array(
							'theme_location' => 'case-study',
							'menu_class' => 'menu cf',
							'container'       => false,
							'depth'           => 3,
						));?>
					</nav>
				</div>
				<div class="col-2-3 col-tb-3-4">
					<div class="col-inner">
						<article class="main-content">
							<?php the_content(); ?>
						<?php endwhile; endif; rewind_posts();?>

							<?php // green case study query
							// WP_Query arguments
							$args = array (
							'post_type'              => array( 'green_case_study' ),
							'posts_per_page'         => '-1',
							);

							// The Query
							$query = new WP_Query( $args );

							if( $query->have_posts() ):
							?>
								<div class="case-study--container">
									<?php while( $query->have_posts() ): $query->the_post();?>
										<div class="col-1-3">
											<a href="<?php the_permalink();?>">
												<div class="overlay">
													<h3><?php the_title();?></h3>
													<p>Green Case Study</p>
												</div>
												<?php
												// first, get the image ID returned by ACF
												$image_id = get_field('case_study_thumbnail');
												// and the image size you want to return
												$image_size = 'green-case-study';
												// use wp_get_attachment_image_src to return an array containing the image
												// we'll pass in the $image_id in the first parameter
												// and the image size registered using add_image_size() in the second
												$image_array = wp_get_attachment_image_src($image_id, $image_size);
												// finally, extract and store the URL from $image_array
												$image_url = $image_array[0];
												?>

												<img src="<?php echo $image_url; ?>" alt="">
											</a>
										</div>
									<?php endwhile;?>
								</div>
							<?php endif; wp_reset_postdata();?>

						<?php if( have_posts() ): while( have_posts() ): the_post();?>
						</article>
					</div>
				</div>
				<div class="col-1-6 col-tb-1">
					<?php rule29_social_nav(); ?>
					<?php rule29_related_nav(); ?>
				</div>
			</div>
		</section>
	<?php endwhile; // endwhile posts ?>
<?php endif; // endif posts ?>
<?php get_footer(); ?>
