<?php
/*
Plugin Name: Fld Emma
Description: Email Signup for MyEmma
Author: Floodlight Design
Author URI: http://floodlightdesign.com
*/

class fld_emma {
	const EMMA_ACCOUNT_ID = 4642;
	const EMMA_PUBLIC_API_KEY = 'b0e1fb40c415ba7217d8';
	const EMMA_PRIVATE_API_KEY = 'd6572e2e647537f1dfa2';
	const LIST_NEWSLETTER = 1084962;
	const LIST_CREATIVE_MATTERS = 308770;
	const LIST_BLOG_UPDATES = 1085986;
	
	function __construct() {
		add_action('wp_ajax_fld_emma_submit', array('fld_emma', 'submit'));
        add_action('wp_ajax_nopriv_fld_emma_submit', array('fld_emma', 'submit'));
	}
	
	public static function submit() {
		$first_name = isset($_POST['first_name']) ? $_POST['first_name'] : '';
		$last_name = isset($_POST['last_name']) ? $_POST['last_name'] : '';
		$email = isset($_POST['email']) ? $_POST['email'] : '';
		$lists = isset($_POST['lists']) ? $_POST['lists'] : array();
		
		$valid = true;
		
		if (empty($email) || empty($lists)) {
			$valid = false;
		}
		
		$response = new stdClass();
		$response->valid = false;
		$response->valid_lists = false;	
		$response->success = false;
		$response->result = false;
		
		if ($valid) {
			$response->valid = true;			
			$list_ids = array();
			foreach($lists as $list) {
				switch($list) {
					case 1:
						$list_ids[] = self::LIST_NEWSLETTER;
						break;
					case 2:
						$list_ids[] = self::LIST_CREATIVE_MATTERS;
						break;
					case 3:
						$list_ids[] = self::LIST_BLOG_UPDATES;
						break;	
				}
			}
			
			if (!empty($list_ids)) {
				$response->valid_lists = true;
				
				require 'includes/Emma.php';
				$emma = new Emma(self::EMMA_ACCOUNT_ID, self::EMMA_PUBLIC_API_KEY, self::EMMA_PRIVATE_API_KEY);
				
				$member_data = array(
					"email" => $email,
					"fields" => array(
						"first_name" => $first_name,
						"last_name" => $last_name
					),
					"group_ids" => $list_ids
				);
				$result = $emma->membersAddSingle($member_data);
				
				$response->result = $result;
				$response->success = true;
			}		
		}
		
		echo json_encode($response);	
		
		die();
	}
}

new fld_emma();
