<?php
/*
Plugin Name: FMap
Description: Custom map
Author: Floodlight Design
Author URI: http://floodlightdesign.com
*/

class fmap {
	const CLASS_NAME = 'fmap';
	const POST_TYPE = 'fmap';
	const META_DATA = '_fmap_data';
			
	function __construct() {
		add_theme_support( 'post-thumbnails' ); 
		add_image_size( 'fmap-image', 298, 139);
		
		register_post_type(self::POST_TYPE, array(
			'labels' => array(
				'name' => 'Map',
				'singular_name' => 'Marker',
				'add_new' => 'Create Marker',
				'add_new_item' => 'Create Marker',
				'edit_item' => 'Edit Marker'
			),
			'rewrite' => false,
			'show_ui' => true,
			'supports' => array('title', 'editor', 'thumbnail')
		));
		add_action( 'admin_init', array(self::CLASS_NAME, 'admin_init'));
		add_action( 'add_meta_boxes', array(self::CLASS_NAME, 'add_meta_box'));
		add_action( 'save_post', array(self::CLASS_NAME, 'save_meta'));
	}
	
	public static function add_meta_box() {
		add_meta_box(self::CLASS_NAME, 'Information', array(self::CLASS_NAME, 'create_meta_box'), self::POST_TYPE);		
	}
	
	public static function admin_init() {
		wp_enqueue_script('google_map', '//maps.google.com/maps/api/js?sensor=false');
		wp_enqueue_script('fmap_script', plugins_url('fmap/js/fmap.js'), array('jquery', 'google_map'));
		wp_enqueue_style('fmap_style', plugins_url('fmap/css/fmap.css'));
	}
	
	private static function validate_save($post_id) {
		// First we need to check if the current user is authorised to do this action. 
		if ( isset($_REQUEST['post-type']) && 'page' == $_REQUEST['post_type'] ) {
			if ( ! current_user_can( 'edit_page', $post_id ) )
				return false;
		} else {
			if ( ! current_user_can( 'edit_post', $post_id ) )
				return false;
		 }

		// Secondly we need to check if the user intended to change this value.
		if ( ! isset( $_POST[self::CLASS_NAME . '_nonce'] ) || ! wp_verify_nonce( $_POST[self::CLASS_NAME . '_nonce'], plugin_basename( __FILE__ ) ) )
			return false;
		
		return true;		
	}

	public static function get_meta_data($post_id) {
		return get_post_meta($post_id, self::META_DATA, true);
	}
	
	private static function set_meta_data($post_id, $meta_data) {
		update_post_meta($post_id, self::META_DATA, $meta_data);
	}
	
	public static function save_meta($post_id) {
		if (self::validate_save($post_id)) {
			$meta_data = self::get_meta_data($post_id);
			
			$subtitle = isset($_POST['fmap_subtitle']) ? $_POST['fmap_subtitle'] : '';
			$meta_data->subtitle = $subtitle;			
			
			$phone = isset($_POST['fmap_phone']) ? $_POST['fmap_phone'] : '';
			$meta_data->phone = $phone;			
			
			$fax = isset($_POST['fmap_fax']) ? $_POST['fmap_fax'] : '';
			$meta_data->fax = $fax;			
			
			$address = isset($_POST['fmap_address']) ? $_POST['fmap_address'] : '';
			$meta_data->address = $address;				
			
			$latitude = isset($_POST['fmap_latitude']) ? $_POST['fmap_latitude'] : '';
			$meta_data->latitude = $latitude;
			
			$longitude  = isset($_POST['fmap_longitude']) ? $_POST['fmap_longitude'] : '';
			$meta_data->longitude = $longitude;
			
			$url  = isset($_POST['fmap_url']) ? $_POST['fmap_url'] : '';
			$meta_data->url = $url;
			
			self::set_meta_data($post_id, $meta_data);
		}
	}
	
	public static function create_meta_box($post) {
		// Use nonce for verification
		wp_nonce_field( plugin_basename( __FILE__ ), self::CLASS_NAME . '_nonce' );

		$meta_data = self::get_meta_data($post->ID);
		
		$subtitle = isset($meta_data->subtitle) ? htmlspecialchars($meta_data->subtitle) : '';
		$phone = isset($meta_data->phone) ? htmlspecialchars($meta_data->phone) : '';
		$fax = isset($meta_data->fax) ? htmlspecialchars($meta_data->fax) : '';
		
		$address = isset($meta_data->address) ? htmlspecialchars($meta_data->address) : '';
		$latitude = isset($meta_data->latitude) ? htmlspecialchars($meta_data->latitude) : '';
		$longitude = isset($meta_data->longitude) ? htmlspecialchars($meta_data->longitude) : '';
		
		$url = isset($meta_data->url) ? htmlspecialchars($meta_data->url) : '';
		echo <<<EOS
		<p>
			<label>
				<strong>Subtitle:</strong><br />
				<input name="fmap_subtitle" type="text" class="text" value="$subtitle" />
			</label>
		</p>
		<p>
			<label>
				<strong>URL:</strong><br />
				<input name="fmap_url" type="text" class="text" value="$url" />
			</label>
		</p>
		<p>
			<label>
				<strong>Telephone:</strong><br />
				<input name="fmap_phone" type="text" class="text" value="$phone" />
			</label>
		</p>
		<p>
			<label>
				<strong>Fax:</strong><br />
				<input name="fmap_fax" type="text" class="text" value="$fax" />
			</label>
		</p>				
		<p>
			<label>
				<strong>Address:</strong><br />
				<textarea name="fmap_address" style="width:230px;height:100px">$address</textarea>
			</label>
		</p>
		<p>
			<label><strong>Location</strong></label><br />
			<input type="hidden" id="fmap_latitude" name="fmap_latitude" value="$latitude" />
			<input type="hidden" id="fmap_longitude" name="fmap_longitude" value="$longitude" />
			<div id="fmap_container">
				<div id="fmap_map"></div>
			</div>
			<div class="clr"></div>		
		</p>
EOS;
	}
}


class fmap_front {
	const CLASS_NAME = 'fmap_front';
	
	function __construct() {
		add_shortcode( 'fmap', array(self::CLASS_NAME, 'shortcode'));
		add_action( 'wp_enqueue_scripts', array(self::CLASS_NAME, 'scripts') );	
	}
	
	public static function scripts() {
		wp_enqueue_style('fmap_style_front', plugins_url('fmap/css/fmap_front.css'));
		wp_enqueue_script('google_map', '//maps.google.com/maps/api/js?sensor=false');
		if (VERSION > VERSION_TWO) {
			wp_enqueue_script('fmap_script_front', plugins_url('fmap/js/fmap_front_v3.js'), array('jquery', 'google_map'));
		} else {
			wp_enqueue_script('fmap_script_front', plugins_url('fmap/js/fmap_front.js'), array('jquery', 'google_map'));		
		}
	}
	
	public static function shortcode($attrs) {
		ob_start();
?>
<div id="map"></div>
	<script type="text/javascript">
		<!--
		jQuery(function() {
			<?php
				$posts = get_posts(array(
					'post_type' => fmap::POST_TYPE,
					'posts_per_page' => -1
				));
				
				$data = array();
				foreach($posts as $post) {
					$post_id = $post->ID;
					$meta = fmap::get_meta_data($post_id);
					
					$item = new stdClass();
					$item->id = $post_id;
					$item->title = $post->post_title;
					$item->description = apply_filters('the_content', $post->post_content);
					$item->address = nl2br($meta->address);
					$item->map_link = 'http://maps.google.com/?saddr=current+location&daddr=' . urlencode(trim(str_replace(array("\n","\r"), " ", $meta->address)));
					$item->latitude = $meta->latitude;
					$item->longitude = $meta->longitude;
					$item->phone = $meta->phone;
					$item->fax = $meta->fax;
					$item->subtitle = $meta->subtitle;		
					$item->url = isset($meta->url) ? $meta->url : '';	
					
					$src = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'fmap-image');;
					$item->image = $src ? $src[0] : false;
					
					$data[] = $item;
				}
					
				
			?>
			fmap.init({
				image_dir: '<?php echo plugins_url('fmap/images/'); ?>',
				data: <?php echo json_encode($data); ?>
			});
		});
		// -->
	</script>
<?php
		return ob_get_clean();
	}
}


new fmap();
new fmap_front();
