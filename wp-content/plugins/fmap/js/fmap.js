var map = {
	WIDTH: 600,
	HEIGHT: 400,
	ZOOM: 15,
	DEFAULT_LAT: 41.89026133677721,
	DEFAULT_LONG: -88.30918419604501,
	jq_map: {},
	gmap: {},
	marker: {},
	
	setup: function() {
		this.jq_map = jQuery('#fmap_map');
		
		if (this.jq_map.length == 0)
			return;
		
		this.jq_lat = jQuery('#fmap_latitude');
		this.jq_long = jQuery('#fmap_longitude');

		var lat_val = parseFloat(this.jq_lat.val());
		var long_val = parseFloat(this.jq_long.val());
		
		if ((isNaN(lat_val) || isNaN(long_val)) || (lat_val == 0 && long_val == 0)) {
			lat_val = this.DEFAULT_LAT;
			long_val = this.DEFAULT_LONG;
		}
		
		this.setup_map(lat_val, long_val);
		this.setup_marker(lat_val, long_val);
	},
	
	setup_map: function(lat_val, long_val) {
		map.jq_map.css({width: map.WIDTH, height: map.HEIGHT});
			
		var opts = {
					center: new google.maps.LatLng(lat_val, long_val),
					zoom: map.ZOOM,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				  };
	
		map.gmap = new google.maps.Map(map.jq_map.get(0), opts);
	},
	
	setup_marker: function(lat_val, long_val) {
		var marker_opts = {
							map: map.gmap,
							draggable: true,
							position: new google.maps.LatLng(lat_val, long_val),
							title: 'Map Position'
							};
		map.marker = new google.maps.Marker(marker_opts);		

		google.maps.event.addListener(map.marker, 'dragend', function() { 
			map.update_position();			
		});
						
	},
	
	update_position: function() {
		var position = map.marker.getPosition();
		var lat_val = position.lat();
		var long_val = position.lng();
		
		map.jq_lat.val(lat_val);
		map.jq_long.val(long_val);
	}

};
jQuery(function() {
	map.setup();
});
