var fmap_styles = [
  {
    "featureType": "landscape",
    "stylers": [
      { "color": "#e2e5e5" }
    ]
  },{
    "elementType": "labels.text.stroke",
    "stylers": [
      { "color": "#ffffff" }
    ]
  },{
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      { "color": "#c4c7c9" }
    ]
  },{
    "featureType": "road",
    "elementType": "geometry.stroke",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "road.arterial",
    "elementType": "geometry.fill",
    "stylers": [
      { "color": "#d3d7d8" }
    ]
  },{
    "elementType": "labels.text.fill",
    "stylers": [
      { "color": "#5a6b6b" }
    ]
  },{
    "elementType": "labels.icon",
    "stylers": [
      { "saturation": -100 },
      { "lightness": 17 }
    ]
  },{
    "featureType": "poi",
    "elementType": "geometry.fill",
    "stylers": [
      { "color": "#ced3d5" }
    ]
  },{
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      { "color": "#96b5d3" }
    ]
  },{
    "featureType": "transit.line",
    "stylers": [
      { "color": "#9fa2a2" }
    ]
  },{
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      { "color": "#e1e5e5" }
    ]
  },{
  },{
    "featureType": "road.arterial",
    "elementType": "labels.text.stroke",
    "stylers": [
      { "color": "#e2e5e5" }
    ]
  }
];


var fmap = {
	CONTAINER_SELECTOR: '#map',
	IMAGE_DIR: '',
	IMAGE_PIN: 'pin.png',
	IMAGE_PIN_RULE29: 'pin-rule29.png',
	ID_RULE29: 130,
	ZOOM: 16,
	DEFAULT_LAT: 41.89026133677721,
	DEFAULT_LONG: -88.30918419604501,
	data: [],
	gmap: {},
	jq_map: {},
	info_window: {},
	
	init: function(opts) {
		this.IMAGE_DIR = opts.image_dir;
		this.data = opts.data;	
		
		this.jq_map = jQuery(this.CONTAINER_SELECTOR);
				
		this.setup_map();
		
		this.setup_info_window();
		
		this.setup_markers();
		
		this.setup_resize();
		this.setup_overlay_text();
	},
	
	setup_info_window: function() {
		this.info_window = new InfoWindow(this.gmap);
	},
	
	setup_overlay_text: function() {
		var map_overlay = jQuery('<div class="fmap-overlay" />');
		
		var map_title = jQuery('<h1 class="fmap-title">Around Town</h1>');
		var map_description = jQuery('<p class="fmap-description">Explore some of the places we love that surround our worldwide headquarters.</p>');
		
		map_overlay.append(map_title).append(map_description);
		
		this.gmap.controls[google.maps.ControlPosition.TOP_CENTER].push(map_overlay.get(0));
		
		
		var map_logo = jQuery('<a href="/" class="fmap-overlay-logo"></a>');
		
		this.gmap.controls[google.maps.ControlPosition.TOP_LEFT].push(map_logo.get(0));
	},
	
	setup_resize: function() {
		jQuery(window).resize(function() {
			fmap.update_map_size();
		});
		fmap.update_map_size();
	},
	
	update_map_size: function() {
		var h = jQuery(window).height();
		h -= (40 + 160);
		
		this.jq_map.css({height: h});		
	},
	
	setup_map: function(lat_val, long_val) {
		var styledMap = new google.maps.StyledMapType(fmap_styles, {name: "Styled Map"});
		
		var opts = {
			center: new google.maps.LatLng(this.DEFAULT_LAT, this.DEFAULT_LONG),
			zoom: this.ZOOM,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			streetViewControl: false,
			panControlOptions: {position: google.maps.ControlPosition.LEFT_BOTTOM},
			zoomControlOptions: {position: google.maps.ControlPosition.LEFT_BOTTOM},
			mapTypeControlOptions: {
		      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
		    }
		 };
	
		this.gmap = new google.maps.Map(this.jq_map.get(0), opts);
		
		this.gmap.mapTypes.set('map_style', styledMap);
  		this.gmap.setMapTypeId('map_style');
  		
  		google.maps.event.addListener(this.gmap, 'click', function() {
  			var me = fmap;
			for(var i=0; i<me.data.length; i++) {
				if (me.data[i].selected) {
					me.unselect_marker(i);
					break;
				}
			}
			me.info_window.hide();
  		});
	},
	
	setup_markers: function() {
		var index_select = 0;
		
		for(var i=0; i<this.data.length; i++) {
			var item = this.data[i];
			var opts = {
				map: this.gmap,
				position: new google.maps.LatLng(item.latitude, item.longitude),
				title: item.title,
				flat: true,
				index: i
			};
			
			if (item.id == this.ID_RULE29) {
				opts.icon = this.IMAGE_DIR + this.IMAGE_PIN_RULE29;
				index_select = i;
			} else {
				opts.icon = this.IMAGE_DIR + this.IMAGE_PIN;
			}
						
			opts.visible = true;
		
			var marker = new google.maps.Marker(opts);
			google.maps.event.addListener(marker, 'click', function() {
				fmap.select_marker(this.index);
			})
			
			this.data[i].marker = marker;
			this.data[i].selected = false;
		}
		
		//setTimeout(function() {
		//	fmap.select_marker(index_select);
		//}, 500);
	},
	
	unselect_marker: function(index) {
		this.data[index].selected = false;
	},
	
	select_marker: function(index) {		
		if (!this.data[index].selected) {
			this.data[index].selected = true;
			//hide others
			for(var i=0; i<this.data.length; i++) {
				if (i != index) {
					if (this.data[i].selected) {
						this.unselect_marker(i);
						break;
					}
				}	
			}
			
			var item = this.data[index];
			this.info_window.show(
				item
			);
		} else {
			this.data[index].selected = false;
			this.info_window.hide();
		}
	}
};

function InfoWindow(map) {
	this.OFFSET_LEFT = -329;
	this.OFFSET_TOP = -179;
	this.CENTER_OFFSET_LEFT = 0;
	this.CENTER_OFFSET_TOP = 0;
	this.FADE_TIME = 250;
	this.map_ = map;
	this.jq_ = {};
	this.div_ = {};
	this.latlng_ = false;
	
	this._create();

	google.maps.OverlayView.call(this);
	this.setMap(this.map_);	

	var me = this;
    google.maps.event.addListener(this.map_, "bounds_changed", function() {
    	me.reposition();
    });	
};
InfoWindow.prototype = new google.maps.OverlayView();

InfoWindow.prototype._create = function() {
	this.jq_ = jQuery('<div id="fmap-info-window" />');
	
	this.div_ = this.jq_.get(0);

};
InfoWindow.prototype.hide = function() {
	this.jq_.animate({opacity: 0}, this.FADE_TIME, function() {
		jQuery(this).attr('style', '');
	});
};
InfoWindow.prototype.show = function(info) {
	this.latlng_ = info.marker.getPosition();

	var html = '<div class="fmap-popup-interior">'
	if (info.image != '') {
		if (info.url != '') {
			html += '<a href="' + info.url + '" target="_blank"><img src="' + info.image + '" /></a>';
		} else {
			html += '<img src="' + info.image + '" />';
		}	
	}
	html += '<div class="fmap-popup-text">';
	if (info.url != '') {
		html += '<div class="fmap-popup-title"><a href="' + info.url + '" target="_blank">' + info.title + '</a></div>';		
	} else {
		html += '<div class="fmap-popup-title">' + info.title + '</div>';
	}
	
	if (info.subtitle.length > 0) {
		html += '<div class="fmap-popup-subtitle">' + info.subtitle + '</div>';		
	}
	
	html += '<div class="fmap-popup-description">' + info.description + '</div>';
	
	if (info.phone != '' || info.fax != '') {
		html += '<div class="fmap-popup-numbers">';
		if (info.phone != '') {
			html += '<span><strong>T</strong>' + info.phone + '</span>';	
		}
		if (info.fax != '') {
			html += '<span><strong>F</strong>' + info.fax + '</span>';	
		}
		html += '</div>';	
	}
	html += '</div>';
	
	if (info.address != '') {
		html += '<div class="fmap-popup-address">';
		html += info.address;
		html += '<a href="' + info.map_link + '" target="_blank" title="Directions"><span>Directions</span></a>';		
		html += '</div>';	
	}
	
	
	html += '</div>';
	
	this.reposition();
  
	this.jq_
		.stop()
		.css({opacity: 0, display: 'block'})
		.animate({opacity: 1}, this.FADE_TIME)
		.html(html);

	// The degrees per pixel
	var bounds = this.map_.getBounds();
	var mapDiv = this.map_.getDiv();
	var mapWidth = mapDiv.offsetWidth;
	var mapHeight = mapDiv.offsetHeight;
	var boundsSpan = bounds.toSpan();
	var longSpan = boundsSpan.lng();
	var latSpan = boundsSpan.lat();
	var degPixelX = longSpan / mapWidth;
	var degPixelY = latSpan / mapHeight;
	  
	// The new map center
	var centerX = this.latlng_.lng() - degPixelX * this.CENTER_OFFSET_LEFT;
	var centerY = this.latlng_.lat() - degPixelY * this.CENTER_OFFSET_TOP;
	
	// center the map to the new shifted center
	this.map_.panTo(new google.maps.LatLng(centerY, centerX)); 
};

InfoWindow.prototype.reposition = function() {
	if (!this.latlng_)
		return;
	var pixPosition = this.getProjection().fromLatLngToDivPixel(this.latlng_);
  
  	var left = pixPosition.x + this.OFFSET_LEFT;
  	var top = pixPosition.y + this.OFFSET_TOP;
  
	this.jq_.css({left: left, top: top})
};

InfoWindow.prototype.draw = function() {
	var panes = this.getPanes();
	
	if (this.div_.parentNode != panes.floatPane) {
	    // The panes have changed.  Move the div.
	    this.div_.parentNode.removeChild(this.div_);
	    panes.floatPane.appendChild(this.div_);
 	 }
};