<?php
/*
Plugin Name: Custom Project
Description: Stores information about projects
Author: Floodlight Design
Author URI: http://floodlightdesign.com
*/

add_action( 'plugins_loaded', array( 'fld_project', 'init' ) );

register_activation_hook( __FILE__, 'fld_project_install' );

function fld_project_install() {
	global $wpdb;
	$prefix = $wpdb->prefix;

	$query = <<<EOS
CREATE TABLE IF NOT EXISTS {$prefix}termmeta (
`meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`)
);
EOS;
	
	$wpdb->query($query);
}

class fld_project {
	public static function init() {
		require_once get_template_directory() . '/core/index.php';
		
		new custom_post_type(array(
			'names' => array(
				'post_type' => 'fld_project',
				'single' => 'Project',
				'plural' => 'Projects'
			),
			'fields' => array(
				array(
					'name' => 'site_url',
					'label' => 'Site URL',
					'type' => 'text'
				),
				array(
					'name' => 'thumbnail',
					'label' => 'Thumbnail',
					'type' => 'media'
				),
				array(
					'name' => 'top_gallery',
					'label' => 'Top Gallery',
					'type' => 'media'
				),
				array(
					'name' => 'bottom_gallery',
					'label' => 'Body Gallery',
					'type' => 'media'
				),
				array(
					'name' => 'clients',
					'label' => 'Client',
					'type' => 'dropdown_taxonomies',
					'taxonomy_type' => 'fld_project_client',
					'taxonomy_label' => 'Clients'
				),
				array(
					'name' => 'capabilities',
					'label' => 'Capabilities',
					'type' => 'list_taxonomies',
					'taxonomy_type' => 'fld_project_capability',
					'taxonomy_label' => 'Capabilities'
				)
			)
		));
		
		new fld_custom_taxonomy(array(
				array(
					'name' => 'client_thumbnail',
					'label' => 'Client Thumbnail',
					'type' => 'media'
				)
			), 'fld_project_client'
		);
				
	}
}
	