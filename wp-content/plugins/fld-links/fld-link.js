;(function ( $, window, document, undefined ) {
    'use strict';

    var pluginName = "fldLink",
        defaults = {
            propertyName: "value"
        };

    function Plugin( element, options ) {
        this.element = element;

        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype.init = function () {
        var that = this;

        that.table = $(that.element);

        that.add = $(that.options.add).click(function ( e ) {
            that.addInput();
            e.preventDefault();
        });
    };

    Plugin.prototype.addInput = function () {
        var that = this,
            last_row = that.table.find('tr').last(),
            new_row = last_row.clone(),
            last_index = last_row.data('index'),
            next_index = last_index + 1;

        new_row.data('index', next_index);
        new_row.find('.fld-input-url')
            .attr('id', 'fld-link-' + next_index)
            .attr('name', 'fld-link[' + next_index + '][url]').val('');
        new_row.find('.fld-input-label')
            .attr('id', 'fld-link-' + next_index + '-label')
            .attr('name', 'fld-link[' + next_index + '][label]').val('');
        new_row.find('label').attr('for', 'fld-link-' + next_index).text('Link ' + (next_index + 1));
        last_row.after(new_row);
    };

    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if ( !$.data(this, pluginName )) {
                $.data( this, pluginName, new Plugin( this, options ) );
            }
        });
    };
} ( jQuery, window, document ) );

jQuery(function() {
    jQuery('.fld-link-table').fldLink({
        add: '.fld-link-add'
    });
});