<?php
/*
Plugin Name: Floodlight Links
Description: Adds ability to add any number of basic links to page
Version: 1.0
Author: Floodlight Design
Author URI: http://floodlightdesign.com/
*/

class Fld_Links_Plugin {
	const META_KEY = 'rule29_related_links';

	function __construct() {
		add_action( 'add_meta_boxes', array( &$this, 'add_meta' ) );
        add_action( 'save_post', array( &$this, 'save_meta' ) );
        add_action( 'admin_enqueue_scripts', array( &$this, 'enqueue' ) );
	}

	function add_meta() {
		add_meta_box(
            'fld-links',
            'Related Links',
            array( &$this, 'meta_content' ),
            'page',
            'normal',
            'high'
        );
	}

	function enqueue() {
		wp_enqueue_script(
			'fld-link',
			plugins_url( '/fld-links/fld-link.js' ),
			array( 'jquery' ),
			'1.0'
		);
	}

	function save_meta( $post_id ) {
		if ( !isset($_POST['fld_links_nonce']) || !wp_verify_nonce( $_POST['fld_links_nonce'], 'meta' )) {
            return $post_id;
        }

        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
            return $post_id;

        $links = array();

        if ( ! empty( $_POST['fld-link'] ) ) {
        	foreach( $_POST['fld-link'] as $link ) {
        		if ( ! empty( $link['url'] ) ) {
        			$links[] = $link;
        		}
        	}
        }
    	update_post_meta( $post_id, self::META_KEY, $links );

        return $post_id;
	}

	function meta_content( $post ) {
		wp_nonce_field( 'meta', 'fld_links_nonce' );

		$existing_links = get_post_meta( get_the_ID(), self::META_KEY, true );

        echo '<table class="form-table fld-link-table">';
        if ( ! empty( $existing_links ) ) {
	        foreach( $existing_links as $key => $link ) {
	        	$this->table_row( $link, $key );
	        }
        } else {
        	$this->table_row( array( 'url' => '', 'label' => '' ), 0 );
        }
        echo '</table>';
        echo '<button class="button fld-link-add">Add Link</button>';
	}

	private function table_row( $value, $index ) {
		echo '<tr data-index="' . $index . '"><th>';
        echo '<label for="fld-link-' . $index . '">Link ' . ( $index + 1 ) . '</label>';
        echo '</th><td>';
        echo '<input type="text" name="fld-link[' . $index . '][url]" class="fld-input-url" id="fld-link-' . $index . '" value="' . $value['url'] . '" placeholder="URL">';
        echo '<input type="text" name="fld-link[' . $index . '][label]" class="fld-input-label" id="fld-link-' . $index . '-label" value="' . $value['label'] . '" placeholder="Label">';
        echo '</td></tr>';
	}
}

new Fld_Links_Plugin();
