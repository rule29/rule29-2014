<?php

class instagram {
	const API_HOST = 'api.instagram.com';
	const SOCK_TIMEOUT = 10;
	const CHUNK_SIZE = 512;
	
	private $client_id = '';
	private $client_secret = '';
	private $error = '';
	
	public function __construct($client_id, $client_secret) {
		$this->client_id = $client_id;
		$this->client_secret = $client_secret;
	}
	
	public function get_last_error() {
		return $this->error;
	}
	
	
	public function user_search($query) {
		return self::get('/v1/users/search', array('client_id' => $this->client_id, 'q' => $query, 'count' => 50));
	}
	
	public function user_media($user_id, $access_token, $count) {
		return self::get(
			'/v1/users/' . $user_id . '/media/recent',
			array(
				/*'access_token' => $access_token,*/
				'client_id' => $this->client_id,
				'count' => $count
			)
		);
	}
	
	private function get($endpoint, $fields) {
		$request = "";
		if (!empty($fields)) {
			$request = array();
			foreach($fields as $key => $val) {
				$request[] = $key .= '=' . urlencode($val);
			}
			$request = '?' . join('&', $request);
		}
		
		$host = self::API_HOST;
		
		//create request
        $content =
            "GET $endpoint{$request} HTTP/1.0\r\n" .
            "Host: $host\r\n" .
            "Connection: close\r\n" .
            "\r\n" .
            "\r\n";

		//open ssl connection
		$errno = $str = "";
		$sock = fsockopen('ssl://' . $host, 443, $errno, $errstr, self::SOCK_TIMEOUT);
		if(!$sock) {
		 	$this->error = 'Could not connect to Instagram API! (' . $errno . ', ' . $errstr . ')';
			return false;
		}
		
		
		
		//load response
		$response = "";
		fwrite($sock, $content);
		stream_set_timeout($sock,  self::SOCK_TIMEOUT);
		
		$info = stream_get_meta_data($sock);
		while ((!feof($sock)) && (!$info["timed_out"])) {
		    $response .= fread($sock, self::CHUNK_SIZE);
		    $info = stream_get_meta_data($sock);
		}
		fclose($sock);		
		
		//parse response
		list($headers, $response) = explode("\r\n\r\n", $response, 2);

		return json_decode($response);
	}
}