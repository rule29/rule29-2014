var feed_cache = {
	ajax_url: '',
	
	init: function() {
		this.init_settings();
		this.init_feeds();
	},
	
	init_settings: function() {
		if (typeof feed_cache_settings != 'undefined') {
			for (var i=0,len=feed_cache_settings.length; i<len; i+=2) {
				var key = feed_cache_settings[i];
				var val = feed_cache_settings[i+1];
				this[key] = val;
			}
		}
	},
	
	init_feeds: function() {
		jQuery('.feed-cache').each(function() {
			feed_cache.init_feed(jQuery(this));
		});	
	},
	
	init_feed: function(jq) {
		var source = jq.data('source');
		var callback = jq.data('callback');
		var data = {
			source: source
		}
		if (source == 'twitter') {
			this.init_feed_twitter(jq, data);	
		} else if (source == 'lastfm') {
			this.init_feed_lastfm(jq, data);	
		} else if (source == 'instagram') {
			this.init_feed_instagram(jq, data);	
		}
		
		this.query(data, function(result) {
			var fn = window[callback];
			var result_json = jQuery.parseJSON(result);
			if (typeof fn === 'function')
				fn(jq, result_json);
		});
	},
	
	init_feed_twitter: function(jq, data) {
		data.user = jq.data('user');
	},
	
	init_feed_lastfm: function(jq, data) {
		data.user = jq.data('user');
	},
	
	init_feed_instagram: function(jq, data) {
		data.user = jq.data('user');
	},
	
	query: function(data, callback) {
		data.action = 'feed_cache';
		jQuery.post(this.ajax_url, data, callback);
	}
};
jQuery(function() {
	feed_cache.init();
});