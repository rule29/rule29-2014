<?php
/*
Plugin Name: Feed Cache
Description: Caching for various feeds. Special modifications for rule29
Author: Floodlight Design
Author URI: http://floodlightdesign.com
*/

register_activation_hook( __FILE__, 'feed_cache_install' );
register_deactivation_hook( __FILE__, 'feed_cache_uninstall' );

add_action( 'plugins_loaded', array( 'feed_cache', 'init' ) );

function feed_cache_install() {
	global $wpdb;
	$prefix = $wpdb->prefix;

	$query = <<<EOS
CREATE TABLE {$prefix}feed_cache (
	`type` VARCHAR(32) NOT NULL,
	`key` VARCHAR(256) NOT NULL,
	`date_update` INT UNSIGNED NOT NULL,
	`result` TEXT NOT NULL,
	PRIMARY KEY (`type`, `key`)
);
EOS;

	$wpdb->query($query);
}

function feed_cache_uninstall() {
	global $wpdb;
	$prefix = $wpdb->prefix;

	$query = <<<EOS
DROP TABLE {$prefix}feed_cache;
EOS;

	$wpdb->query($query);
}


class feed_cache {
	public static function init() {
		add_action('wp_ajax_feed_cache', array('feed_cache', 'ajax_callback'));
		add_action('wp_ajax_nopriv_feed_cache', array('feed_cache', 'ajax_callback'));
		add_shortcode( 'feed_cache', array('feed_cache', 'shortcode'));
		add_action( 'wp_enqueue_scripts', array('feed_cache', 'scripts') );
	}

	public static function ajax_callback() {
		$result = new stdClass();

		$source = isset($_POST['source']) ? $_POST['source'] : '';

		if ($source == 'twitter') {
			new feed_cache_twitter($result);
		} elseif ($source == 'lastfm') {
			new feed_cache_lastfm($result);
		} elseif ($source == 'instagram') {
			new feed_cache_instagram($result);
		}

		echo json_encode($result);
		die();
	}

	public static function shortcode($opts) {
		ob_start();

		$data_attributes = array();
		foreach($opts as $key => $value) {
			$data_attributes[] = <<<EOS
data-$key="$value"
EOS;
		}

		$data_attributes_str = join(' ', $data_attributes);

		echo <<<EOS
<div class="feed-cache" $data_attributes_str></div>
EOS;

		return ob_get_clean();
	}

	public static function scripts() {
		wp_enqueue_script('feed_cache_script', plugins_url('feed_cache/js/feed_cache.js'), array('jquery'));
		wp_localize_script('feed_cache_script', 'feed_cache_settings', array('ajax_url', admin_url('admin-ajax.php')));
	}
}

abstract class feed_cache_source {
	const CACHE_TIME = 300;
	protected $result;

	abstract protected function load_fields();
	abstract protected function load();

	function __construct(&$result) {
		$this->result = $result;
		$this->load_fields();
		$this->load();
	}

	protected function check_cache($type, $key, $ignore_timeout = false) {
		global $wpdb;
		$key = esc_sql($key);
		$prefix = $wpdb->prefix;
		$query = <<<EOS
SELECT `date_update`, `result`
FROM {$prefix}feed_cache
WHERE `type` = '$type' AND `key` = '$key'
EOS;
		$result = $wpdb->get_row($query);

		if ($result) {
			if ($ignore_timeout) {
				return json_decode($result->result);
			} else {
				$time = time();
				if ($result->date_update > $time - self::CACHE_TIME) {
					return json_decode($result->result);
				}
			}
		}

		return false;
	}

	protected function store_cache($type, $key) {
		global $wpdb;

		$key = esc_sql($key);
		$value = esc_sql(json_encode($this->result));
		$prefix = $wpdb->prefix;

		$time = time();

		$query = <<<EOS
INSERT INTO {$prefix}feed_cache (`type`, `key`, `date_update`, `result`) VALUES
(
	'$type',
	'$key',
	$time,
	'$value'
)
ON DUPLICATE KEY UPDATE `result` = '$value', `date_update` = $time;
EOS;

		$wpdb->query($query);
	}
}


class feed_cache_lastfm extends feed_cache_source {
	private $user;

	protected function load_fields() {
		$this->user = isset($_POST['user']) ? strtolower($_POST['user']) : '';
	}
	
	public function return_cache_time() {
		return 300;
	}

	protected function load() {
		if (empty($this->user)) {
			$this->result = false;
			return;
		}

		$success = false;

		if ($result = $this->check_cache('lastfm', $this->user)) {
			$success = true;

			foreach($result as $key => $value) {
				$this->result->$key = $value;
			}

			$this->result->cached = true;
		} else {
			add_filter('wp_feed_cache_transient_lifetime', array($this, 'return_cache_time'));
			$feed = fetch_feed('http://ws.audioscrobbler.com/1.0/user/' . $this->user . '/recenttracks.rss');
			remove_filter('wp_feed_cache_transient_lifetime', array($this, 'return_cache_time'));
			if (!is_wp_error($feed)) {
				$success = true;
				$items = $feed->get_items();

				$songs = array();
				foreach($items as $item) {
					$title = $item->get_title();
					$parts = explode(' – ', $title, 2);

					$song = new stdClass();
					$song->artist = $parts[0];
					$song->song = $parts[1];
					$songs[] = $song;
				}

				$this->result->songs = $songs;

				$this->store_cache('lastfm', $this->user);

			}
		}

		if (!$success) {
			$result = $this->check_cache('lastfm', $this->user, true);
			if ($result) {
				$this->result = $result;
			} else {
				$this->result = false;
			}
		}
	}

}

class feed_cache_twitter extends feed_cache_source {
	const CONSUMER_KEY = 'rMBsLKkiJujp5hUDC1GjCw';
	const CONSUMER_SECRET = '6M7eYEd0SG6bsirXM0xtSiyZhAq9UQNZlHzEZ1yng';
	const OAUTH_TOKEN = '14156324-m5ETDhQ4WP8POJErvhQsHeWkrgjvO1pttaznK7lqF';
	const OAUTH_TOKEN_SECRET = '5HHRflbAavOO1NN19mHewZwLBEWx1aLdU2cmgrUh8';

	private $user;

	protected function load_fields() {
		$this->user = isset($_POST['user']) ? strtolower($_POST['user']) : '';
	}

	protected function load() {
		if (empty($this->user)) {
			$this->result = false;
			return;
		}

		$success = false;

		if ($result = $this->check_cache('twitter', $this->user)) {
			$success = true;

			foreach($result as $key => $value) {
				$this->result->$key = $value;
			}

			$this->result->cached = true;
		} else {
			require_once 'library/twitteroauth/twitteroauth.php';
			$connection = new TwitterOAuth(self::CONSUMER_KEY, self::CONSUMER_SECRET, self::OAUTH_TOKEN, self::OAUTH_TOKEN_SECRET);
			$user_timeline = $connection->get('statuses/user_timeline', array('screen_name' => $this->user, 'count' => 10, 'include_rts' => false, 'exclude_replies' => true));
			if (!empty($user_timeline) && is_array($user_timeline)) {
				$item = $user_timeline[0];
				$this->result->text = $this->parseTweet($item->text);
				$this->result->screen_name = $item->user->screen_name;
				$this->result->name = $item->user->name;
				$this->result->tweet_age = $this->parseTime($item->created_at);
				$this->result->tweet_date = strtotime($item->created_at);

				$this->store_cache('twitter', $this->user);

				$success = true;
			}
		}

		if (!$success) {
			$result = $this->check_cache('twitter', $this->user, true);
			if ($result) {
				$this->result = $result;
			} else {
				$this->result = false;
			}
		}
	}

	private function parseTime($latest_time) {
		$pub_date = strtotime($latest_time);
		$seconds = time() - $pub_date;
		$minutes = $seconds / 60;
		$hours = $minutes / 60;
		$days = $hours / 24;
		if ($minutes < 60) {
			$length = round($minutes,0);
			if ($length == 1)
				$units = "minute";
			else
				$units = "minutes";
		} else if ($hours < 24) {
			$length = round($hours, 0);
			if ($length == 1)
				$units = "hour";
			else
				$units = "hours";
		} else {
			$length = round($days, 0);
			if ($length == 1)
				$units = "day";
			else
				$units = "days";
		}
		return "$length $units";
	}

	private function parseTweet($tweet) {
		$tweet = preg_replace("/(http:\/\/)([^\s]+)/u", '<a href="$1$2" target="_blank">$1$2</a>', $tweet);
		$tweet_words = explode(" ",$tweet);
		$full_tweet = "";
		foreach($tweet_words as $word) {
			if (substr($word, 0, 1) == "@") {
				$username = substr($word,1);
				$extra = "";
				if (strpos($username,":")) {
					$username = substr($username,0,strpos($username,":"));
					$extra = substr($word,strlen($username) + 1);
				}
				$full_tweet .= "<a href=\"https://twitter.com/" . $username . "\" target=\"_blank\" class='fldtwitter-at-reply'>@" . $username . "</a>" . $extra . " ";
			} else {
				$full_tweet .= $word . " ";
			}
		}
		return $full_tweet;
	}
}

//https://instagram.com/oauth/authorize/?client_id=9a103c7be0eb4032888f0fc554f3b814&redirect_uri=http://rule29.com&response_type=token

class feed_cache_instagram extends feed_cache_source {
	const INSTAGRAM_CLIENT_ID = '9a103c7be0eb4032888f0fc554f3b814';
	const INSTAGRAM_CLIENT_SECRET = '6dff52193cc8438c92afeac3436635b2';
	const INSTAGRAM_ACCESS_TOKEN = '235404573.9a103c7.17e2907b5ada442689e7d50c6110e5a1';

	private $user;

	protected function load_fields() {
		$this->user = isset($_POST['user']) ? strtolower($_POST['user']) : '';
	}

	protected function load() {
		if (empty($this->user)) {
			$this->result = false;
			return;
		}

		$success = false;

		if ($result = $this->check_cache('instagram', $this->user)) {
			$success = true;

			foreach($result as $key => $value) {
				$this->result->$key = $value;
			}

			$this->result->cached = true;
		} else {
			require_once 'library/instagram.class.php';

			$instagram = new instagram(self::INSTAGRAM_CLIENT_ID, self::INSTAGRAM_CLIENT_SECRET);

			$user = $instagram->user_search($this->user);			
			
			if ($user) {
				if (isset($user->meta) && isset($user->meta->code) && $user->meta->code == 200) {
					$user_id = 0;
					$user_name = "";
					for($i=0; $i<count($user->data); $i++) {
						if (strtolower($user->data[$i]->username) == $this->user) {
							$user_id = $user->data[$i]->id;
							$user_name = $user->data[$i]->username;
							
							break;	
						} 	
					}
					
					if ($user_id > 0) {
						$media = $instagram->user_media($user_id, self::INSTAGRAM_ACCESS_TOKEN, 6);
						if ($media) {
							if (isset($media->meta) && isset($media->meta->code) && $media->meta->code == 200) {
								$success = true;
	
								$this->result->user_id = $user_id;
								$this->result->user_name = $user_name;
								$this->result->images = array();
	
								$this->result->media = $media->data;
	
								foreach( $this->result->media as $media_item ) {
									$media_item->human_time = human_time_diff( $media_item->created_time );
								}
	
								$this->store_cache('instagram', $this->user);
							}
						}
					}
				}
			}
		}

		if (!$success) {
			$result = $this->check_cache('instagram', $this->user, true);
			if ($result) {
				$this->result = $result;
			} else {
				$this->result = false;
			}
		}
	}
}
