<?php
define( 'WP_ADMIN', true );

/** Load WordPress Bootstrap */
require_once( dirname( dirname(  dirname( dirname( __FILE__ ) ) ) ) . '/wp-load.php' );

/** Allow for cross-domain requests (from the frontend). */
send_origin_headers();

/** Load WordPress Administration APIs */
require_once( ABSPATH . 'wp-admin/includes/admin.php' );

@header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
@header( 'X-Robots-Tag: noindex' );

send_nosniff_header();
nocache_headers();

$form_id = intval($_POST['form_id']);

if ($form_id === 0)
	die( '0' );

do_action( 'admin_init' );

if (isset($_COOKIE['source']))
	$_POST['source'] = $_COOKIE['source'];
else
	$_POST['source'] = 'not set';

$referrer = isset($_POST['_wp_http_referer']) ? $_POST['_wp_http_referer'] : '';
$botcheck = $_POST['message']; 
$pattern = '</a>';
        
        //NOT working...need to test again
        if(preg_match($pattern, $botcheck)) {
            $success = false;
        }
		else {
			$success = fld_form_logger::submit($form_id, $_POST);
		}


if ($success) {
	$success_page = get_option('fld_form_logger_success', '');
	
	if (empty($success_page)) {
		wp_redirect(get_site_url() . preg_replace('/\?.*/', '', $referrer) . '?success=1');
	} else {
		wp_redirect($success_page);
	}
} else {
	wp_redirect(get_site_url() . preg_replace('/\?.*/', '', $referrer) . '?submission_error=1');	
}
