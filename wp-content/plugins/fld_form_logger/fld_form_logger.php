<?php
/*
Plugin Name: Floodlight Forms
Description: Logs and Emails Form Submissions
Version: 1.0
Author: Floodlight Design
Author URI: http://floodlightdesign.com/
*/

class fld_form_logger {
	const SUBMIT_URI = '/fld_form_logger/submit.php';
	const ACTIONS_URI = '/fld_form_logger/actions.php';
	const CLASS_NAME = 'fld_form_logger';

	const SECTION_ID = 'fld_form_logger';
	const OPTION_GROUP = 'fld_form_logger_options';
	const SETTINGS_SLUG = 'fld_form_logger_settings';

	static $forms;
	static $emails;

	function __construct() {
		add_action('admin_menu', array('fld_form_logger', 'admin_menu'));
		add_action('wp_enqueue_scripts', array( 'fld_form_logger', 'enqueue' ) );

		self::$forms = array(
			1 => array(
				'name' => 'Client',
				'columns' => array(
					'name' => 'Name',
					'email' => "Email Address"
				),
				'email' => 'client.html',
			),
			2 => array(
				'name' => 'Job Hunter',
				'columns' => array(
					'name' => "Name",
					'email' => "Email Address"
				),
				'email' => 'job.html',
			),
			3 => array(
				'name' => 'Old Friend',
				'columns' => array(
					'name' => "Name",
					'email' => "Email Address"
				),
				'email' => 'friend.html',
			),
			4 => array(
				'name' => 'General Contact',
				'columns' => array(
					'name' => 'Name',
					'email' => "Email Address"
				),
			),
		);

		self::$emails = array();

		add_action( 'wp_ajax_nopriv_fld_form_logger', array(self::CLASS_NAME, 'ajax_submit') );
		add_action( 'wp_ajax_fld_form_logger', array(self::CLASS_NAME, 'ajax_submit') );

		register_activation_hook( __FILE__, array($this,'activate') );
		register_deactivation_hook( __FILE__, array($this,'deactivate') );
	}

	function enqueue() {
		wp_enqueue_script( 'fld_form_ajax_request', plugins_url() . '/' . self::CLASS_NAME . '/ajax.js', array( 'jquery' ) );
		wp_localize_script( 'fld_form_ajax_request', 'fld_form_ajax_url', array( 'url' => admin_url( 'admin-ajax.php' ) ) );
	}

	function admin_menu() {
		add_menu_page('Forms', 'Forms', 'activate_plugins', self::CLASS_NAME, array(self::CLASS_NAME, 'create_page'), false, 86);
		add_submenu_page(self::CLASS_NAME, 'Form Settings', 'Form Settings', 'activate_plugins', self::SETTINGS_SLUG, array(self::CLASS_NAME,'create_settings_page'));

		add_settings_section(self::SECTION_ID, 'Form Settings', array('fld_form_logger', 'settings_text'), self::SETTINGS_SLUG);

		register_setting( self::OPTION_GROUP, 'fld_form_logger_emails');
		register_setting( self::OPTION_GROUP, 'fld_form_logger_subject');
		register_setting( self::OPTION_GROUP, 'fld_form_logger_from_name');
		register_setting( self::OPTION_GROUP, 'fld_form_logger_from_email');
		register_setting( self::OPTION_GROUP, 'fld_form_logger_success');
		register_setting( self::OPTION_GROUP, 'fld_form_mandrill_api');

		foreach(self::$forms as $form) {
		    $label = $form['name'] . ' Admin Email(s)';
            $email_option_name = self::to_option_name($label);

            $subject = $form['name'] . ' Subject';
            $subject_option_name = self::to_option_name($subject);

			register_setting( self::OPTION_GROUP, $email_option_name);
			add_settings_field(
			    $email_option_name,
			    $label,
			    array('fld_form_logger', 'create_input'),
			    self::SETTINGS_SLUG,
			    self::SECTION_ID,
			    array(
                    'name' => $email_option_name,
                    'value' => get_option($email_option_name, 'web@floodlightdesign.com')
                )
            );

            register_setting( self::OPTION_GROUP, $subject_option_name);
            add_settings_field(
                $subject_option_name,
                $subject,
                array('fld_form_logger', 'create_input'),
                self::SETTINGS_SLUG,
                self::SECTION_ID,
                array(
                    'name' => $subject_option_name,
                    'value' => get_option($subject_option_name, 'New Submission')
                )
            );
		}

		// add_settings_field('fld_form_logger_emails', 'Admin Email(s)', array('fld_form_logger', 'create_input'), self::SETTINGS_SLUG, self::SECTION_ID, array('name' => 'fld_form_logger_emails', 'value' => get_option('fld_form_logger_emails', 'web@floodlightdesign.com')));
		add_settings_field('fld_form_logger_subject', 'Subject', array('fld_form_logger', 'create_input'), self::SETTINGS_SLUG, self::SECTION_ID, array('name' => 'fld_form_logger_subject', 'value' => get_option('fld_form_logger_subject', 'Form Submission')));
		add_settings_field('fld_form_logger_from_name', 'From Name', array('fld_form_logger', 'create_input'), self::SETTINGS_SLUG, self::SECTION_ID, array('name' => 'fld_form_logger_from_name', 'value' => get_option('fld_form_logger_from_name', 'No Reply')));
		add_settings_field('fld_form_logger_from_email', 'From Email', array('fld_form_logger', 'create_input'), self::SETTINGS_SLUG, self::SECTION_ID, array('name' => 'fld_form_logger_from_email', 'value' => get_option('fld_form_logger_from_email', 'no-reply@floodlightdesign.com')));
		add_settings_field('fld_form_logger_success', 'Thank You Page (Non-AJAX)', array('fld_form_logger', 'create_input'), self::SETTINGS_SLUG, self::SECTION_ID, array('name' => 'fld_form_logger_success', 'value' => get_option('fld_form_logger_success', '')));

		add_settings_field('fld_form_mandrill_api', 'Mandrill API Key', array('fld_form_logger', 'create_input'), self::SETTINGS_SLUG, self::SECTION_ID, array('name' => 'fld_form_mandrill_api', 'value' => get_option('fld_form_mandrill_api', 'cde31372-dbea-43c0-8e1b-ffd4cfe5f351')));

	}

    public static function to_option_name( $string ) {
        $option_name = preg_replace('/ /', '_', strtolower($string));
        $option_name = preg_replace('/[^\w-_]/', '', $option_name);

        return $option_name;
    }

	function settings_text() {
		'<p>Form Settings</p>';
	}

	function create_input($args) {
		echo "<input type='text' name='{$args['name']}' value='{$args['value']}' />";
	}

	function create_settings_page() {
		?>
	<div class="wrap">
		<form action="<?php echo admin_url('/options.php'); ?>" method="post">
			<input type="hidden" name="page" value="<?php echo self::SETTINGS_SLUG; ?>" />
		<?php
			settings_fields(self::OPTION_GROUP);
			do_settings_sections(self::SETTINGS_SLUG);
			submit_button();
		?>
		</form>
	</div>
	<?php

	}

	function create_page() {
		?>
		<div class="wrap">
		<h2>Form Data</h2>
		<?php
		global $wpdb;

		if (isset($_REQUEST['id'])) {
			$id = intval($_REQUEST['id']);
			if (is_int($id)) {
				self::show_form_data($id);
			} else {
				echo '<p>Sorry, no results could be found.</p>';
			}

		} elseif (isset($_REQUEST['form_id'])) {
			$form_id = intval($_REQUEST['form_id']);

			if (is_int($form_id) && $form_id > 0) {
				self::show_form_table($form_id);
			} else {
				echo '<p>Sorry, no results could be found.</p>';
			}
		} else {
			include_once 'fld_form_table_forms.php';

			$fld_form_table = new fld_form_table_forms(self::$forms);
			$fld_form_table->prepare_items();
			$fld_form_table->display();
		}
		?>
		</div>
		<?php
	}

	function network_propagate($pfunction, $networkwide) {
		global $wpdb;
		if (function_exists('is_multisite') && is_multisite()) {
			if ($networkwide) {
				$old_blog = $wpdb->blogid;
				$blogids = $wpdb->get_col("SELECT blog_id FROM {$wpdb->blogs}");
				foreach ($blogids as $blog_id) {
					switch_to_blog($blog_id);
					call_user_func($pfunction, $networkwide);
				}
				switch_to_blog($old_blog);
				return;
			}
		}
		call_user_func($pfunction, $networkwide);
	}

	function activate($networkwide) {
		$this->network_propagate(array($this, '_activate'), $networkwide);
	}

	function deactivate($networkwide) {
		$this->network_propagate(array($this, '_deactivate'), $networkwide);
	}

	function _activate() {
		global $wpdb;
		$wpdb->query("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}fld_form_log` ( `id` int(10) NOT NULL AUTO_INCREMENT, `form_id` int(10) NOT NULL,`time` int(10) NOT NULL,`data` text NOT NULL,PRIMARY KEY (`id`))");
	}

	function _deactivate() {

	}

	public static function show_form_table($form_id) {
			global $wpdb;
		?>
		<h3><?php echo self::$forms[$form_id]['name']; ?></h3>
		<form method="POST">
		<?php
			wp_nonce_field('table_actions', 'fld_form_logger');
			include_once 'fld_form_table.php';

			$args = array(
				'table' => $wpdb->prefix . 'fld_form_log',
				'columns' => self::$forms[$form_id]['columns'],
				'sortable' => array(
					'col_time'=>array('time', true),
				),
				'list_page' => 'fld_form_logger',
			);
			$fld_form_table = new fld_form_table($args);

			if ($fld_form_table->current_action() === 'delete' && wp_verify_nonce($_POST['fld_form_logger'], 'table_actions')) {
				self::delete_entries($_POST['entries']);
			}
			$fld_form_table->prepare_items();
			$fld_form_table->search_box('Search', 'form_search');
			$fld_form_table->display();
		?>
		</form>
		<a href="<?php echo plugins_url() . '/' . self::CLASS_NAME . '/export.php?form_id=' . $form_id; ?>">Download Form Data as CSV</a>
		<?php
	}

	public static function show_form_data($id) {
		global $wpdb;
		$id = intval($id);
		$result = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}fld_form_log WHERE id = $id ORDER BY `time` DESC");
		if (empty($result)) {
			echo '<p>Sorry, no results could be found.</p>';
			return;
		}
		?>
		<a href="<?php echo admin_url('/admin.php?page=' . self::CLASS_NAME . '&form_id=' . $result->form_id); ?>">Back to Form</a>
		<table>
			<tbody>
		<?php
		$result_data = json_decode($result->data);
		foreach ($result_data as $key => $value) {
			$key = htmlspecialchars(ucwords(preg_replace('/[-_]/', ' ', $key)));
			if (is_array($value))
				$value = implode(', ', $value);
			$value = htmlspecialchars($value);
			echo "<tr><th>$key</th><td>$value</td></tr>";
		}
		?>
				<tr>
					<th>Time</th>
					<td>
					<?php
						echo date('F j, Y g:i a e', $result->time);
					?>
					</td>
				</tr>
			</tbody>
		</table>
		<form id="delete-form" method="POST" action="<?php echo plugins_url() . self::ACTIONS_URI; ?>">
			<input type="hidden" name="entry_id" value="<?php echo $result->id; ?>" />
			<input type="hidden" name="form_id" value="<?php echo $result->form_id; ?>" />
			<input type="hidden" name="action" value="delete" />
			<?php wp_nonce_field('delete', 'fld_form_logger'); ?>
			<input type="submit" value="Delete" />
		</form>
		<script>
			jQuery(function() {
				jQuery('#delete-form').submit(function() {
					if(confirm('Are you sure you want to delete this entry?')) {
						return true;
					} else {
						return false;
					}
				});
			});
		</script>
		<?php
	}

	function ajax_submit() {
		$data = $_POST['data'];

		$form_id = intval($_POST['data']['form_id']);

		if ($form_id <= 0)
			die( '0' );

        $success = fld_form_logger::submit($form_id, $data);
		$response = json_encode( array( 'success' => $success ) );
		header( "Content-Type: application/json" );
		echo $response;
		exit;

	}

	static function captcha_validate($data) {
	    require_once('recaptchalib.php');
	    //$privatekey = "6Ldcq-cSAAAAAPOKSSBag6BaGbEQll_3zhao7mP-";
	    $privatekey = "6LemreoSAAAAAFe1ENRQD4V6eSdH20dXBm_kBY2u";
	    $valid = recaptcha_check_answer(
	        $privatekey,
	        $_SERVER["REMOTE_ADDR"],
	        $data["recaptcha_challenge_field"],
	        $data["recaptcha_response_field"] );
	    return $valid->is_valid;
	}

	function submit($form_id, $data) {
		if ( ! self::captcha_validate($data) ) {
			return false;
		}

		unset($data['fld_form_logger']);
		unset($data['_wp_http_referer']);
		unset($data['form_id']);
		unset($data['submit']);
		unset($data['recaptcha_challenge_field']);
		unset($data['recaptcha_response_field']);

        unset($data['fld_submission_type']);

        $file_url = '';
        if ( ! empty( $_FILES['work'] ) ) {
        	$file_url = self::upload_file( $_FILES['work'] );
        	if ( ! empty( $file_url ) ) {
        		$data['work'] = $file_url;
        	}
        }

		global $wpdb;
		$wpdb->insert(
			$wpdb->prefix . 'fld_form_log',
			array('data' => json_encode($data), 'time' => time(), 'form_id' => $form_id),
			array('%s')
		);


		$tbody = '';
		foreach($data as $key => $value) {
			$key = ucwords(preg_replace('/[-_]/', ' ', $key));
			if (is_array($value))
				$value = implode(', ', $value);
			$tbody .= "<tr><th>$key</th><td>$value</td></tr>";
		}

		$content = <<<floodlight
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
The following information was submitted:
<table>
<tbody>
$tbody
</tbody>
</table>
</body>
</html>
floodlight;

        $email_option_name = self::$forms[$form_id]['name'] . ' Admin Email(s)';
        $email_option_name = self::to_option_name( $email_option_name );

        $subject_name = self::$forms[$form_id]['name'] . ' Subject';
        $subject_name = self::to_option_name( $subject_name );

		$admin_emails = explode(',', get_option($email_option_name, 'web@floodlightdesign.com'));

		$subject = get_option( $subject_name, 'Form Submission');
		$from_name = get_option('fld_form_logger_from_name', 'No Reply');
		$from_email = get_option('fld_form_logger_from_email', 'no-reply@rule29.com' );
		$api_key = get_option('fld_form_mandrill_api', 'cde31372-dbea-43c0-8e1b-ffd4cfe5f351');

		require_once 'mandrill.class.php';

		$mandrill = new mandrill($api_key);


		$address_arr = array();
		foreach($admin_emails as $admin_email) {
			$address_arr[] = $mandrill->create_address(trim($admin_email));
		}

		if ( ! empty($data['email'] ) ) {
			$client_email_template = 'general.html';

			if ( ! empty( self::$forms[$form_id]['email'] ) ) {
				$client_email_template = self::$forms[$form_id]['email'];
			}

			$client_email_html = file_get_contents(ABSPATH . 'wp-content/plugins/fld_form_logger/' . $client_email_template);

			$client_email_html = str_replace('%data%', $tbody, $client_email_html);


			$client_email_addresses = array(
				$mandrill->create_address( trim( $data['email'] ) )
			);

			$mandrill->messages_send($client_email_html, '', $subject, $from_email, $from_name, $client_email_addresses);
		}

		$mandrill->messages_send($content, '', $subject, $from_email, $from_name, $address_arr);

		return true;
	}

	public function upload_file( $file ) {
		$attachment_url = '';
		if (!empty($file) && $file['error'] == 0) {
			//temp name
			$tmp = $file['tmp_name'];

			$wp_filetype = wp_check_filetype($tmp . $file['name'], null );

			// only allow pdf files
			if ( $wp_filetype['type'] === 'application/pdf' && is_uploaded_file($tmp) ) {
				$basename = basename($file['name'], '.pdf');

				// create filename
				$attachment = substr( md5( time() ), 1, 12 ) . preg_replace('/[\W]+/', '-', strtolower( $basename ) );

				if ( ! empty( $attachment ) ) {
					$wp_upload_dir = wp_upload_dir();

					$dir = $wp_upload_dir['path'] . '/';

					$attachment_url = $wp_upload_dir['url'] . '/' . $attachment . '.pdf';

					$destination_name = $dir . $attachment . '.pdf';

					move_uploaded_file($tmp, $destination_name);
				}
			}
		}
		return $attachment_url;
	}

	function delete_entries($array_of_ids) {
		if (!is_array($array_of_ids) || empty($array_of_ids))
			return false;
		$sanitized_ids = array();
		foreach($array_of_ids as $id) {
			$int_id = intval($id);
			if ($int_id > 0)
				$sanitized_ids[] = $int_id;
		}
		if (empty($sanitized_ids))
			return false;
		$ids_str = implode(',',$sanitized_ids);
		global $wpdb;
		$query = "DELETE FROM `{$wpdb->prefix}fld_form_log` WHERE `id` IN ($ids_str)";
		$wpdb->query($query);
		return true;
	}

	function delete_entry($entry_id) {
		$entry_id = intval($entry_id);
		global $wpdb;
		$wpdb->query("DELETE FROM `{$wpdb->prefix}fld_form_log` WHERE `id` = $entry_id LIMIT 1");
		return true;
	}

    public static function hidden_fields( $form_id = 1 ) {
        wp_nonce_field('submit', 'fld_form_logger', true);
        echo '<input type="hidden" name="form_id" value="' . $form_id . '" />';
    }

	public static function submit_uri() {
		echo plugins_url() . self::SUBMIT_URI;
	}
}
new fld_form_logger();

function fld_form_hidden_fields( $form_id = 1 ) {
    fld_form_logger::hidden_fields( $form_id );
}

function fld_form_submit_uri() {
    fld_form_logger::submit_uri();
}
