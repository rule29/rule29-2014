<?php
define( 'WP_ADMIN', true );

/** Load WordPress Bootstrap */
require_once( dirname( dirname(  dirname( dirname( __FILE__ ) ) ) ) . '/wp-load.php' );

/** Allow for cross-domain requests (from the frontend). */
send_origin_headers();

/** Load WordPress Administration APIs */
require_once( ABSPATH . 'wp-admin/includes/admin.php' );

$form_id = intval($_GET['form_id']);

if (!is_user_logged_in() || !current_user_can('activate_plugins') || !is_int($form_id))
	die( '0' );

send_nosniff_header();
nocache_headers();

do_action( 'admin_init' );

global $wpdb;
$form_submissions = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}fld_form_log WHERE form_id = $form_id");

$columns = array('id', 'time');

foreach($form_submissions as $sub_key => $submission) {
	$submission->data = json_decode($submission->data);
	
	foreach($submission->data as $key => $data) {
		if (!in_array($key, $columns))
			$columns[] = $key;
	}
	
	$form_submissions[$sub_key] = $submission;
}
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=data.csv');

$output = fopen('php://output', 'w');

fputcsv($output, $columns);

foreach($form_submissions as $submission) {
	$data = array();
	foreach($columns as $column) {
		if (isset($submission->data->$column)) {
			$value = $submission->data->$column;
			if (is_array($value))
				$value = implode(', ', $value);
			$data[] = $value;
		} elseif ($column === 'id') {
			$data[] = $submission->id;
		} elseif ($column === 'time') {
			$data[] = date('Y-m-d H:i:s', $submission->time);
		}else {
			$data[] = '';
		}
	}
	
	fputcsv($output, $data);
}


