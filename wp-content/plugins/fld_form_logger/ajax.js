var fld_form_logger_ajax = {
    query: function(data, callback) {
        data.action = 'fld_form_logger';
        jQuery.post(
            fld_form_ajax_url.url, 
            data,
            function( response ) {
                if (jQuery.isFunction(callback))
                    callback(response);
            }
        );      
    },
    
    submit: function(jq_selector, callback) {
        serialized_data = jQuery(jq_selector).serializeArray();
        
        data = {};
        for (i = 0; i < serialized_data.length; i++) {
            name = serialized_data[i].name.replace(/[[]]/, '');
            value = serialized_data[i].value;
            
            if (!data[name]) {
                data[name] = value; 
            } else if (data[name] instanceof Array) {
                data[name].push(value);
            } else {
                data[name] = [data[name], value];
            }
        }
        
        fld_form_logger_ajax.query({
            data: data
        }, callback);
    }

}