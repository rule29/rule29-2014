<?php

class fld_form_table extends WP_List_Table {
	var $columns;
	var $sortable;
	var $table;
	var $edit_page;
	var $list_page;
	var $filters;
	var $form_id;
	var $actions;

	/**
	 * Constructor, we override the parent to pass our own arguments
	 * We usually focus on three parameters: singular and plural labels, as well as whether the class supports AJAX.
	 */
	 function __construct($args) {
	 	$this->columns = (isset($args['columns'])) ? array_merge(array('cb' => '', 'col_time' => 'Time'), $args['columns']) : array();
		$this->sortable = (isset($args['sortable'])) ?  $args['sortable'] : array();
		$this->table = (isset($args['table'])) ? $args['table'] : '';
		$this->list_page = (isset($args['list_page'])) ? $args['list_page'] : '';
		$this->edit_page = (isset($args['edit_page'])) ? $args['edit_page'] : '';
		$this->filters = (isset($args['filters'])) ? $args['filters'] : '';
		
		$this->actions = (isset($args['filters'])) ? $args['actions'] : '';
		 parent::__construct( array(
		'singular'=> 'wp_list_text_link', //Singular label
		'plural' => 'wp_list_test_links', //plural label, also this well be one of the table css class
		'ajax'	=> false //We won't support Ajax for this table
		) );
	 }

	/**
	 * Add extra markup in the toolbars before or after the list
	 * @param string $which, helps you decide if you add the markup after (bottom) or before (top) the list
	 */
	function extra_tablenav( $which ) {
		if ( $which == "top" ){
			?>
			<?php
		}
		if ( $which == "bottom" ){
			?>
			<?php
		}
	}
	
	/**
	 * Prepare the table with different parameters, pagination, columns and table elements
	 */
	function prepare_items() {
		global $wpdb, $_wp_column_headers;
		$screen = get_current_screen();
	
		/* -- Preparing your query -- */
	        $query = "SELECT * FROM " . $this->table;
			
			$where = ' WHERE 1';
			
			if (isset($_GET['form_id'])) {
				$form_id = intval($_GET['form_id']);
				if (is_int($form_id)) {
					$where .= ' AND `form_id` = ' . $form_id;
				}
			}
			
			if (isset($_POST['s'])) {
				$search_terms = explode(' ', $_POST['s']); 
				if (!empty($search_terms)) {
					$where .= " AND (0 ";
					foreach($search_terms as $term) {
						$where .= " OR `data` like '%$term%'"; 
					}
					$where .= ")";
				}
			}
			
		/* -- Filter parameters -- */
			if (is_array($this->filters))
			foreach($this->filters as $key => $filter) {
				$set_value = !empty($_GET[$key]) ? mysql_real_escape_string($_GET[$key]) : $filter[0]['value'];
				foreach($filter as $accpt_value) {
					if ($set_value == $accpt_value['value']) {
						$where .= $accpt_value['sql'];
					}
				}
			}
			
			$query .= $where;
			
		/* -- Ordering parameters -- */
		    //Parameters that are going to be used to order the result
		    $orderby = !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : '`time`';
		    $order = !empty($_GET["order"]) ? mysql_real_escape_string($_GET["order"]) : 'DESC';
		    if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }
	
		/* -- Pagination parameters -- */
	        //Number of elements in your table?
	        $totalitems = $wpdb->query($query); //return the total number of affected rows
	        //How many to display per page?
	        $perpage = 20;
	        //Which page is this?
	        $paged = !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';
	        //Page Number
	        if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
	        //How many pages do we have in total?
	        $totalpages = ceil($totalitems/$perpage);
	        //adjust the query to take pagination into account
		    if(!empty($paged) && !empty($perpage)){
			    $offset=($paged-1)*$perpage;
	    		$query.=' LIMIT '.(int)$offset.','.(int)$perpage;
		    }
	
		/* -- Register the pagination -- */
			$this->set_pagination_args( array(
				"total_items" => $totalitems,
				"total_pages" => $totalpages,
				"per_page" => $perpage,
			) );
			//The pagination links are automatically built according to those parameters
	
		/* -- Register the Columns -- */
			$columns = $this->columns;
			 $hidden = array();
			 $sortable = $this->sortable;
			 $this->_column_headers = array($columns, $hidden, $sortable);
	
		/* -- Fetch the items -- */
		
			$this->items = $wpdb->get_results($query);
	}	

	function get_bulk_actions() {
		return $this->actions;
	}
    //takes the string and cuts it to an exerpt number of words adding '...' at the end making sure not to cut in the middle 

	function display_rows() {
	    
		//Get the records registered in the prepare_items method
		$records = $this->items;
		
		//Get the columns registered in the get_columns and get_sortable_columns methods
		list( $columns, $hidden ) = $this->get_column_info();
	
		//Loop for each record
		if(!empty($records)){foreach($records as $rec){
			
			//Open the line
	        echo '<tr id="record_'.$rec->id.'">';
			
			foreach ( $columns as $column_name => $column_display_name ) {
	
				//Style attributes for each col
				$class = "class='$column_name column-$column_name'";
				$style = "";
				if ($column_name == 'checkbox') 
					$style = ' width="10"';
				if ( in_array( $column_name, $hidden ) ) 
					$style = ' style="display:none;"';
				$attributes = $class . $style;
	
				//edit link
				// $editlink  = '/wp-admin/admin.php?page='.$this->edit_page.'&edit='.(int)$rec->id;

				$editlink = '<a href="admin.php?page=fld_form_logger&id=' . $rec->id .'">';

				$data = json_decode($rec->data);
				$data_text = '';
                $trimmed_text = "";
				if (isset($data->$column_name))
                {
                    $data_text = $data->$column_name;
                    
                }
            
					if($column_name ==='message') {
					     $string = $data->$column_name;
                        $new_array = array();
                        $array_string = explode(" ",$string); 
                      
 
                        for($i=1; $i <= 10; $i++) {
                            
                            $new_array[] = $array_string[$i-1];
                           
                        }
                        if(count($array_string) > 10) {
                        $new_array[] = "&hellip;";
                        }
                        $trimmed_text = implode(" ", $new_array);
					
               
                    }
				//Display the cell
				switch ( $column_name ) {
					case 'cb': 
						echo '<td '.$attributes.'><input type="checkbox" name="entries[]" value="' . $rec->id .'" /></td>';
					break;
					case "col_time": 
						echo '<td '.$attributes.'>' . $editlink.date('F j, Y g:i a e', $rec->time).'</a></td>';
					break;
                    case 'message':
                        echo '<td '.$attributes.'>' . $editlink . $trimmed_text . '</a></td>';
                    break;
					default:
						echo '<td '.$attributes.'>' . $editlink . $data_text . '</a></td>';
					break;
				}
			}
	
			//Close the line
			echo'</tr>';
		}}
	}
}
?>