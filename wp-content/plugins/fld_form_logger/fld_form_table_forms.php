<?php

class fld_form_table_forms extends WP_List_Table {
	var $columns;
	var $forms;

	 function __construct($args) {
	 	global $wpdb;
	 	$this->columns = array('form_id' => 'Form', 'count' => 'Number of Responses');
		$this->forms = $args;
		 parent::__construct( array(
		'singular'=> 'wp_list_text_link',
		'plural' => 'wp_list_test_links', 
		'ajax'	=> false 
		) );
	 }

	
	function prepare_items() {
		global $wpdb;
	
			 $this->_column_headers = array($this->columns, array(), array());
	
			$forms = array();
			
			foreach($this->forms as $key => $form) {
				$form_row = new stdClass();
				$form_row->form_id = $key;
				
				$count = $wpdb->get_results("SELECT f.form_id, COUNT(f.form_id) as num_responses FROM {$wpdb->prefix}fld_form_log as f WHERE f.form_id = $key GROUP BY f.form_id");
				if ($count && isset($count[0])) {
					$form_row->count = $count[0]->num_responses;
				} else {
					$form_row->count = 0;	
				}
				
				$form_row->name = $form['name'];
				$forms[] = $form_row;
			}
			
			$this->items = $forms;
	}	

	function display_rows() {
		$records = $this->items;
		
		list( $columns, $hidden ) = $this->get_column_info();
	
		if(!empty($records)){foreach($records as $rec){
			
	        echo '<tr id="record_'.$rec->form_id.'">';
			
			foreach ( $columns as $column_name => $column_display_name ) {
	
				$class = "class='$column_name column-$column_name'";
	
				$editlink = '<a href="admin.php?page=fld_form_logger&form_id=' . $rec->form_id .'">';

				switch ( $column_name ) {
					case "count": echo '<td '.$class.'>' . $editlink . $rec->count.'</a></td>';
					break;
					default:
						echo '<td '.$class.'>' . $editlink . $rec->name . '</a></td>';
					break;
				}
			}
	
			echo'</tr>';
		}}
	}
}
?>