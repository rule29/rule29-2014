<?php
define( 'WP_ADMIN', true );

/** Load WordPress Bootstrap */
require_once( dirname( dirname(  dirname( dirname( __FILE__ ) ) ) ) . '/wp-load.php' );

/** Allow for cross-domain requests (from the frontend). */
send_origin_headers();

/** Load WordPress Administration APIs */
require_once( ABSPATH . 'wp-admin/includes/admin.php' );

@header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
@header( 'X-Robots-Tag: noindex' );

send_nosniff_header();
nocache_headers();

$form_id = intval($_POST['form_id']);
$entry_id = intval($_POST['entry_id']);

if (!wp_verify_nonce($_POST['fld_form_logger'], 'delete') || $form_id === 0 || $entry_id === 0)
	die( '0' );

do_action( 'admin_init' );

fld_form_logger::delete_entry($entry_id);

wp_redirect(admin_url('/admin.php?page=fld_form_logger&form_id=' . $form_id));
