<?php
/*
 * Plugin Name: Flood IP Block
 */

class flood_ip_block {
	const MAX_HISTORY = 3600;
	const MAX_PER_HISTORY = 10;
	const MAX_PER_15_MINUTES = 6;
	const MAX_PER_MINUTE = 4; 
	
	private $ip;
	private $username;
	private $past_history;
	private $option_key;
	private $whitelist;
	private $initd = false;
	
	function __construct() {
		add_action('wp_authenticate', array($this, 'login_init'));
		add_action('login_page', array($this, 'login_init'));
		add_action('wp_login_failed', array($this, 'login_failed'));
		add_action('admin_menu', array($this, 'create_menu'));
		
		$plugin = plugin_basename(__FILE__); 
		add_filter("plugin_action_links_" . $plugin, array($this, 'settings_link'));
	}	
	
	public function settings_link($links) {
		
		$settings_link = '<a href="options-general.php?page=ip-block-settings">Settings</a>'; 
		array_unshift($links, $settings_link); 
		return $links; 
	}
	
	public function create_menu() {
		add_options_page('IP Block Settings', 'IP Block Settings', 'manage_options', 'ip-block-settings', array($this, 'settings_page'));

		add_action( 'admin_init', array($this, 'register_settings') );
	}
	
	public function register_settings() {
		register_setting( 'ip_block_settings_group', 'ip_block_whitelist' );
	}
	
	public function settings_page() {
	?>
<div class="wrap">
<h2>IP Block Settings</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'ip_block_settings_group' ); ?>
    <?php do_settings_sections( 'ip_block_settings_group' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">IP Whitelist</th>
        <td><input type="text" name="ip_block_whitelist" value="<?php echo esc_attr( get_option('ip_block_whitelist') ); ?>" /></td>
        </tr>
     </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php
	}
	
	public function login_init() {
		if ($this->initd)
			return;
		$this->initd = true;
		
		$this->load_whitelist();
		$this->get_visitor_ip();

		if (!empty($this->whitelist)) {
			if (in_array($this->ip, $this->whitelist)) {
				return;	
			}
		}
		
		$is_banned = get_option('floodipbanned' . $this->ip, 0);
		if ($is_banned == 1) {
			die('You have been banned from accessing this server.');	
		}
	}
	
	private function load_whitelist() {
		$whitelist = explode(',', get_option('ip_block_whitelist', ''));
		
		$this->whitelist = array();
		foreach($whitelist as $item) {
			$i = trim($item);
			if (!empty($i))
				$this->whitelist[] = $i;
		}
	}
	
	public function login_failed($username) {
		$this->login_init();
		
		if (!empty($this->whitelist)) {
			if (in_array($this->ip, $this->whitelist)) {
				return;	
			}
		}
		$this->username = $username;
		
		$this->get_past_history();
		$this->update_past_history();		
		$this->log_event(array('login failed', $this->ip, count($this->past_history), $this->username));
		
		if ($this->should_ban()) {
			$this->ban_user();
		}
	}
	
	private function ban_user() {
		add_option('floodipbanned' . $this->ip, 1, '', 'no');
		$this->log_event(array('ip banned!', $this->ip));
		
		$to = 'djm406@gmail.com';
		
		$file = __FILE__;
		$content = <<<EOS
<h1>Another One Bites the Dust</h1>
<p>
	The following address has been banned: {$this->ip}<br />
	My file name is $file
</p>
EOS;
		 
		$admin_emails = explode(',', $to);
		$subject = 'IP Blocker - IP Banned';
		$from_name = 'IP Blocker';
		$from_email = 'no-reply@floodlightdesign.com';
		$api_key = 'cde31372-dbea-43c0-8e1b-ffd4cfe5f351';
		 
		require 'mandrill.class.php';
		$mandrill = new mandrill_flood_ip_block($api_key);
		 
		$address_arr = array();
		foreach($admin_emails as $admin_email) {
			$em = trim($admin_email);
			if (strlen($em) > 5) {
				$address_arr[] = $mandrill->create_address($em);
			}
		}
			
		$mandrill->messages_send($content, '', $subject, $from_email, $from_name, $address_arr);
	}
	
	private function should_ban() {
		$history_count = 0;
		$fifteen_minute_count = 0;
		$minute_count = 0;
		
		$time = time();
		$min_fifteen_minute = $time - 60*15;
		$min_minute = $time - 60;
		
		foreach($this->past_history as $t) {
			if ($t >= $min_minute) {
				$fifteen_minute_count++;
				$minute_count++;	
			} elseif ($t >= $min_fifteen_minute) {
				$fifteen_minute_count++;
			}
		}
		
		return (	
					count($this->past_history) > self::MAX_PER_HISTORY || 
					$fifteen_minute_count > self::MAX_PER_15_MINUTES ||
					$minute_count > self::MAX_PER_MINUTE
				);
	}
	
	private function get_past_history() {		
		$this->option_key = 'floodiphistory' . $this->ip;
		$this->past_history = get_option($this->option_key, array());
		if (empty($this->past_history)) {
			add_option($this->option_key, "", '', 'no');
		}
		$this->past_history[] = time();
	}
	
	private function update_past_history() {
		$new_history = array();
		$min_time = time() - self::MAX_HISTORY;
		foreach($this->past_history as $history) {
			if ($history >= $min_time) {
				$new_history[] = $history;	
			}
		}
		$this->past_history = $new_history;
				
		update_option($this->option_key, $this->past_history);
	}
	
	private function log_event($messages) {
		file_put_contents(dirname(__FILE__) . '/log.txt', time() . "," . date('r') . ',' . join(',', $messages) . "\n", FILE_APPEND);
	}
	
	private function get_visitor_ip() {
		$this->ip = trim($_SERVER['REMOTE_ADDR']);	
		
		if (strlen($this->ip) < 7) {
			$str = print_r($_SERVER, true);
			$this->log_event($str);	
		}
	}
}
new flood_ip_block();
	