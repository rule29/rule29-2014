<?php
class mandrill_flood_ip_block {
	const API_URL_PROTOCOL = 'https';
	const API_URL_HOST = 'mandrillapp.com';
	const API_URL_RESOURCE = '/api/1.0/';
	
	const SOCK_TIMEOUT = 10;
	const SOCK_CHUNK_SIZE = 512;
	
	var $api_key = NULL;
	var $error_message = '';
	
 
	function __construct($api_key) {
		$this->api_key = $api_key;
	}	
	
	private function query($method, $request) {
		$request_json = json_encode($request);
		
		$request_json_len = strlen($request_json);
		
		$host = self::API_URL_HOST;
		
		$url = self::API_URL_RESOURCE . $method;
		
		//create request
		$content = "POST $url HTTP/1.0\r\n"
. "Host: $host\r\n"
. "Content-length: $request_json_len\r\n"
. "Connection: close\r\n"
. "\r\n"
. $request_json;
 
		//open ssl connection
		$sock = fsockopen('ssl://' . $host, 443, $errno, $errstr, self::SOCK_TIMEOUT);
		if(!$sock) {
		 	$this->error_message = 'Could not connect to Mandrill API! (' . $errno . ', ' . $errstr . ')';
			return false;
		}
		
		//load response
		$response = "";
		fwrite($sock, $content);
		stream_set_timeout($sock,  self::SOCK_TIMEOUT);
		
		$info = stream_get_meta_data($sock);
		while ((!feof($sock)) && (!$info["timed_out"])) {
		    $response .= fread($sock, self::SOCK_CHUNK_SIZE);
		    $info = stream_get_meta_data($sock);
		}
		fclose($sock);
		
		//parse response
		list($headers, $response) = explode("\r\n\r\n", $response, 2);
	
		
		return json_decode($response);
	}
	
	public function users_info() {
		$request = new stdClass();
		$request->key = $this->api_key;
		
		$result = $this->query('users/info.json', $request);
		
		return $result;
	}
	
	public function create_address($email, $name = '') {
		$address = new stdClass();
		$address->email = $email;
		$address->name = $name;
		
		return $address;
	}
	
	public function create_template_content($name, $content) {
		$tc = new stdClass();
		$tc->name = $name;
		$tc->content = $content;
		
		return $tc;
	}
	
	public function create_merge_var($name, $content) {
		$var = new stdClass();
		$var->name = $name;
		$var->content = $content;
		return $var;
	}
	
	public function messages_send($html, $text, $subject, $from_email, $from_name, $address_arr, $track_opens = true, $track_clicks = true) {
		$request = new stdClass();
		$request->key = $this->api_key;
		$request->message = new stdClass();
		$request->message->html = $html;
		
		if (!empty($text))
			$request->message->text = $text;
		
		$request->message->subject = $subject;
		$request->message->from_email = $from_email;
		
		if (!empty($from_name))
			$request->message->from_name = $from_name;
		
		$request->message->to = $address_arr;
		$request->track_opens = $track_opens;
		$request->track_clicks = $track_opens;		
		
		$result = $this->query('messages/send.json', $request);
		
		if (is_array($result)) {
			return true;
		} else {
			return false;
		}
	}
	
	public function templates_render($template_name, $template_content_arr = array(), $merge_vars_arr = array()) {
		$request = new stdClass();
		$request->key = $this->api_key;
		$request->template_name = $template_name;
		$request->template_content = $template_content_arr;
		
		if (!empty($merge_vars_arr))
			$request->merge_vars = $merge_vars_arr;
				
		
		$result = $this->query('templates/render.json', $request);
		
		if (isset($result->html)) {
			
			return $result->html;
		} else {
			return false;
		}
	}
	
	public function messages_send_template($template_name, $template_content_arr, $subject, $from_email, $from_name, $address_arr, $global_merge_vars_arr = array(), $track_opens = true, $track_clicks = true) {
		$request = new stdClass();
		$request->key = $this->api_key;
		$request->template_name = $template_name;
		$request->template_content = $template_content_arr;
		
		$request->message = new stdClass();
		
		$request->message->subject = $subject;
		$request->message->from_email = $from_email;
		
		if (!empty($from_name))
			$request->message->from_name = $from_name;
		
		$request->message->to = $address_arr;
		$request->message->track_opens = $track_opens;
		$request->message->track_clicks = $track_opens;
			
		if (!empty($global_merge_vars_arr))
			$request->message->global_merge_vars = $global_merge_vars_arr;
				
		
		$result = $this->query('messages/send-template.json', $request);
		
		
		if (is_array($result)) {
			return true;
		} else {
			return false;
		}
	}
}